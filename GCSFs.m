function [] = GCSFs(left_pth, right_pth, output_path, params)
%GCSFs - Growing Correspondences in the Scene Flow (sequence)
%
% GCSFs(left_imgs, right_imgs, output_path, params)
%
% input:
%   left_imgs,right_imgs  - sequence of left,right *rectified* images, e.g. 
%                           '.imgs/left*.jpg','.imgs/right*.jpg', resp.
%               Note: the sequence is the output of dir function.
%   output_path - the directory where results of the scene-flow is saved
%                 (It saves D1,D2,Fh,Fv in .mat file per frame).
%
%   params.parsS - GCS parameters 
%                (see GCS for more details)
%         .pars_pm - Harris prematching parameters 
%                  (see GCS for more details, pm-prefix)
%         .pars_t - tracker parameters (OpenCV LK tracker)
%                .pars_t.winsize = 5 (window size of the tracker)
%                .pars_t.pyrlevels = 4 (number of image pyramid levels)
%         .parsF - GCSF parameters
%                (see GCSF for more details)
%         .vis - visualizatio of the results (1/0)
%        (All parameters are optional)
%
% output:
%   Output is saved in output_path directory (D1,D2,Fh,Fv) in a .mat file
%   per frame.
%
% Example: 
%   GCSFs('imgs/left*.jpg','imgs/right*.jpg','results/')
%
% The algorithm is described in:
%    	Jan Cech, Jordi Sanchez-Riera, Radu Horaud. Scene Flow Estimation
%       by Growing Correspondence Seeds. In Proc. CVPR , 2011.
%       (also available from http://cmp.felk.cvut.cz/~cechj/GCSF/)
%
% Jan Cech, cechj@cmp.felk.cvut.cz, 01/09/2011
% (toolbox revised: 04/04/2013)
%

%% - default parameters --------------------------------------------------

pars0 = [];

%GCS stereo parameters
pars0.parsS = [];
pars0.parsS.searchrange = [-inf,inf];
pars0.parsS.tau = 0.6;
pars0.parsS.window = 2;
pars0.parsS.pm_harris_prctile = 50;
pars0.parsS.pm_tau = 0.8;  
pars0.parsS.pm_window = 2; 
pars0.parsS.algorithm = 'dgrow1';
%pars.zonegap = 1;
%pars.max_candidates = 10;
%pars.mu = 0.1;
%pars.csbeta = 0.1;

%prematching parameters (sparse Harris point matching)
pars0.pars_pm = [];
%pars_pm_harris_prctile = 0;
pars0.pars_pm.pm_tau = 0.9;
pars0.pars_pm.pm_window = 2; %2

%tracker parameters
pars0.pars_t = [];
pars0.pars_t.vis = 0;
pars0.pars_t.winsize = 5; %2
pars0.pars_t.pyrlevels = 4; %2; %0
% pars0.pars_t2 = []; %second parallel tracker
% pars0.pars_t2.vis = 0;
% pars0.pars_t2.winsize = 3; %2
% pars0.pars_t2.pyrlevels = 0; %2; %0

%GCSF parameters
pars0.parsF = [];
pars0.parsF.c_thr = 0.5;
pars0.parsF.kappa = 0.2;
pars0.parsF.epsilon = 0.05;
pars0.parsF.alpha = 0.05; 
pars0.parsF.searchrange_Fh = [-15,15];
pars0.parsF.searchrange_Fv = [-10,10];
pars0.parsF.window = 2;

pars0.vis = 1;

%% fusion of input parameters with default -------------------------------

if ~exist('params','var') || isempty(params),
    params = [];
    fns_ = [];
else
    fns_ = fieldnames(params);
end

pars = [];
fns = fieldnames(pars0);
fn_diff = setdiff(fns_,fns);
if ~isempty(fn_diff)
    for i=1:length(fn_diff)
        warning('Unknown parameter set name "%s"',fn_diff{i});
    end
end
for i=1:length(fns)
    if isfield(params,fns{i}),
        P = params.(fns{i});
        P0 = pars0.(fns{i});
        optioncheck(P0,P);
        P = optionmerge(P0,P);
    else
        P = pars0.(fns{i});
    end
    pars.(fns{i}) = P;
end
% end

%% main loop

tt = 0;

D = dir(left_pth);
left_names = sort({D.name}); left_names = left_names(40:end);

D = dir(right_pth);
right_names = sort({D.name}); right_names = right_names(40:end);

if length(left_names)~= length(right_names) 
    error('Number of images in the left and right image streams are different.')
end

pathL = fileparts(left_pth);
pathR = fileparts(right_pth);


frames = length(left_names);
for iter=1:frames-1;
    t0 = cputime;
    

    %--reading input images, rectification, resizing
    if iter==1,
        Il0 = double(imread(fullfile(pathL, left_names{iter})));  Il0 = round(sum(Il0,3)/3);  
        Ir0 = double(imread(fullfile(pathR, right_names{iter}))); Ir0 = round(sum(Ir0,3)/3);  
        Il1 = double(imread(fullfile(pathL, left_names{iter+1})));  Il1 = round(sum(Il1,3)/3); 
        Ir1 = double(imread(fullfile(pathR, right_names{iter+1}))); Ir1 = round(sum(Ir1,3)/3); 
    else
        Il0 = Il1; Ir0 = Ir1;
        Il1 = double(imread(fullfile(pathL, left_names{iter+1})));  Il1 = round(sum(Il1,3)/3);
        Ir1 = double(imread(fullfile(pathR, right_names{iter+1}))); Ir1 = round(sum(Ir1,3)/3);
    end
    
    %--sparse stereo seeds + initial disparity map + predicted seeds
    if iter==1,
        [D1,W1,x,w,K,S] = gcs(Il0,Ir0,[],pars.parsS); %=>D1,S
    else
        %[tmp,S] = prematching(Il0,Ir0,pars.pars_pm);
        [S] = prematching(Il0,Ir0,pars.pars_pm);
        
        ix = ~isnan(Fh);
        [y0,xl0] = find(ix);
        fh = Fh(ix); xl1=xl0-fh;
        fv = Fv(ix); y1=y0-fv;
        Fh_ = NaN(size(Fh)); Fv_ = NaN(size(Fv));
        ix = sub2ind(size(Fh_),y1,xl1);
        Fh_(ix) = fh;
        Fv_(ix) = fv;
        
        S1 = dmap2seeds(D2); S1 = S1(:,1:3);
        ind = sub2ind(size(Fh_),S1(:,3),S1(:,1));
        S2 = [S1(:,1)-Fh_(ind),S1(:,2)-Fh_(ind),S1(:,3)-Fv_(ind)];
        SEEDs = [S1,S2]; %SEEDs = [SEEDs;SS];   %=>predicted seeds
        D1 = D2;                                %=>D1 
    end
    
    %--tracking
    Pl1 = [LK_track(Il0,Il1, [S(:,1),S(:,3)], pars.pars_t)];
    %    LK_track(Il0,Il1, [S(:,1),S(:,3)], pars.pars_t2)];
    Pr1 = [LK_track(Ir0,Ir1, [S(:,2),S(:,3)], pars.pars_t)];
     %   LK_track(Ir0,Ir1, [S(:,2),S(:,3)], pars.pars_t2)];
    %SEEDss = [[S(:,1);S(:,1)],[S(:,2);S(:,2)],[S(:,3);S(:,3)], round(Pl1(:,1)), round(Pr1(:,1)), round(0.5*(Pl1(:,2)+Pr1(:,2)))];
    SEEDss = [[S(:,1)],[S(:,2)],[S(:,3)], round(Pl1(:,1)), round(Pr1(:,1)), round(0.5*(Pl1(:,2)+Pr1(:,2)))];
    ind = abs([Pl1(:,2)-Pr1(:,2)])>1;
    SEEDss(ind,:) = [];  %=> sparse seeeds (matching+tracking)
    
    %--complete set of seeds
    if iter==1,
        SEEDs = SEEDss;
    else
        %SEEDs = [SEEDss];  %sparse (matching+tracking)
        %SEEDs = [SEEDs];   %predicted seeds
        SEEDs = [SEEDs;SEEDss];  %complete set of seeds (predicted+sparse)
    end
    
    %--nan-check
    SEEDs = round(SEEDs);
    [i,j] = find(isnan(SEEDs));
    SEEDs(i,:) = [];
    %range check for new seeds;
    w = pars.parsF.window;
    ind = (SEEDs(:,4)<=w | SEEDs(:,5)<=w | SEEDs(:,6)<=w | ...
        SEEDs(:,4)>=size(Il1,2)-w | SEEDs(:,5)>=size(Ir1,2)-w | SEEDs(:,6)>=size(Il1,1)-w );
    SEEDs(ind,:) = [];
    
    %--alpha correlation enhancement
    if iter==1,
        alpha0 = pars.parsF.alpha;
        pars.parsF.alpha = 0;
    else
        pars.parsF.alpha = alpha0;
    end
    
    %--dgrowF
    [D2,Fh,Fv,W] = GCSF(Il0,Ir0,Il1,Ir1,round(D1),SEEDs,pars.parsF);
  
    %--saving results
    if iter==1, %save parameters
        save([output_path,'sf_pars'],'pars');
    end
    out_fname = sprintf('%ssf_%05i.mat',output_path,iter);
    if iter>1 %do not write first startup frames
        save(out_fname,'D1','D2','Fh','Fv','SEEDss');
    end
    if iter==length(frames), %GCSFs finished
        fil = fopen([output_path,'done.txt'],'w'); fclose(fil);
    end
        
    %--visualization
    if pars.vis,
        figure(1);
        if iter==1,
            pos = get(1,'position');
            pos(3) = 2*pos(4);
            set(1,'position',pos);
        end
        set(1,'name', ['GCSFs - frame #',num2str(iter+1),'/',num2str(frames)]);
        subplot(2,3,1,'replace'); subimg(Il0,gray(256),[]); title 'I_L^0'; axis off; axis tight; 
        subplot(2,3,2,'replace'); subimg(Ir0,gray(256),[]); title 'I_R^0'; axis off; axis tight; 
        subplot(2,3,3,'replace'); subimg(D1, dmapb, [pars.parsS.searchrange(1), pars.parsS.searchrange(2)]); title('D^0'); axis off; axis tight;
        subplot(2,3,4,'replace'); subimg(D2, dmapb, [pars.parsS.searchrange(1), pars.parsS.searchrange(2)]); title('D^1'); axis off; axis tight;
        subplot(2,3,5,'replace'); subimg(Fh, dmapb, [-15,15]); axis off; title('F_h'); axis tight; 
        subplot(2,3,6,'replace'); subimg(Fv, dmapb, [-15,15]); axis off; title('F_v'); axis tight;
        drawnow;
    end
     
    %--grow stereo (to recover lost or motion occluded pixels)
    S2 = dmap2seeds(D2);
    parsm2 = pars.parsS;
    parsm2.init_seeds_accept = 1;
    %parsm2.algorithm = 'dgrow0';
    [D2,W2] = gcs(Il1,Ir1,S2(:,1:3),parsm2); 

end


