%demo of GCSF matching 
%   (example on a short sequence of CAVA dataset of INRIA)

p = []';
p.parsS.searchrange = [-30,5]; %stereo disparity search range 

GCSFs('imgs/left*.jpg','imgs/right*.jpg','results/',p)