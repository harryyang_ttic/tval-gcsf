function [P1] = LK_track(I0,I1,P0, pars);
%LK_track - Lucas-Kanade tracker using openCV
%
%  [P1] = LK_track(I0,I1,P0 pars);
%
%       I0 - image at time t
%       I1 - image at time t+1
%       P0 - input points to track (in I0), (nx2) [col,row]
%            (if left empty, automatic initialization is launched)
%       pars - parmeter structure (see below)
%
%       P1 - out points tracked (in I1),(nx2) [col,row]
%            (if contains NaN, the point is lost)
%
%   default parameters: pars.
%       pyrlevels = 0 (pyramids not used)
%       winsize = 5   (window full-size)
%       maxiter = 20  (maximum number of iterations)
%       EPS = 0.03    (convergence termination epsilon)
%       vis = 0       (if 1, tracking is visualized)
%

pars0.pyrlevels = 0;
pars0.winsize = 5;
pars0.maxiter = 20;
pars0.EPS = 0.03;
pars0.vis = 0;

if exist('pars','var'),
    optioncheck(pars0,pars);
    pars = optionmerge(pars0,pars);
else
    pars = pars0;
end

%[new_points] = LK_track_mex(I0, I1, points, 
%                             pars_pyrlevels, pars_winsize, pars_maxiter, pars_EPS, pars_vis);

[tmp,host] = system('hostname');
if strcmp(deblank(host),'chamaeleon') 
    P1 = LK_track_mex_chamaeleon(I0, I1, P0, pars.pyrlevels, pars.winsize, pars.maxiter, pars.EPS, pars.vis);
elseif strcmp(deblank(host),'cassiopeia')
    P1 = LK_track_mex_cassiopeia(I0, I1, P0, pars.pyrlevels, pars.winsize, pars.maxiter, pars.EPS, pars.vis);
else
    P1 = LK_track_mex(I0, I1, P0, pars.pyrlevels, pars.winsize, pars.maxiter, pars.EPS, pars.vis);
end



