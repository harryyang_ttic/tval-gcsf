function [map] = dmapb();
%dmapb - black-background colormap with based on jet colormap
%
%

map = jet(256);
map(1,:) = [0,0,0];