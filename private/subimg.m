function subimg(X, map, clim)
%SUBIMG - subimage with independent colormap and Clim
%
%  subimg(X, map, clim)
%
%   X - (m x n) image 
%   map - color map (3 x k)
%   clim - color limit (1 x 2)
%

if isempty(clim)
    clim = [min(min(X)),  max(max(X))];
end

L = size(map,1);
k = (L-2)/(clim(2)-clim(1));
q = 2-k*clim(1);

Y = uint8(k*X + q);

Yrgb = ind2rgb(Y, map);
imagesc(Yrgb); axis equal; %set(gca,'LooseInset',get(gca,'TightInset'))







