function [SEEDs] = dmap2seeds(D,Dv)
%
% [SEEDs] = dmap2seeds(D,Dv);
%
%    D - horizontal disparity map
%    Dv - vertical disparity map (if left empty, zero is assumed)
%
%  SEEDs - [xl,xr,yl,yr] (nx4)
%

if ~exist('Dv','var');
    Dv = zeros(size(D));
end
    
[yl,xl] = find(~isnan(D));
ind = sub2ind(size(D),yl,xl);

xr = xl - D(ind);
yr = yl - Dv(ind);


SEEDs = [xl,xr,yl,yr];
