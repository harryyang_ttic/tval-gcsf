%MAKE_GCSF - compile mex modules (run first to install the toolbox)

disp('Mexing...');

% -----------------

fprintf('1) GCS...');
mex -I./src/GCS/include/ ./src/GCS/boxing.c ./src/GCS/src/myerror.c ./src/GCS/src/imglib.c
mex -I./src/GCS/include/ ./src/GCS/smatchingl.c ./src/GCS/src/myerror.c ./src/GCS/src/heap.c ./src/GCS/src/ejournal.c

mex -I./src/GCS/include/ ./src/GCS/prematch_mex.cpp
mex -I./src/GCS/include/ ./src/GCS/dgrow0_bf_mex.cpp
mex -I./src/GCS/include/ ./src/GCS/dgrow3b_bf_mex.cpp
fprintf('Done.\n')

% -----------------

fprintf('2) GCSF...');
mex ./src/dgrowF_bf_mex.cpp -I./src/include/

if ispc
    try
        mex ./src/LK_track_mex.cpp -I./src/include
    catch err
        fprintf('There is a problem with OpenCV. Check if it is installed properly with Matlab.\n See e.g., http://www.mathworks.com/matlabcentral/fileexchange/40665-matalb-and-opencv for more details. \n')
        rethrow(err)
    end
else
    try 
    %mex ./src/LK_track_mex.cpp -I./src/include -I/usr/include/opencv -L/usr/lib/ -lcxcore -lcv -lcvaux -lhighgui -lml
    mex ./src/LK_track_mex.cpp -I./src/include -I/usr/include/opencv -L/usr/lib/ -lopencv_core  -lopencv_ml -lopencv_imgproc -lopencv_video
    catch err
        fprintf('There is a problem with OpenCV. Check if it is installed properly in your system and if the paths are correct. \n')
        rethrow(err)
    end
end

fprintf('Done.\n')

% -----------------


%Move mexes to private folder
system('mv boxing* private');
system('mv smatchingl* private');
system('mv *_mex* private');

% -----------------

disp('Completed succesfully.');
