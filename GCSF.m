function [D2,Fh,Fv,W]=GCSF(Il1,Ir1,Il2,Ir2,D1,SEEDs,pars)
%GCSF - Growing Correspondences in the Scene Flow (frame-to-frame)
%             
%  [D2,Fh,Fv,W]=GCSF(Il1,Ir1,Il2,Ir2,D1,SEEDs,pars)
%
%   Il1,Ir1...input images (rectified) at time t1
%   Il2,Ir2...input images (rectified) at time t2
%   SEEDs...initial correspondences [xl1,xr1,y1,xl2,xr2,y2] (nx6 vector)
%   pars....(optional parameters, see below for default values)
%       .window = 2; %half window size
%       .c_thr = 0.7; %threshold for correlation growing
%       .epsilon = 0; %local ambiguity (minimal distance of 1st and 2nd corr. maxima)
%       .searchrange = [-inf,inf];  %disparity searchrange (unlimited)
%       .searchrange_Fh = [-inf,inf]; %flow searchrange (horizontal)
%       .searchrange_Fv = [-inf,inf]; %flow searchrange (vertical)
%       .vis_step = inf; %number of steps to display current disparity map
%       .kappa = 0; %zero flow regularization constant: c = c*exp(-k*(abs(dh)+abs(dv)) 
%       .alpha = 0.1; %a seed enforcement constant (it is summed to the  seed correlation c = c + alpha)
%
%   D2...disparity maps (at time t2)
%   Fh,Fv...optical flow in the left image 
%           (Fr_h = D2 + Fh, Fr_v = Fv) 
%   W...correlation map 
%
% See Also: GCSFs, GCS
%

pars0 = []; %default parametres
pars0.window = 2; %half window size
pars0.c_thr = 0.7; %threshold for correlation growing
pars0.epsilon = 0; %local ambiguity (minimal distance of 1st and 2nd corr. maxima)
pars0.searchrange = [-inf,inf];  %disparity searchrange (unlimited)
pars0.searchrange_Fh = [-inf,inf]; %flow searchrange (horizontal)
pars0.searchrange_Fv = [-inf,inf]; %flow searchrange (vertical)
pars0.vis_step = inf; %number of steps to display current disparity map
%pars0.init_seeds_accept = 0; %accept all initial seeds (turned off)
pars0.kappa = 0; %zero flow regularization constant: c = c*exp(-k*(abs(dh)+abs(dv)) 
pars0.alpha = 0.1; %a constant which is added to the seed correlation c = c + alpha

                    %xl, xr, y
pars0.local_search = [ 0, 0, 0; ... 
                      -1, 0, 0; ...
                      +1, 0, 0; ...
                       0,-1, 0; ...
                       0,+1, 0; ...
                       0, 0,-1; ...
                       0, 0,+1];

%pars0.local_search = [0 0 0];                   
                   
if exist('pars','var'),
    optioncheck(pars0,pars);
    pars = optionmerge(pars0,pars);
else
    pars = pars0;
end

w = pars.window;
c_thr = pars.c_thr;
local_search = pars.local_search;
epsilon = pars.epsilon;
vis_step = pars.vis_step;
searchrange = pars.searchrange;
searchrange_Fh = pars.searchrange_Fh;
searchrange_Fv = pars.searchrange_Fv;
%i_seed_accept = pars.init_seeds_accept;
kappa = pars.kappa;
alpha = pars.alpha;

a = (2*w+1)^2;

%precomputed local characteristics - t=1
sL1 = conv2(Il1,ones(2*w+1),'same')/sqrt(a);
sR1 = conv2(Ir1,ones(2*w+1),'same')/sqrt(a);
vL1 = conv2(Il1.^2,ones(2*w+1),'same') - sL1.^2;
vR1 = conv2(Ir1.^2,ones(2*w+1),'same') - sR1.^2;

%precomputed local characteristics - t=2
sL2 = conv2(Il2,ones(2*w+1),'same')/sqrt(a);
sR2 = conv2(Ir2,ones(2*w+1),'same')/sqrt(a);
vL2 = conv2(Il2.^2,ones(2*w+1),'same') - sL2.^2;
vR2 = conv2(Ir2.^2,ones(2*w+1),'same') - sR2.^2;

%
% c = MNCC(xl,xr,row) 
%
% c = 2*( sLR(xl,xr,row) - sL(row,xl)*sR(row,xr) ) / ( vL(row,xl) + vR(row,xr) )
% var Il(row,xl) = vL/a   %variance normalized by n, not (n-1)
%

%[D,Dv,W]=dgrow4_bf_mex(Il,Ir,sL,sR,vL,vR,SEEDs,w,local_search,c_thr,searchrange,searchrangeV,vis_step,epsilon,i_seed_accept,kappa);
[D2,Fh,Fv,W] = dgrowF_bf_mex(Il1,Ir1,sL1,sR1,vL1,vR1, ...
                             Il2,Ir2,sL2,sR2,vL2,vR2, ...
                             D1,SEEDs, w, c_thr, ...
                             local_search, searchrange, searchrange_Fh, searchrange_Fv, ...
                             vis_step, epsilon, kappa, alpha);

