This toolbox contains an algorithm for computing a scene-flow (=disparity and optical flow) in a  setup with binocular stereo sequences. The algorithm is described in: 

Jan Cech, Jordi Sanchez-Riera, Radu Horaud. Scene Flow Estimation by Growing Correspondence Seeds. In Proc. CVPR , 2011.
http://cmp.felk.cvut.cz/~cechj/GCSF/

@InProceedings{Cech-CVPR-2011,
  author       = {Cech, Jan and Sanchez-Riera, Jordi and Horaud, Radu P.},
  title        = {Scene Flow Estimation by Growing Correspondence Seeds},
  booktitle    = {Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition},
  pages        = {49--56},
  book_pages   =  {3503},
  day          = {20--25},
  month        = {June},
  year         = {2011},
  publisher    = {IEEE Computer Society Press},
  address       = {Los Alamitos, USA}, 
  ISBN         = {978-1-4577-0394-2},
  venue        = {Colorado Springs, CO, USA},
  url          = {http://perception.inrialpes.fr/Publications/2011/CSH11},
  ANNOTE       = {A simple seed growing algorithm for estimating scene flow in a stereo setup is presented. Two calibrated and synchronized cameras observe a scene and output a sequence of image pairs. The algorithm simultaneously computes a disparity map between the image pairs and optical flow maps between consecutive images. This, together with calibration data, is an equivalent representation of the 3D scene flow, i.e. a 3D velocity vector is associated with each reconstructed point. The proposed method starts from correspondence seeds and propagates these correspondences to their neighborhood. It is accurate for complex scenes with large motions and produces temporally-coherent stereo disparity and optical flow results. The algorithm is fast due to inherent search space reduction. An explicit comparison with recent methods of spatiotemporal stereo and variational optical and scene flow is provided.},
  keywords     = {scene flow, disparity, optical flow, stereo, seed growing},
  status = {published},
}

Please refer to that article if you are publishing results based on
this toolbox.


INSTALLATION:

1) The toolbox requires OpenCV (http://opencv.org/),  since it uses the implementation of LK-tracker. Note that OpenCV must be installed in the computer. 

2) To compile mex-modules, run a Matlab script: make_GCSF.m, which compiles all mex function. In case, the compilation fails, it is likely that there is a problem with OpenCV. In Windows, file mexopts.bat must be modified (see e.g., http://www.mathworks.com/matlabcentral/fileexchange/40665-matalb-and-opencv for more details.). In Linux, check if the paths in the make_GCSF script are with accordance to paths in your system.

Note that this GCSF toolbox, includes a complete GCS toolbox for stereo matching (desribed http://cmp.felk.cvut.cz/~cechj/GCS/).


TESTING:

- Run test_GCSF to test functionality and learn how to run the main module. 


This is free toolbox for a non-commercial purpose. Commercial use
requires a licence. The software is distributed AS IS, WITHOUT ANY
IMPLIED OR EXPRESSED WARRANTY and WITHOUT ANY FURTHER SUPPORT for the
desired and/or other purpose.

(c) Jan Cech (cechj@cmp.felk.cvut.cz) FEE CTU Prague, 04 Mar 13

