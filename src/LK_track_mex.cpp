//bug fix - memory leak,  Aug 3, 2012

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp" 

//using namespace cv;

#include "mex.h"
#include <arrays.h>
#include <my_opencv.h>

const double NaN = mxGetNaN();

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
  //input
  ARRAY_2D<double> I0(prhs[0]); 
  ARRAY_2D<double> I1(prhs[1]);
  ARRAY_2D<double> points_orig(prhs[2]);
   
           //pars_pyrlevels, pars_winsize, pars_maxiter, pars_EPS, pars_vis);
  double *tmp;
  tmp = mxGetPr(prhs[3]);  const int pars_pyrlevels = int(tmp[0]);
  tmp = mxGetPr(prhs[4]);  const int pars_winsize = int(tmp[0]);
  tmp = mxGetPr(prhs[5]);  const int pars_maxiter = int(tmp[0]);
  tmp = mxGetPr(prhs[6]);  const double pars_EPS = tmp[0];
  tmp = mxGetPr(prhs[7]);  const int pars_vis = int(tmp[0]);
  
        
  //openCV data
  IplImage *Pyr0 = 0, *Pyr1 = 0;
  
  IplImage *Im0, *Im1;
  Im0 = convert_ARRAY_2D_to_IplImage(I0);
  Im1 = convert_ARRAY_2D_to_IplImage(I1);
  
  
  int MAX_COUNT=500; //if automatic initialization used;
  if (points_orig.M>0) MAX_COUNT = points_orig.M;
          
  CvPoint2D32f* points[2] = {0,0};
  char* status = 0;
  
  
  points[0] = (CvPoint2D32f*)cvAlloc(MAX_COUNT*sizeof(points[0][0]));
  points[1] = (CvPoint2D32f*)cvAlloc(MAX_COUNT*sizeof(points[0][0]));
  status = (char*)cvAlloc(MAX_COUNT);

  //int win_size = 5;
  int flags = 0;

  int count;

  if (points_orig.M == 0) { 
      /* automatic initialization */
      IplImage* eig = cvCreateImage( cvGetSize(Im0), 32, 1 );
      IplImage* temp = cvCreateImage( cvGetSize(Im0), 32, 1 );
      double quality = 0.01;
      double min_distance = 10;
      
      count = MAX_COUNT;
      
      cvGoodFeaturesToTrack( Im0, eig, temp, points[0], &count,
              quality, min_distance, 0, 3, 0, 0.04 );
      cvFindCornerSubPix( Im0, points[0], count,
              cvSize(pars_winsize, pars_winsize), cvSize(-1, -1),
              cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.03));
      cvReleaseImage( &eig );
      cvReleaseImage( &temp );
  }
  else {
    /* read the input*/
    count = points_orig.M;
    for (int i=0; i<count; i++) {
        points[0][i].x = points_orig.s(i+1,1);
        points[0][i].y = points_orig.s(i+1,2);
    }
  }

//             for (int i=0; i<count; i++) 
//                 mexPrintf("(%f,%f) \n",points[1][i].x, points[1][i].y);
                
//mexPrintf("%i",count);

cvCalcOpticalFlowPyrLK( Im0, Im1, Pyr0, Pyr1,
        points[0], points[1], count, cvSize(pars_winsize, pars_winsize), pars_pyrlevels, status, 0,
        cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, pars_maxiter, pars_EPS), flags );
                        
    
//output
plhs[0] = mxCreateDoubleMatrix(count, 2, mxREAL);
ARRAY_2D<double> points_new(plhs[0]);
for (int i=0; i<count; i++) {
    if (status[i]) {
        points_new.s(i+1, 1) = points[1][i].x;
        points_new.s(i+1, 2) = points[1][i].y;
    }
    else {
        points_new.s(i+1, 1) = NaN;
        points_new.s(i+1, 2) = NaN;
    }
}

// //vis
// if (pars_vis) {
//     for (int i=0; i<count; i++) {
//     //         mexPrintf("%i: (%f,%f) -> (%f,%f)\n",
//     //                 status[i],
//     //                 points[0][i].x, points[0][i].y,
//     //                 points[1][i].x, points[1][i].y);
//         if (status[i]) cvCircle( Im1, cvPointFrom32f(points[0][i]), 3, CV_RGB(255, 255, 255), -1, 8, 0);
//     }
//     //cvNamedWindow("img0", CV_WINDOW_AUTOSIZE); cvShowImage( "img0", Im0);
//     cvNamedWindow("img1", CV_WINDOW_AUTOSIZE); cvShowImage( "img1", Im1);
//     cvWaitKey(10);
// }
  
    cvReleaseImage( &Im0);
    cvReleaseImage( &Im1);
    cvFree( &points[0]);
    cvFree( &points[1]);
    cvFree( &status);
    cvReleaseImage( &Pyr0);
    cvReleaseImage( &Pyr1);
    
  
}


// [new_points] = LK_track_mex(I0, I1, points, 
//                             pars_pyrlevels, pars_winsize, pars_maxiter, pars_EPS, pars_vis);
//
//    I0,I1 - input (subsequent images) (double)
//    points - [col,row] (nx2)  - if empty automatic initialization is done (good features to track)

//     pars_pyrlevels   - number of image pyramid levels
//     pars_win_size    - window size
//     pars_maxiter     - maximum number of iterations
//     pars_EPS         - convergence termination epsiolon
//     pars_vis         - if not 0 shows tracked points
//     

//    new_points - [col,row] (mx2)


