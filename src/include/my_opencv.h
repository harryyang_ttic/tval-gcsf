// // --------------------------------------------------------------------
// Mat convert_mxArray_to_Mat(const mxArray* D) {
//     double *data;
//     int M = mxGetM(D);
//     int N = mxGetN(D);
// 
//     Mat result(M,N, CV_8UC1); 
//     data=mxGetPr(D);
//     
//     for(int i=0; i < M; i++)
//         for(int j=0; j< N; j++) 
//         result.at<uchar>(i, j) = uchar(data[j*M+i]);
//     
//     return result;
// }
// 
// // --------------------------------------------------------------------
// //template<class T>
// Mat convert_ARRAY_2D_to_Mat(ARRAY_2D<double>& D) {
// 
//     Mat result(D.M,D.N, CV_8UC1); 
//     
//     for(int i=0; i < D.M; i++)
//         for(int j=0; j< D.N; j++) 
//         result.at<uchar>(i, j) = uchar(D.s(i+1,j+1));
//     
//     return result;
// }
// 
// 
// // -------------------------------------------------------------------


 IplImage*  convert_ARRAY_2D_to_IplImage(ARRAY_2D<double>& D) {

  CvScalar s;
  IplImage* Im0=cvCreateImage(cvSize(D.N,D.M),IPL_DEPTH_8U,1); 
  for (int i=0; i<D.M; i++) 
      for (int j=0; j<D.N; j++) {
            s.val[0] = uchar(D.s(i+1,j+1));
            cvSet2D(Im0,i,j,s);
      }
  return Im0;  
 }

 // -------------------------------------------------------------------
 