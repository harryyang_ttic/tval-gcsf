#include <queue>
#include <mex.h>
#include <arrays.h> //my library /home.dokt/cechj/matlab/cech/src/
                    //compile >> mex fname.c -I/home.dokt/cechj/matlab/cech/src/

using namespace std;  // std c++ libs implemented in std

#define max(a,b) (a>b)?a:b;

const double NaN = mxGetNaN();
const double eps = mxGetEps();
                    
struct seed {
  int xl;
  int xr;
  int row;
  double c;
};

bool operator <(const seed& a, const seed&b) {
  return a.c<b.c;
}


class MAIN {
public:
  ARRAY_2D<double> iL;
  ARRAY_2D<double> iR;
  ARRAY_2D<double> sL;
  ARRAY_2D<double> sR;
  ARRAY_2D<double> vL;
  ARRAY_2D<double> vR;
  ARRAY_2D<double> SEEDs;
  ARRAY_2D<double> D; //output disparity map
  ARRAY_2D<double> Dr; //disparity map (right camera)
  ARRAY_2D<double> W; //output correlation map

  int w;
  int range;
  double c_thr;
  double epsilon;
  unsigned vis_step;
  double dmin, dmax; //disparity searchrange
  bool i_seed_accept; //accept all initial seeds
  bool seed_accept; //accept all seeds when growing

  priority_queue<seed> SEEDS;

  double wcorr(int xl, int xr, int row);
  void local_optim(int xl, int xr, int row, double& c, int& dp);
  void lo_left(int& xl, int& xr, int& row, double& c);  //for grow5
  void lo_right(int& xl, int& xr, int& row, double& c); //for grow5
  void lo_up(int& xl, int& xr, int& row, double& c);    //for grow5
  void lo_down(int& xl, int& xr, int& row, double& c);  //for grow5
  void grow();
  void grow5(); //but symmetric local optimization when growing

  MAIN(ARRAY_2D<double>& iL_, ARRAY_2D<double> &iR_,
       ARRAY_2D<double>& sL_, ARRAY_2D<double> &sR_,
       ARRAY_2D<double>& vL_, ARRAY_2D<double> &vR_,
       ARRAY_2D<double>& SEEDs_, 
       ARRAY_2D<double>& D_, ARRAY_2D<double>& Dr_,
       ARRAY_2D<double>& W_,
       int w_, int range_, double c_thr_, int vis_step_,
       double dmin_, double dmax_, double epsilon_,
       bool i_seed_accept_, bool seed_accept_);

  ~MAIN(){};
};

// MAIN methods implementation ---------------------------------------------

MAIN::MAIN(ARRAY_2D<double>& iL_, ARRAY_2D<double> &iR_,
       ARRAY_2D<double>& sL_, ARRAY_2D<double> &sR_,
       ARRAY_2D<double>& vL_, ARRAY_2D<double> &vR_,
       ARRAY_2D<double>& SEEDs_, 
       ARRAY_2D<double>& D_, 
       ARRAY_2D<double>& Dr_, 
       ARRAY_2D<double>& W_,
       int w_, int range_, double c_thr_, int vis_step_,
       double dmin_, double dmax_, double epsilon_,
       bool i_seed_accept_, bool seed_accept_) {

  seed s; 
  int i;

  iL.assign(iL_);
  iR.assign(iR_);
  sL.assign(sL_);
  sR.assign(sR_);
  vL.assign(vL_);
  vR.assign(vR_);
  SEEDs.assign(SEEDs_);
  D.assign(D_);
  Dr.assign(Dr_);
  W.assign(W_);

  w = w_; range = range_; c_thr = c_thr_; vis_step = vis_step_;
  dmin = dmin_; dmax = dmax_;
  epsilon = epsilon_;
  i_seed_accept = i_seed_accept_;
  seed_accept = seed_accept_;

  //output intialilzation
  for(i=1;i<=(iL.M*iL.N);i++) { 
    D[i] = NaN;
    W[i] = NaN;
  }
  for(i=1;i<=(iR.M*iR.N);i++) {//tuhle chybu (iL.M na iR.M) jsem hledal 2 hodiny
    Dr[i] = NaN;
  }

  
  //filling the list (priority queue)  of seeds and initial disparity 
  //into disparity map
  for (i=1;i<=SEEDs.M;i++) {
    s.xl = int(SEEDs.s(i,1));
    s.xr = int(SEEDs.s(i,2));
    s.row = int(SEEDs.s(i,3));
    s.c = wcorr(s.xl,s.xr,s.row);
    
    //postpone the decision (accept seed when removing from growing queue)
    //27.10.2006
    if (i_seed_accept) {
      SEEDS.push(s);
      D.s(s.row,s.xl) = s.xl-s.xr; 
      Dr.s(s.row,s.xr) = s.xl-s.xr;
      W.s(s.row,s.xl) = s.c;
    }
    else {
      if ( s.c > c_thr ) {
        SEEDS.push(s);
      }
    }
    

  }

//   s.xl = 1; s.xr = 1; s.row = 2; s.c = 0.1;
//   SEEDS.push(s);
//   s.xl = 1; s.xr = 1; s.row = 2; s.c = 0.1;
//   SEEDS.push(s);
//   //Print out the content
//   while (!SEEDS.empty()) {
//     s = SEEDS.top();
//     mexPrintf("(%i,%i,%i|%f) \n",s.xl,s.xr,s.row,s.c);
//     SEEDS.pop();
//   }
  
}

// window correlation --------------------------------------------------------

double MAIN::wcorr(int xl, int xr, int row) {
  int i,j;
  double sLR;
  double c;
  
  sLR = 0;
  for (i=-w; i<=w; i++) {
      for (j=-w; j<=w; j++) {
        sLR = sLR + iL.s(row+i,xl+j) * iR.s(row+i,xr+j);
      }
  }

  c = 2*(sLR - sL.s(row,xl)*sR.s(row,xr)) / (vL.s(row,xl)+vR.s(row,xr));
  if (c>1) c = 0; //hack
  return c;
}

// local optimization ------------------------------------------------------

void MAIN::local_optim(int xl, int xr, int row, double& c, int& dp) {
  //c,dp is output
  
  int i,xrr;
  double cc; //current corelation
  double c2; //2nd best correlation

  c = -1;
  dp = 0;  
  for (i=-range; i<=range; i++) {
    xrr = xr + i;
    if (xl>w && xl<iL.N-w && xrr>w && xrr<iR.N-w && row>w && row<iL.M-w ) {
      cc = wcorr(xl,xrr,row);
      if (cc > c) {
        c2 = c;
        c = cc;
        dp = i;
      };
    };
  }
  if ( c-c2 < epsilon ) { //do not grow when ambiguity
    c = -1;
  }
}

// local optimization (left) -------------------------------------------------

void MAIN::lo_left(int& xlo, int& xro, int& row, double& c) {
  //c, xlo, xro (output)
  
  int xli,xri,xl,xr;
  double cc;

  xli = xlo;
  xri = xro;
  
  c = -1;

  if (row>w && row<iL.M-w) { //perhaps redundant

    //1
    xl = xli-1; 
    xr = xri-1;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //2
    xl = xli-2; 
    xr = xri-1;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli-1; 
    xr = xri-2;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }

  }
}

// local optimization (right) -------------------------------------------------

void MAIN::lo_right(int& xlo, int& xro, int& row, double& c) {
  //c, xlo, xro (output)
  
  int xli,xri,xl,xr;
  double cc;

  xli = xlo;
  xri = xro;
  
  c = -1;

  if (row>w && row<iL.M-w) { //perhaps redundant

    //1
    xl = xli+1; 
    xr = xri+1;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //2
    xl = xli+2; 
    xr = xri+1;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli+1; 
    xr = xri+2;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }

  }
}

// local optimization (up) -------------------------------------------------

void MAIN::lo_up(int& xlo, int& xro, int& row, double& c) {
  //c, xlo, xro, row (output)
  
  int xli,xri,xl,xr;
  double cc;

  xli = xlo;
  xri = xro;
  

  c = -1;
  
  row = row - 1;
  if (row>w && row<iL.M-w) { 

    //1
    xl = xli; 
    xr = xri;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //2
    xl = xli-1; 
    xr = xri;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli+1; 
    xr = xri;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //4
    xl = xli; 
    xr = xri-1;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli; 
    xr = xri+1;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }

  }
}

// local optimization (down) -------------------------------------------------

void MAIN::lo_down(int& xlo, int& xro, int& row, double& c) {
  //c, xlo, xro, row (output)
  
  int xli,xri,xl,xr;
  double cc;

  xli = xlo;
  xri = xro;
  

  c = -1;
  
  row = row + 1;
  if (row>w && row<iL.M-w) { 

    //1
    xl = xli; 
    xr = xri;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //2
    xl = xli-1; 
    xr = xri;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli+1; 
    xr = xri;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //4
    xl = xli; 
    xr = xri-1;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli; 
    xr = xri+1;
    if (xl>w && xl<iL.N-w && xr>w && xr<iR.N-w) {
      cc = wcorr(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }

  }
}


// growing -----------------------------------------------------------------

void MAIN::grow() {

  unsigned vis=0;
  int xl,xr,row,dp;
  seed s,st;
  double c;
  int ind;
  mxArray *M; //only for visualization
  int d;

  //main loop
  while (!SEEDS.empty()) {
    s = SEEDS.top();
    SEEDS.pop();   //remove the processed seed


    //add the seed into the current disparity map
    if (!i_seed_accept) {
      if ( mxIsNaN(D.s(s.row,s.xl)) ) { 
        D.s(s.row,s.xl) = s.xl - s.xr;
        Dr.s(s.row,s.xr) = s.xl - s.xr;
        W.s(s.row,s.xl) = s.c;
      }
    }

    //mexPrintf("%f ",s.c);

    //left
    xl = s.xl-1;
    xr = s.xr-1; 
    row = s.row;
    ind = D.sub2ind(row,xl);
    if ( mxIsNaN(D[ind]) ) {
      local_optim(xl,xr,row,c,dp);
      if (c>c_thr) {
        if ( mxIsNaN(Dr.s(row,xr+dp)) ) { //uniquness constraint
          d = xl-(xr+dp);
          if (d>=dmin & d<=dmax) {
            if (seed_accept) {
              Dr.s(row,xr+dp) = d;
              D[ind] = d;
              W[ind] = c;
            }
            st.xl = xl;
            st.xr = xr+dp;
            st.row = row;
            st.c = c;
            SEEDS.push(st);
          }
        }
      }
    };

    //right
    xl = s.xl+1;
    xr = s.xr+1; 
    row = s.row;
    ind = D.sub2ind(row,xl);
    if ( mxIsNaN(D[ind]) ) {
      local_optim(xl,xr,row,c,dp);
      if (c>c_thr) {
        if ( mxIsNaN(Dr.s(row,xr+dp)) ) { //uniquness constraint
          d = xl-(xr+dp);
          if (d>=dmin & d<=dmax) {
            if (seed_accept) {
              Dr.s(row,xr+dp) = d;
              D[ind] = d;
              W[ind] = c;
            }
            st.xl = xl;
            st.xr = xr+dp;
            st.row = row;
            st.c = c;
            SEEDS.push(st);
          }
        }
      }
    };

    //up
    xl = s.xl;
    xr = s.xr; 
    row = s.row-1;
    ind = D.sub2ind(row,xl);
    if ( mxIsNaN(D[ind]) ) {
      local_optim(xl,xr,row,c,dp);
      if (c>c_thr) {
        if ( mxIsNaN(Dr.s(row,xr+dp)) ) { //uniquness constraint
          d = xl-(xr+dp);
          if (d>=dmin & d<=dmax) {
            if (seed_accept) {
              Dr.s(row,xr+dp) = d;
              D[ind] = d;
              W[ind] = c;
            }
            st.xl = xl;
            st.xr = xr+dp;
            st.row = row;
            st.c = c;
            SEEDS.push(st);
          }
        }
      }
    };

    //down
    xl = s.xl;
    xr = s.xr; 
    row = s.row+1;
    ind = D.sub2ind(row,xl);
    if ( mxIsNaN(D[ind]) ) {
      local_optim(xl,xr,row,c,dp);
      if (c>c_thr) {
        if ( mxIsNaN(Dr.s(row,xr+dp)) ) { //uniquness constraint
          d = xl-(xr+dp);
          if (d>=dmin & d<=dmax) {
            if (seed_accept) {
              Dr.s(row,xr+dp) = d;
              D[ind] = d;
              W[ind] = c;
            }
            st.xl = xl;
            st.xr = xr+dp;
            st.row = row;
            st.c = c;
            SEEDS.push(st);
          }
        }
      }
    };
  
    //visualization only
    vis++;
    if (vis > vis_step) {
      M = D.convert2mx();
      mexCallMATLAB(0,NULL,1,&M,"imager");
      mxDestroyArray(M);
      vis = 0;
    }

  }

}

// growing -----------------------------------------------------------------

void MAIN::grow5() {

  unsigned vis=0;
  int xl,xr,row,dp;
  seed s,st;
  double c;
  int ind;
  mxArray *M; //only for visualization
  int d;

  //main loop
  while (!SEEDS.empty()) {
    s = SEEDS.top();
    SEEDS.pop();   //remove the processed seed


    //add the seed into the current disparity map
//     if (!i_seed_accept) {
//       if ( mxIsNaN(D.s(s.row,s.xl)) ) { 
//         D.s(s.row,s.xl) = s.xl - s.xr;
//         Dr.s(s.row,s.xr) = s.xl - s.xr;
//         W.s(s.row,s.xl) = s.c;
//       }
//     }

    //mexPrintf("%f ",s.c);

    //left
    xl = s.xl;
    xr = s.xr; 
    row = s.row;
    lo_left(xl,xr,row,c);
    ind = D.sub2ind(row,xl);
    if ( mxIsNaN(D[ind]) ) {
      if (c>c_thr) {
        if ( mxIsNaN(Dr.s(row,xr)) ) { //uniquness constraint
          d = xl-xr;
          if (d>=dmin & d<=dmax) {
            //          if (seed_accept) {
              Dr.s(row,xr) = d;
              D[ind] = d;
              W[ind] = c;
              //}
            st.xl = xl;
            st.xr = xr;
            st.row = row;
            st.c = c;
            SEEDS.push(st);
          }
        }
      }
    };

    //right
    xl = s.xl;
    xr = s.xr; 
    row = s.row;
    lo_right(xl,xr,row,c);
    ind = D.sub2ind(row,xl);
    if ( mxIsNaN(D[ind]) ) {
      if (c>c_thr) {
        if ( mxIsNaN(Dr.s(row,xr)) ) { //uniquness constraint
          d = xl-xr;
          if (d>=dmin & d<=dmax) {
            //if (seed_accept) {
              Dr.s(row,xr) = d;
              D[ind] = d;
              W[ind] = c;
              //}
            st.xl = xl;
            st.xr = xr;
            st.row = row;
            st.c = c;
            SEEDS.push(st);
          }
        }
      }
    };


    //up
    xl = s.xl;
    xr = s.xr; 
    row = s.row;
    lo_up(xl,xr,row,c);
    ind = D.sub2ind(row,xl);
    if ( mxIsNaN(D[ind]) ) {
      if (c>c_thr) {
        if ( mxIsNaN(Dr.s(row,xr)) ) { //uniquness constraint
          d = xl-xr;
          if (d>=dmin & d<=dmax) {
            //if (seed_accept) {
              Dr.s(row,xr) = d;
              D[ind] = d;
              W[ind] = c;
              //}
            st.xl = xl;
            st.xr = xr;
            st.row = row;
            st.c = c;
            SEEDS.push(st);
          }
        }
      }
    };
    

    //down
    xl = s.xl;
    xr = s.xr; 
    row = s.row;
    lo_down(xl,xr,row,c);
    ind = D.sub2ind(row,xl);
    if ( mxIsNaN(D[ind]) ) {
      if (c>c_thr) {
        if ( mxIsNaN(Dr.s(row,xr)) ) { //uniquness constraint
          d = xl-xr;
          if (d>=dmin & d<=dmax) {
            //if (seed_accept) {
              Dr.s(row,xr) = d;
              D[ind] = d;
              W[ind] = c;
              //}
            st.xl = xl;
            st.xr = xr;
            st.row = row;
            st.c = c;
            SEEDS.push(st);
          }
        }
      }
    };

  
    //visualization only
//     vis++;
//     if (vis > vis_step) {
//       M = D.convert2mx();
//       mexCallMATLAB(0,NULL,1,&M,"imager");
//       mxDestroyArray(M);
//       vis = 0;
//     }

  }

}



// ========================================================================

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  //input

  ARRAY_2D<double> iL(prhs[0]); 
  ARRAY_2D<double> iR(prhs[1]);
  
  ARRAY_2D<double> sL(prhs[2]);
  ARRAY_2D<double> sR(prhs[3]);

  ARRAY_2D<double> vL(prhs[4]);
  ARRAY_2D<double> vR(prhs[5]);

  ARRAY_2D<double> SEEDs(prhs[6]);

  double* tmp;
  int w,range;
  unsigned vis_step;
  double c_thr;
  double epsilon;
  double dmin, dmax;
  bool i_seed_accept;
  bool seed_accept;
  int grow_version;

  tmp = mxGetPr(prhs[7]); w = int(tmp[0]);
  tmp = mxGetPr(prhs[8]); range = int(tmp[0]);
  tmp = mxGetPr(prhs[9]); c_thr = tmp[0];
  tmp = mxGetPr(prhs[10]); vis_step = int(tmp[0]);
      if (tmp[0]>INT_MAX) vis_step = INT_MAX; else vis_step = int(tmp[0]);
  tmp = mxGetPr(prhs[11]); dmin = tmp[0]; dmax = tmp[1];
  tmp = mxGetPr(prhs[12]); epsilon = tmp[0];
  tmp = mxGetPr(prhs[13]); i_seed_accept = bool(tmp[0]);
  tmp = mxGetPr(prhs[14]); seed_accept = bool(tmp[0]);
  tmp = mxGetPr(prhs[15]); grow_version = int(tmp[0]);
  
  //output
  plhs[0] = mxCreateDoubleMatrix(iL.M, iL.N, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(iL.M, iL.N, mxREAL);
  plhs[2] = mxCreateDoubleMatrix(iR.M, iR.N, mxREAL);
  ARRAY_2D<double> D(plhs[0]);
  ARRAY_2D<double> W(plhs[1]);
  ARRAY_2D<double> Dr(plhs[2]);

  //end of interface

  MAIN p(iL,iR,sL,sR,vL,vR,SEEDs,D,Dr,W,w,range,c_thr,vis_step,dmin,dmax,
         epsilon, i_seed_accept, seed_accept);
  
  if (grow_version == 5) {
    p.grow5();
  }
  else {
    p.grow();
  }

}
  

/*

  [D,W] = dgrow_mex(Il,Ir,SEEDs, Sl,Sr,Vl,VR, w,range,c_thr,
                vis_step,searchrange,epsilon,
                init_seed_accept, seed_accept,grow_version)

   - Il,Ir...input images (rectified)
   - SEEDs...initial correspondences [xl,xr,row] (nx3 vector)
   - grow_version...growing version (1-basic, 5-symmetric [default]])

   ->D...disparity map    
   ->W...correlation map
   ->Dr..right camera disparity map

*/

