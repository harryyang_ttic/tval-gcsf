/*_

 $Source$
 $Id$

 Matlab 7.2 mex module

 M = smatchingl(data, option, value, option, value, ...)

     Stereo matching on a list of match hypotheses. SSK algorithm.

     parameter description
  
       data - match hypotheses. Either
                (i,j,c,dc) per columns, where (i,j) is a putative match, 
                c is its image similarity and dc is the width of similarity
                interval, or
                (i,j,k,c,dc) per columns where (i|k, j|k) is a putative
                 match and the rest is the same. Use this version for an
                efficient solution of many matching problems at once,
                the problem is indexed by k. The procedure essentially
                runs one matching per k. The segments of same k in the
                data array must be contiguous, if not, the problem k would
                be split to smaller sub-problems.

          M - resulting matching as indices to data

      Options:

 'problemsize' - size of the matching problem in the form [maxi,maxj],
                 where maxi is the largest 1-based index i in hypothesis 
                 (i,j,c,dc) and maxj is the largest 1-based index j. 
                 If known, this will save one pass through the data. 

     'zonegap' - width of the gap in X-zone around a pair, default 0

  (c) Radim Sara (sara@cmp.felk.cvut.cz) FEE CTU Prague, 12 Jul 07

  This is free software; you can redistribute it and/or modify it. It is
  distributed WITHOUT ANY IMPLIED OR EXPRESSED WARRANTY and WITHOUT ANY
  FURTHER SUPPORT for the desired and/or other purpose. See README file in the
  main distribution.

 $Log$
_*/



#include <string.h>
#ifdef _WIN32
#define strcasecmp _strcmpi
#define sprintf sprintf_s
#endif
#include <time.h>
#include <mex.h>

#include "myerror.h"
#include "ejournal.h"
#include "heap.h"

/* Speeded-up functions. Assumes non-empty non-implicit heap */
#define HeapRootKeyQuick(h) ((h).list[0].key)
#define HeapRootBlockQuick(h) ((h).list[0].block)


#ifndef CLOCKS_PER_SEC
#define CLOCKS_PER_SEC 100
#endif

#ifndef min
#define min(a,b) ((a)<(b)?(a):(b))
#endif

#define DEBUG 0
#define debugPrintf if (DEBUG) mexPrintf

static struct {
 clock_t start;        /* auxiliary                                   */

 clock_t init;
 clock_t overhead;     /* allocation, initialization, destruction     */
 clock_t matching;
 clock_t total;        /* total time                                  */

 clock_t initT;
 clock_t overheadT;
 clock_t matchingT;
 clock_t totalT;

} T; /* for timing purposes */

/* ______________________ MEMORY MANAGEMENT ______________________ */


/* whether allocated memory is persistent. */
#define PERSMEM 

static struct {
 int ispersistent;
 int rows;
 int cols;

 int Msiz;
} pSize = {0,0,0,0};


static void *mxPersCalloc(size_t n, size_t size)
/* allocates persistent memory */
{
 void *p = mxCalloc(n,size);
#ifdef PERSMEM
 mexMakeMemoryPersistent(p);
#endif
 return p;
}

static void *mxPersMalloc(size_t n)
{
 void *p = mxMalloc(n);
#ifdef PERSMEM
 mexMakeMemoryPersistent(p);
#endif
 return p;
}

static void *mxPersRealloc(void *ptr, size_t size)
{
 void *p = mxRealloc(ptr,size);
#ifdef PERSMEM
 mexMakeMemoryPersistent(p);
#endif
 return p;
}

/* _____________________ UNION OF X(D) ZONES _____________________ */


typedef struct {

 int *rbeg, *rend;
 int *cbeg, *cend;
 int g;    /* zone gap */
 int m, n; /* size of the zone m - No of rows, n - No of columns */
} XdZoneT;

/* GNU C */

#if __GNUC__
#define INLINE static /*inline*/
#endif

/* Microsoft C :( */

#ifdef _MSC_VER
#define INLINE static __inline
#endif

static XdZoneT allocXd(int m, int n, int g)
/* sets zone size m, n in the struct but this is later re-set by
   initXd */
{
 XdZoneT Z;

 Z.m = m;
 Z.n = n;
 Z.g = g;

 Z.rbeg = mxPersCalloc(m, sizeof(int));
 Z.rend = mxPersCalloc(m, sizeof(int));

 Z.cbeg = mxPersCalloc(n, sizeof(int));
 Z.cend = mxPersCalloc(n, sizeof(int));

 return Z;
}

static void freeXd(XdZoneT Z)
{
 mxFree(Z.cend);
 mxFree(Z.cbeg);
 mxFree(Z.rend);
 mxFree(Z.rbeg);
}

static void initXd(XdZoneT *Z, int m, int n, int g)
{
 int i, j;
 
 Z->m = m;
 Z->n = n;
 Z->g = g;

 for(i=0; i<m; i++) Z->rbeg[i] = 0;
 for(i=0; i<m; i++) Z->rend[i] = n-1;
 for(j=0; j<n; j++) Z->cbeg[j] = 0;
 for(j=0; j<n; j++) Z->cend[j] = m-1;
}

INLINE int isInXd(XdZoneT Z, int u, int v)
{
#if (DEBUG>0)
 if ( u < 0 || Z.m-1 < u || v < 0 || Z.n-1 < v )
  Error(10,"Zone index (%i, %i) out of range", u, v);
#endif
 return v < Z.rbeg[u] || Z.rend[u] < v || u < Z.cbeg[v] || Z.cend[v] < u;
}

INLINE int isColumnEmptyXd(XdZoneT Z, int v)
{
 if (Z.cbeg[v] > Z.cend[v])
  printf("col empty\n");

 return Z.cbeg[v] > Z.cend[v];
}

INLINE void updateXd(XdZoneT *Z, int u, int v)
{
#if (DEBUG>0)
 if ( u < 0 || Z->m-1 < u || v < 0 || Z->n-1 < v )
  Error(10,"Zone index (%i, %i) out of range", u, v);
#endif
 if (Z->rbeg[u] < v - Z->g) Z->rbeg[u] = v - Z->g; /* max(rbeg,v-g) */
 if (Z->rend[u] > v + Z->g) Z->rend[u] = v + Z->g; /* min(rend,v+g) */

 if (Z->cbeg[v] < u - Z->g) Z->cbeg[v] = u - Z->g; /* max(cbeg,u-g) */
 if (Z->cend[v] > u + Z->g) Z->cend[v] = u + Z->g; /* min(cend,u+g) */
}

#if (DEBUG>0)
#define isInR(Xssk,u,v)                                                 \
 ((Xssk.Rc[v]!=u || Xssk.Rr[u]!=v) && (Xssk.Rr[u]!=-1 || Xssk.Rc[v]!=-1))

#define updateR(Xssk,u,v) Xssk.Rr[u] = v; Xssk.Rc[v] = u;

#define isInD(Xssk,u,v) (Xssk.Dr[u] || Xssk.Dc[v])

#define updateD(Xssk,u,v) Xssk.Dr[u] = Xssk.Dc[v] = 1;
#endif

/* ___________________ GLOBAL CONTROL VARIABLE ___________________ */


static struct { /* global struct for X-SSK */

 int nd;         /* number of input data       */
 double *x;      /* pointer to input data      */

 int maxr, maxc; /* problem size               */
 int Xgap;       /* X-zone gap                 */
 int n;          /* number of matches in M     */
 double **M;     /* matching                   */
 int Msiz;       /* maximum possible size of M */
 int probNo;     /* the number of independent matching problems */

 XdZoneT R;      /* red participants */
 XdZoneT D;      /* participants marked for deletion */

#if (DEBUG>0)
 int *Rc, *Rr,       /* red participants */
     *Dc, *Dr;       /* participants marked for deletion */
#endif

 HeapT U;            /* auxiliary heap of waiting vertices */

 HeapT *cHeap;       /* array of heaps: cHeap[i] is a heap for column i of
			correlation table */
 helementT **cHeapA; /* heap contents of cHeap, cHeapA[i] is contents for
			cHeap[i] */
 HeapT colsH;        /* the heap of column heaps */
 helementT *colsHA;  /* heap contents of colsH */

} Xssk;




static int determineProblemSize(int dsiz)
/* Determine the size of the problem. Returns 1 if the new size is the
   same or smaller than the persistent size  */
{

 if (Xssk.maxr <= 0) /* we have not yet initialized it */
  {
   int i;
   Xssk.maxr = Xssk.maxc = 0;
   for(i=0; i<Xssk.nd; i++)
    {
     if ((int)Xssk.x[dsiz*i+0] > Xssk.maxr)
      Xssk.maxr = (int)Xssk.x[dsiz*i+0];
    }
   Xssk.maxr++; /* to accommodate all hypotheses 0:maxc+1 */
   
   for(i=0; i<Xssk.nd; i++)
    {
     if ((int)Xssk.x[dsiz*i+1] > Xssk.maxc)
      Xssk.maxc = (int)Xssk.x[dsiz*i+1];
    }
   Xssk.maxc++;
  }

 if (dsiz == 5)
  {
   int i;
   /* count the number of matching problems in input data */
   for(i=0; i<Xssk.nd; i++)
    {
     if ((int)Xssk.x[dsiz*i+2] > Xssk.probNo)
      Xssk.probNo = (int)Xssk.x[dsiz*i+2];
    }
  }
 
 debugPrintf("%i matching problems\n", Xssk.probNo);

 /* maximum expected size of matching */
 Xssk.Msiz = Xssk.probNo*(2*Xssk.Xgap + 1)*min(Xssk.maxc,Xssk.maxr);

 if ( (Xssk.maxr <= pSize.rows)
      && (Xssk.maxc <= pSize.cols)
      && (Xssk.Msiz <= pSize.Msiz ) )
  {
   debugPrintf("Size %i x %i /%i (same or smaller than persistent)\n",
               Xssk.maxr, Xssk.maxc, Xssk.Msiz);
   return 1;
  }
 else
  {
   debugPrintf("Size %i x %i (persistent: %i x %i)\n",
               Xssk.maxr, Xssk.maxc, pSize.rows, pSize.cols);
   return 0;
  }
}


static void allocSSK_X(void)
/* Allocates matching structures. It is assumed allocation procedures
   clear the memory, so that we do not have to initialize zeros. */
{
 int j;

 Xssk.M = mxPersCalloc(Xssk.Msiz, sizeof(double*));

 Xssk.D = allocXd(Xssk.maxr, Xssk.maxc, Xssk.Xgap);
 Xssk.R = allocXd(Xssk.maxr, Xssk.maxc, Xssk.Xgap);

#if (DEBUG>0)
 Xssk.Rc = mxPersCalloc(Xssk.maxc, sizeof(int));
 Xssk.Rr = mxPersCalloc(Xssk.maxr, sizeof(int));
 Xssk.Dc = mxPersCalloc(Xssk.maxc, sizeof(int));
 Xssk.Dr = mxPersCalloc(Xssk.maxr, sizeof(int));
#endif

 Xssk.colsHA = mxPersCalloc(Xssk.maxc, sizeof(helementT));
 Xssk.cHeap = mxPersCalloc(Xssk.maxc, sizeof(HeapT));
 Xssk.cHeapA = mxPersCalloc(Xssk.maxc, sizeof(helementT*));

 for(j=0; j<Xssk.maxc; j++)
  Xssk.cHeapA[j] = mxPersCalloc(Xssk.maxr, sizeof(helementT));
}

static void freeSSK_X(int maxc)
/* destroys matching structures */
{
 int j;

 for(j=0; j<maxc; j++)
  FreeH(&Xssk.cHeap[j], NULL); /* frees all cHeapA[j] */ 

 mxFree(Xssk.cHeapA);
 mxFree(Xssk.cHeap);
 FreeH(&Xssk.colsH, NULL); /* takes care of freeing Xssk.colsHA */

 freeXd(Xssk.R);
 freeXd(Xssk.D);

#if (DEBUG>0)
 mxFree(Xssk.Dr);
 mxFree(Xssk.Dc);
 mxFree(Xssk.Rr);
 mxFree(Xssk.Rc);
#endif

 mxFree(Xssk.M);
}


/* _________________ PROBLEM (RE-)INITIALIZATION _________________ */


static void initSSK_X(int starti, int stopi, int psize)
/* Initializes pre-allocated matching structures. */
{
 int i,j;
 int added;

 /* clear heaps */

 InitH(&Xssk.U, max_rooted);
 InitH(&Xssk.colsH, max_rooted);

 for(j=0; j<Xssk.maxc; j++)
  {
   Xssk.cHeap[j].n = 0; 
   InitH(Xssk.cHeap+j, max_rooted);
  }



 for(i=starti; i<=stopi; i++) /* for each match hypothesis */
  {
   int v = (int)Xssk.x[psize*i+1];
   int n = Xssk.cHeap[v].n;
   if (v > Xssk.maxc || n > Xssk.maxr)
    Error(10,"Match candidate out of range given by problem size");

   Xssk.cHeapA[v][n].key = Xssk.x[psize*i+psize-2];
   Xssk.cHeapA[v][n].block = Xssk.x+psize*i;
   Xssk.cHeap[v].n++;
  }

 /* construct heap of heaps from collected data */
 added = 0;
 for(j=0; j<Xssk.maxc; j++)
  {
   ConsH(Xssk.cHeap+j, Xssk.cHeapA[j], Xssk.cHeap[j].n);
   if ( !IsEmptyH(Xssk.cHeap[j]) )
    {
     Xssk.colsHA[added].key = HeapRootKeyQuick(Xssk.cHeap[j]);
     Xssk.colsHA[added].block = Xssk.cHeap+j;
     added++;
    }
  } 

 ConsH(&Xssk.colsH, Xssk.colsHA, added);

#if (DEBUG>0)
 /* initialize D and R */
 for(i=0; i<Xssk.maxr;i++) Xssk.Rr[i] =-1;
 for(j=0; j<Xssk.maxc;j++) Xssk.Rc[j] =-1;
 for(i=0; i<Xssk.maxr;i++) Xssk.Dr[i] = 0;
 for(j=0; j<Xssk.maxc;j++) Xssk.Dc[j] = 0;
#endif

 initXd(&Xssk.D, Xssk.maxr, Xssk.maxc, Xssk.Xgap);
 initXd(&Xssk.R, Xssk.maxr, Xssk.maxc, Xssk.Xgap);
}




/* ________________________ MAIN PROCEDURE ________________________ */


static void doSSK_X(int dsiz)
{
 /* the value of an element in Xssk.cHeapA[v] and in Xssk.U is a
    pointer to the input list of type (double*) and the value of an
    element in Xssk.colsH is of type (HeapT*) */

 while ( !IsEmptyH(Xssk.colsH) || !IsEmptyH(Xssk.U) )
  {
   if ( !IsEmptyH(Xssk.U) && (IsEmptyH(Xssk.colsH) ||
         HeapRootKeyQuick(Xssk.U) > HeapRootKeyQuick(Xssk.colsH)) )
    {
     int u, v;
     double *p;

     /* this effectively removes the column heap as a whole even if it
        is not empty: ??? */
#if (DEBUG>0)
     double c = ExtractRootH(&p, &Xssk.U);
#else
     ExtractRootH(&p, &Xssk.U);
#endif

     u = (int)p[0];
     v = (int)p[1];
 
     /* debugPrintf("in U: (%i,%i):%g\n",u,v,c); */

#if (DEBUG>0)
     if ( isInXd(Xssk.R,u,v) ^ isInR(Xssk,u,v) )
      mexPrintf("Membership in R zone inconsistent\n");
#endif
     if (isInXd(Xssk.R,u,v)) /* is red */
      {
       /* debugPrintf("\t is red\n"); */
       continue;
      }
#if (DEBUG>0)
     updateD(Xssk,u,v);
#endif
     updateXd(&Xssk.D, u, v);

     /* debugPrintf("including (%i,%i):%g\n",u,v,c); */

     /* the members of M are also in Xssk.D as D.beg[v] for all v
        where beg[v]<=end[v] */
#if (DEBUG>0)
     if (Xssk.n >= Xssk.Msiz)
      mexPrintf("Matching size %i exceeded %i\n", Xssk.n, Xssk.Msiz);
     else
      Xssk.M[Xssk.n++] = p; /* (u,v,c) belongs to M */
#else
     Xssk.M[Xssk.n++] = p; /* (u,v,c) belongs to M */
#endif
    }
   else
    {
     double *p, *paux, c, dc;
     int u,v;
     HeapT *h;

     h = HeapRootBlockQuick(Xssk.colsH);
     if (IsEmptyH(*h))
      Error(100, "Heap must not be empty");

     c = ExtractRootH(&p, h); /* removes from a column heap */
     u = (int)p[0];
     v = (int)p[1];
     dc = p[dsiz-1];

     /* debugPrintf("next: (%i, %i): %g - %g \n",u,v,c, dc); */

     /* the test on isColumnEmptyXd seems to never fire */
     if ( IsEmptyH(*h) /*|| isColumnEmptyXd(Xssk.D, v)*/ )
      ExtractRootH(&paux, &Xssk.colsH);
     else
      ChangeRootKeyH(&Xssk.colsH, HeapRootKeyQuick(*h));

#if (DEBUG>0)
     if ( isInXd(Xssk.D, u, v) ^ isInD(Xssk,u,v) )
      mexPrintf("Membership in D-zone inconsistent\n");
#endif

     if ( isInXd(Xssk.D, u, v) ) /* ( isInD(Xssk,u,v) ) */
      {
       /* debugPrintf("\t is deleted\n"); */
       continue;
      }

#if (DEBUG>0)
     if ( isInXd(Xssk.R, u, v) ^ isInR(Xssk,u,v) )
      mexPrintf("Membership in R-zone inconsistent\n");
#endif
     if ( !isInXd(Xssk.R, u, v) ) /* ( !isInR(Xssk,u,v) ) */
      {
       /*debugPrintf("inserting to Q (%i,%i):%g\n",u,v,c-dc);*/
       InsertH(&Xssk.U, c-dc, p);
       /*debugPrintf("\t inserted in U as (%i,%i):%g, is not red\n",u,v,c-dc);*/
      }
     /*else
       debugPrintf("\t is red\n"); */

#if (DEBUG>0)
     updateR(Xssk,u,v); 
#endif
     updateXd(&Xssk.R, u, v);
    }
  }

 /* debugPrintf("doSSK_X finished\n");*/
}



/* ___________________________ MATLAB INTERFACE ___________________________ */

static void parseOptions(int nrhs, const mxArray *prhs[])
{
 int curopt = 0; 
 while (curopt < nrhs)
  {
   char opt[32];
   int failure;

   if ( !mxIsChar(prhs[curopt]) ) /* skip non-string parameters */
    {
     curopt++;
     continue;
    }

   failure = mxGetString(prhs[curopt], opt, sizeof(opt));
   if (failure)
    Error(1,"Internal: buffer too small (%i bytes)", sizeof(opt));

   if (strcasecmp(opt, "problemsize") == 0)
    {
     double *p;
     curopt++;
     if (curopt >= nrhs) 
      Error(2, "Not enough parameters for option %s", opt);

     if (mxGetNumberOfElements(prhs[curopt]) != 2)
      Error(2, "Problem size must be a vector of length two");

     p = mxGetPr(prhs[curopt]);
     Xssk.maxr = (int)p[0];
     Xssk.maxc = (int)p[1];
     if (Xssk.maxr<=0 || Xssk.maxc<=0)
      Error(2, "Problem sizes must be greater than zero");
    }
   else if (strcasecmp(opt, "zonegap") == 0)
    {
     int Xgap;
     curopt++;
     if (curopt >= nrhs) 
      Error(2, "Not enough parameters for option %s", opt);

     Xgap = (int)mxGetScalar(prhs[curopt]);
     if (Xgap < 0)
      Error(2, "X-zone gap must be a positive integer");

     Xssk.Xgap = Xgap;
    }
   else
    Message(0, "Unknown option: %s", opt);

   curopt++;
  }
}

static void LocalError(int c)
{
 static char sbuf[64];

 sprintf(sbuf, "Error code: %i", c);
 mexErrMsgTxt(sbuf);
}


static void mxExitFcn(void)
/* this is called upon module clear or Matlab terminates */
{
 if (pSize.ispersistent)
  {
   printf("smatchingl: Releasing persistent memory for %i x %i problem\n",
          pSize.rows, pSize.cols);
   freeSSK_X(pSize.cols);
   pSize.ispersistent = 0;
  }
}

static void reallocWorkingMemory(int same)
{

#ifdef PERSMEM
 if (!same && pSize.ispersistent)
  {
   printf("releasing memory\n");
   freeSSK_X(pSize.cols);
   pSize.ispersistent = 0;
  }
 if (!pSize.ispersistent)
  {
   /* the first call of the module after loading ends up here */
   EjrnlPrintf("allocating memory for %i x %i problem", Xssk.maxr,Xssk.maxc);
   allocSSK_X();
   pSize.rows = Xssk.maxr;
   pSize.cols = Xssk.maxc;
   pSize.Msiz = Xssk.Msiz;
   pSize.ispersistent = 1;
  }
#else
 allocSSK_X();
 pSize.rows = Xssk.maxr;
 pSize.cols = Xssk.maxc;
 pSize.Msiz = Xssk.Msiz;
 pSize.ispersistent = 0;
#endif
}

static void freeWorkingMemory(void)
{
#ifdef PERSMEM
 if (!pSize.ispersistent)
  {
   printf("releasing memory (at end)\n");
   freeSSK_X(pSize.cols);
  }
#else
 freeSSK_X(Xssk.maxc);
#endif

 FreeH(&Xssk.U, NULL); 
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
/*  M = smatchingl(data, option, value, option, value, ...) */
{
 double *p;
 int i;
 int mytckpersec = CLOCKS_PER_SEC;
 int secpermin = 60;
 double t;
 int same; /* is memory the same as the persistent memory? */
 int starti, stopi, probNo;


 /* registers memory management functions for heaps: */
 Hmm(mxPersMalloc,mxPersRealloc,mxFree);


 SetupLogFile("stderr");
 CallAtError(LocalError);
 EjournalCatch();

#ifdef PERSMEM
 mexAtExit(mxExitFcn);
#endif

 if (nrhs < 1)
  Error(2, "Not enough arguments");

  /* default parameter values */
 Xssk.n = 0;
 Xssk.nd = mxGetN(prhs[0]);
 Xssk.x = mxGetPr(prhs[0]);
 Xssk.maxr = Xssk.maxc = -1;
 Xssk.Xgap = 0;
 Xssk.probNo = 1;

 parseOptions(nrhs-1, &prhs[1]);

 T.init = T.overhead = T.matching = T.total = clock();
 T.initT = T.overheadT = T.matchingT = T.totalT = 0;

 switch ( mxGetM(prhs[0]) )
  {
  case 4:
   debugPrintf("Single problem\n");
   same = determineProblemSize(4);
   reallocWorkingMemory(same);

 /* this is the matching */
   T.overheadT += (T.init = clock()) - T.overhead; 
   initSSK_X(0, Xssk.nd-1, 4);
   T.initT += (T.matching = clock()) - T.init; 
   doSSK_X(4);
   T.matchingT += (T.overhead = clock()) - T.matching;


   plhs[0] = mxCreateDoubleMatrix(1,Xssk.n,mxREAL);
   p = mxGetPr(plhs[0]);
   for (i=0; i<Xssk.n; i++)
    p[i] = (Xssk.M[i]-Xssk.x)/4 + 1; /* this is a 1-based Matlab index */
 
   freeWorkingMemory();

   T.overheadT += clock()-T.overhead;
   T.totalT = clock()-T.total;

   break;


  case 5:
   same = determineProblemSize(5);
   reallocWorkingMemory(same);
   EjrnlPrintf("%i matching problems", Xssk.probNo);
   starti = 0;
   probNo = 0;


   while (starti < Xssk.nd)
    {
     stopi = starti;
     while ( (stopi < Xssk.nd) && 
             (int)Xssk.x[5*stopi+2] == (int)(int)Xssk.x[5*starti+2])
      stopi++;

     stopi--;

     T.overheadT += (T.init = clock()) - T.overhead; 
     initSSK_X(starti, stopi, 5);
     T.initT += (T.matching = clock()) - T.init; 
     doSSK_X(5);
     T.matchingT += (T.overhead = clock()) - T.matching;
     starti = stopi+1;
    }

   plhs[0] = mxCreateDoubleMatrix(1,Xssk.n,mxREAL);
   p = mxGetPr(plhs[0]);
   for (i=0; i<Xssk.n; i++)
    p[i] = (Xssk.M[i]-Xssk.x)/5 + 1; /* this is a 1-based Matlab index */
 
   freeWorkingMemory();

   T.overheadT += clock()-T.overhead;
   T.totalT = clock()-T.total;

   break;

  default:
  Error(2, "Match hypothesis list must be a 4 x n or a 5 x n array");
  }


 /* interface output */
 

 EjrnlPrintf("smatching timings");
 t = (double)T.matchingT/mytckpersec;
 EjrnlPrintf("          matching: %8.3f s (%4.1f min)",t,t/secpermin);
 t = (double)T.initT/mytckpersec;
 EjrnlPrintf("    initialization: %8.3f s (%4.1f min)",t,t/secpermin);
 t = (double)T.overheadT/mytckpersec;
 EjrnlPrintf("          overhead: %8.3f s (%4.1f min)",t,t/secpermin);
 t = (double)T.totalT/mytckpersec;
 EjrnlPrintf("    total CPU time: %8.3f s (%4.1f min)",t,t/secpermin);
}

/* end of smatchingl.c */

