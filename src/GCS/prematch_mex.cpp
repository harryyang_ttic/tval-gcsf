#include <mex.h>
#include <stack>  //DOES NOT REQUIRE SGI-STL
#include <list>
#include <arrays.h> //my library /home.dokt/cechj/matlab/cech/src/
                    //compile >> mex fname.c -I/home.dokt/cechj/matlab/cech/src/

using namespace std;  // std c++ libs implemented in std

const double NaN = mxGetNaN();

struct XYDW {
  int row; 
  int xl;
  int d;    //disparity
  double c; //correlation
};

class MAIN {
public:
  ARRAY_2D<double> iL;
  ARRAY_2D<double> iR;
  ARRAY_2D<double> sL;
  ARRAY_2D<double> sR;
  ARRAY_2D<double> vL;
  ARRAY_2D<double> vR;
  ARRAY_2D<double> fL;
  ARRAY_2D<double> fR;

  stack<XYDW> list_DW; //current solution
  
  int pm_w;        //pre-matching parametres
  double pm_c_thr;
  double pm_dmin,pm_dmax; //disparity searchrange

  double wcorr(int xl, int xr, int row, int w);
  void prematch();
  void pm_result(ARRAY_2D<double>& pm_D, ARRAY_2D<double>& pm_W);
        //convert internal represeantion into mxArray compatible repres.

  MAIN(ARRAY_2D<double>& iL_, ARRAY_2D<double> &iR_,
       ARRAY_2D<double>& sL_, ARRAY_2D<double> &sR_,
       ARRAY_2D<double>& vL_, ARRAY_2D<double> &vR_,
       ARRAY_2D<double>& fL_, ARRAY_2D<double> &fR_,
       int pm_w_, double pm_c_thr_,
       double pm_dmin_, double pm_dmax_);

  ~MAIN() {};

};

// -------------------------------------------------------------------------

MAIN::MAIN(ARRAY_2D<double>& iL_, ARRAY_2D<double> &iR_,
                  ARRAY_2D<double>& sL_, ARRAY_2D<double> &sR_,
                  ARRAY_2D<double>& vL_, ARRAY_2D<double> &vR_,
                  ARRAY_2D<double>& fL_, ARRAY_2D<double> &fR_,
                  int pm_w_, double pm_c_thr_,
                  double pm_dmin_, double pm_dmax_) {
  
  iL.assign(iL_);
  iR.assign(iR_);
  sL.assign(sL_);
  sR.assign(sR_);
  vL.assign(vL_);
  vR.assign(vR_);
  fL.assign(fL_);
  fR.assign(fR_);
    
  pm_w = pm_w_;
  pm_c_thr = pm_c_thr_;
  pm_dmin = pm_dmin_;
  pm_dmax = pm_dmax_;
}

// -------------------------------------------------------------------------

void MAIN::prematch() {
  
	XYDW xydw;
	double c;
	int row,c1,c2;
	//int i;
	int d;

	list<int> L1;
	list<int> L2;
	list<int>::iterator it1,it2;

	//stack<XYDW>::iterator slit;
    
	//i=0;

	for (row=1; row<=fL.M; row++) {
		//mexPrintf("%i: ",row);
		for (c1=1;c1<=fL.N; c1++) {
			if (!mxIsNaN(fL.s(row,c1))) L1.push_front(c1);
		}
		for (c2=1;c2<=fR.N;c2++) {
			if (!mxIsNaN(fR.s(row,c2))) L2.push_front(c2);
		}

		for (it1=L1.begin(); it1!=L1.end(); it1++ ) {
			c1 = *it1;
		    for (it2=L2.begin(); it2!=L2.end(); it2++ ) {
				c2 = *it2;
				d = c1 - c2; //disparity
				if (d >= pm_dmin && d <= pm_dmax) {//in the disparity searchrange
					c = wcorr(c1,c2,row,pm_w);
					if (c>pm_c_thr) {
						//mexPrintf("pm: %f\n",c);
						xydw.row = row;
						xydw.xl = c1;
						xydw.d = c1-c2;
						xydw.c = c;
						//list_DW.push_front(xydw);
						list_DW.push(xydw);
						//i++;
						//mexPrintf("%i \n",i);
					}
				}
				// mexPrintf("(%i,%i)",*it1,*it2);
			}
		}
		
		L1.clear();
		L2.clear();
	}

}

// -------------------------------------------------------------------------

void MAIN::pm_result(ARRAY_2D<double>& pm_D, ARRAY_2D<double>& pm_W) {

  int row;
  XYDW xydw;
 // slist<XYDW>::iterator slit;

  row=1;
  while (!list_DW.empty()) {
  //for (slit=list_DW.begin(); slit!=list_DW.end(); slit++) { 
    xydw = list_DW.top();
    //pm_D.s(row,1) = (*slit).row;
    //pm_D.s(row,2) = (*slit).xl;
    //pm_D.s(row,3) = (*slit).d;
    //pm_W.s(row,1) = (*slit).c;
    pm_D.s(row,1) = xydw.row;
    pm_D.s(row,2) = xydw.xl;
    pm_D.s(row,3) = xydw.d;
    pm_W.s(row,1) = xydw.c;
    row++;
	list_DW.pop();
  }
}

// -------------------------------------------------------------------------

double MAIN::wcorr(int xl, int xr, int row, int w) {
  int i,j;
  double sLR;
  double c;
  
  sLR = 0;
  for (i=-w; i<=w; i++) {
      for (j=-w; j<=w; j++) {
        sLR = sLR + iL.s(row+i,xl+j) * iR.s(row+i,xr+j);
      }
  }

  c = 2*(sLR - sL.s(row,xl)*sR.s(row,xr)) / (vL.s(row,xl) + vR.s(row,xr));
  if (c>1) c=0; //hack
  return c;
}

// =========================================================================

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{ 
  //input
  ARRAY_2D<double> iL(prhs[0]); 
  ARRAY_2D<double> iR(prhs[1]);
  
  ARRAY_2D<double> sL(prhs[2]);
  ARRAY_2D<double> sR(prhs[3]);

  ARRAY_2D<double> vL(prhs[4]);
  ARRAY_2D<double> vR(prhs[5]);

  ARRAY_2D<double> fL(prhs[6]);
  ARRAY_2D<double> fR(prhs[7]);

  double *tmp;
  int pm_w;
  double pm_c_thr;
  double pm_dmin, pm_dmax;

  tmp = mxGetPr(prhs[8]); pm_w = int(tmp[0]);
  tmp = mxGetPr(prhs[9]); pm_c_thr = tmp[0];
  tmp = mxGetPr(prhs[10]); pm_dmin = tmp[0]; pm_dmax = tmp[1];
  
  //main
  MAIN pm(iL,iR,sL,sR,vL,vR,fL,fR,pm_w,pm_c_thr,pm_dmin,pm_dmax);
  pm.prematch();

  //output
  int matched_points_number = pm.list_DW.size();
  plhs[0] = mxCreateDoubleMatrix(matched_points_number, 3, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(matched_points_number, 1, mxREAL);
  ARRAY_2D<double> pm_D(plhs[0]);
  ARRAY_2D<double> pm_W(plhs[1]);

  pm.pm_result(pm_D,pm_W);

}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*

  [D,W] = prematch_mex(Il,Ir, Sl,Sr,Vl,Vr, Fl,Fr, w,c_thr,searchrange)

   - Il,Ir...input images (rectified)
   - Sl,Sr,Vl,Vr...sum and variances for MNCC computation
   - Fl,Fr...detected feature points,NaN where not detected

   ->D...list of disparity points [u,v,d] per row
   ->W...list of corresponding correlation [w] per row

*/
