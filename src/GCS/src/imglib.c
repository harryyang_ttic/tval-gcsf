/* IMAGE PROCESSING LIBRARY FOR MATLAB   */
/* author: R. Sara                       */
/* last modification: April 1, 1997      */

#include "imglib.h"
#include <math.h>
#include <float.h>  /* for DBL_EPSILON */
#include <stdio.h>  /* for sprintf()   */
#include <memory.h> /* for memcpy()    */
#include "myerror.h"

#ifndef NULL
#define NULL 0
#endif

#define SQR(x) ((x)*(x))
#ifndef min
#define min(x,y) (((x)<(y))?(x):(y))
#endif
#ifndef max
#define max(x,y) (((x)>(y))?(x):(y))
#endif

int ImGetK(ImageT im)
 /* return 3rd dimension size, returns 1 in 2-D matrices */
{
 int d = mxGetNumberOfDimensions(im.im);
 const int *sz = mxGetDimensions(im.im);
 return (d<=2) ? 1 : sz[2];
}

/*________________________ WINDOWING _______________________*/

const WindowT EmptyWindow = {1, 1, 0, 0};

WindowT ConsWindow(int r1, int c1, int r2, int c2)
/************************************************/
{
 WindowT wnd;
 wnd.r1 = r1;
 wnd.c1 = c1;
 wnd.r2 = r2;
 wnd.c2 = c2;
 return wnd;
}

int SameWindowSizeQ(WindowT wnd1, WindowT wnd2)
/*********************************************/
{
 return ((wnd1.r2 - wnd1.r1) == (wnd2.r2 - wnd2.r1)) &&
        ((wnd1.c2 - wnd1.c1) == (wnd2.c2 - wnd2.c1));
}

WindowT FullWindow(ImageT img)
/****************************/
/* returns full image window */
{
 WindowT wnd;
 wnd.r1 = 0;
 wnd.c1 = 0;
 wnd.r2 = ImGetM(img)-1;
 wnd.c2 = ImGetN(img)/ImGetK(img)-1;
 return wnd;
}

int WindowInImageQ(ImageT img, WindowT wnd)
{
 return
  wnd.r1 >= 0 && wnd.c1 >= 0 && 
  (wnd.r2 <= ImGetM(img)-1) &&
  (wnd.c2 <= ImGetN(img)/ImGetK(img)-1);
}


WindowT WindowUp(WindowT wnd, int rows)
/*************************************/
/* idiom: SetWindow(img, WindowUp(img.wnd)) */
{
 wnd.r1 -= rows;
 wnd.r2 -= rows;
 return wnd;
}

WindowT WindowLeft(WindowT wnd, int cols)
/***************************************/
{
 wnd.c1 -= cols;
 wnd.c2 -= cols;
 return wnd;
}

WindowT ShrinkBottomSide(WindowT wnd, int rows)
/*********************************************/
{
 wnd.r2-=rows;
 return wnd;
}

WindowT ShrinkLeftSide(WindowT wnd, int cols)
/*********************************************/
{
 wnd.c1+=cols;
 return wnd;
}

WindowT ShrinkRightSide(WindowT wnd, int cols)
/*********************************************/
{
 wnd.c2-=cols;
 return wnd;
}

WindowT ShrinkTopSide(WindowT wnd, int rows)
/*********************************************/
{
 wnd.r1+=rows;
 return wnd;
}

WindowT IntersectWindows(WindowT wnd1, WindowT wnd2)
/**************************************************/
/* returns empty window if there is no intersection, see WindowEmptyQ() */
{
 WindowT res;
 res = EmptyWindow;

 if (WindowEmptyQ(wnd1) || WindowEmptyQ(wnd2))
  return res;

 res.r1 = max(wnd1.r1, wnd2.r1);
 res.c1 = max(wnd1.c1, wnd2.c1);
 res.r2 = min(wnd1.r2, wnd2.r2);
 res.c2 = min(wnd1.c2, wnd2.c2);

 return res;
}

WindowT CutOff(ImageT im, int mr, int mc)
 /************************************************/
{
 WindowT res;
 mr /= 2;
 mc /= 2;

 res = im.wnd;
 res.r1 += mr;
 res.r2 -= mr;
 res.c1 += mc;
 res.c2 -= mc;
 return res;
}


WindowT Extend(ImageT im, int mr, int mc)
 /************************************************/
{
 WindowT res;
 mr /= 2;
 mc /= 2;

 res = im.wnd;
 res.r1 -= mr;
 res.r2 += mr;
 res.c1 -= mc;
 res.c2 += mc;
 return IntersectWindows(res,FullWindow(im));
}

void GetWindowCenter(int *r, int*c, WindowT wnd)
{
 *r = (wnd.r1 + wnd.r2)/2;
 *c = (wnd.c1 + wnd.c2)/2;
}


void PrintWindow(FILE *out, WindowT w, const char *name)
{
 fprintf(out, "%s [%i, %i, %i, %i]\n", name, w.r1, w.r2, w.c1, w.c2);
}


/*____________________ CONSTRUCTOR, etc. __________________*/


ImageT ConsImage(Matrix *m)
{
 ImageT res;
 PixelT *p;
 int d;

 res.im = m;
 SetWindow(res, FullWindow(res));
 res.allocated = 0;

 d = mxGetNumberOfDimensions(m);
 if (d>3)
  Error(10,"Cannot work with matrix of dimension higher than 3");

 if (d<=2)
  {
   int i;
   int cols = mxGetN(m);
   int rows = mxGetM(m);

   p = (PixelT*)mxGetPr(m);
   res.pcol = (PixelT***)mxCalloc(1,sizeof(PixelT**));
   res.col = (PixelT**)mxCalloc(cols,sizeof(PixelT*));
   for(i=0; i<cols; i++)
    res.col[i] = p + i*rows;

   res.pcol[0] = res.col;
  }
 else
  {
   /* 3-D matrix */
   int k;
   const int *siz = mxGetDimensions(m);

   p = (PixelT*)mxGetPr(m);
   res.pcol = (PixelT***)mxCalloc(siz[2],sizeof(PixelT**));
   for(k=0; k<siz[2]; k++)
    {
     int j;
     res.pcol[k] = (PixelT**)mxCalloc(siz[1],sizeof(PixelT*));
     for(j=0; j<siz[1]; j++)
      res.pcol[k][j] = p + siz[0]*(siz[1]*k+j);
    }
   res.col = res.pcol[0];
  }

 return res;
}


#if !defined(V4_COMPAT) 

ImageT ImCreate(int rows, int cols, int complexFlag)
{
 ImageT res;
 int i;
 PixelT *p;

 res.allocated = 1;
 res.im =  mxCreateDoubleMatrix((signed)rows, (signed)cols, complexFlag);
 SetWindow(res, FullWindow(res));

 p = (PixelT*)mxGetPr(res.im);
 res.col = (PixelT**)mxCalloc(cols,sizeof(PixelT));
 for(i=0; i<cols; i++)
  res.col[i] = p + i*rows;

 res.pcol = (PixelT***)mxCalloc(1,sizeof(PixelT**));
 res.pcol[0] = res.col;

 return res;
}

ImageT ImCreate3D(int rows, int cols, int planes, int complexFlag)
{
 ImageT res;
 int k;
 PixelT *p;
 int siz[3];

 siz[0] = rows;
 siz[1] = cols;
 siz[2] = planes;

 res.allocated = 1;
 res.im = mxCreateNumericArray(3, siz, mxDOUBLE_CLASS, complexFlag);

 SetWindow(res, FullWindow(res));

 p = (PixelT*)mxGetPr(res.im);
 res.pcol = (PixelT***)mxCalloc(siz[2],sizeof(PixelT**));
 for(k=0; k<siz[2]; k++)
  {
   int j;
   res.pcol[k] = (PixelT**)mxCalloc(siz[1],sizeof(PixelT*));
   for(j=0; j<siz[1]; j++)
    res.pcol[k][j] = p + siz[0]*(siz[1]*k+j);
  }
 res.col = res.pcol[0];

 /* res.col = (PixelT**)mxCalloc(cols,sizeof(PixelT));
    for(i=0; i<cols; i++)
    res.col[i] = p + i*rows; */

 return res;
}



void ImFree(ImageT im)
{
 int i;

 for(i=0; i<ImGetK(im); i++) mxFree(im.pcol[i]);

 mxFree(im.pcol);
 if (im.allocated) mxDestroyArray(im.im);
}

#else

ImageT ImCreate(int rows, int cols, int complexFlag)
{
 ImageT res;
 int i;
 PixelT *p;

 res.allocated = 1;
 res.im =  mxCreateFull(rows, cols, complexFlag);
 SetWindow(res, FullWindow(res));

 p = (PixelT*)mxGetPr(res.im);
 res.col = (PixelT**)mxCalloc(cols,sizeof(PixelT));
 for(i=0; i<cols; i++)
  res.col[i] = p+rows;

 return res;
}

void ImFree(ImageT im)
{
 mxFree(im.col);
 if (im.allocated) mxFreeMatrix(im.im);
}
#endif



/*_____________________IMAGE ARITHMETICS_____________________*/

ImageT SumImages(ImageT *dst, ImageT src1, ImageT src2, WTransfT transf)
/**********************************************************************/
/* does not check matrix size consistency                             */
{
 /* PixelT *src1P = ImGetPr(src1);
    PixelT *src2P = ImGetPr(src2);
    PixelT *dstP  = ImGetPr(*dst); */

 PixelT ***src1P = ImGetAA(src1);
 PixelT ***src2P = ImGetAA(src2);
 PixelT ***dstP = ImGetAA(*dst);

 /* int rows = ImGetM(src1); */
 int planes = ImGetK(src1);
 int p, r, c, dr, dc, s1r, s1c, s2r, s2c;

 switch (transf)
  {
    case intersect:
       dst->wnd = IntersectWindows(src1.wnd, src2.wnd);
       if ( !WindowInImageQ(src1, dst->wnd) || 
            !WindowInImageQ(src2, dst->wnd) ||
            !WindowInImageQ(*dst, dst->wnd) )
        Error(10, "Window out of image borders");

       for(p=0;p<planes;p++)
        FOR_WINDOW(dst->wnd, r, c)
         dstP[p][c][r] = src1P[p][c][r] + src2P[p][c][r];

       /* dstP[r+c*rows] = src1P[r+c*rows] + src2P[r+c*rows]; */

       break;

     case move:
       if (!(SameWindowSizeQ(src1.wnd, src2.wnd) && 
             SameWindowSizeQ(src1.wnd, dst->wnd)))
        Error(11, "Incompatible windows");

       if ( !WindowInImageQ(src1, dst->wnd) || 
            !WindowInImageQ(src2, dst->wnd) ||
            !WindowInImageQ(*dst, dst->wnd) )
        Error(10, "Window out of image borders");


       for(p=0;p<planes;p++)
        FOR_3_WINDOWS(dst->wnd, dr, dc, src1.wnd, s1r, s1c, src2.wnd, s2r, s2c)
         dstP[p][dc][dr] = src1P[p][s1c][s1r] + src2P[p][s2c][s2r];

       /*
         FOR_3_WINDOWS(dst->wnd, dr, dc, src1.wnd, s1r, s1c, src2.wnd, s2r, s2c)
         dstP[dr+dc*rows] = src1P[s1r+s1c*rows] + src2P[s2r+s2c*rows];
       */

       break;

    default:  
            Error(12, "Not Implemented");
  }

 return *dst;
}

ImageT SubImages(ImageT *dst, ImageT src1, ImageT src2, WTransfT transf)
/**********************************************************************/
/* does not check matrix size consistency                             */
{
 PixelT ***src1P = ImGetAA(src1);
 PixelT ***src2P = ImGetAA(src2);
 PixelT ***dstP = ImGetAA(*dst);

 int planes = ImGetK(src1);
 int p, r, c, dr, dc, s1r, s1c, s2r, s2c;

 switch (transf)
  {
    case intersect:
       dst->wnd = IntersectWindows(src1.wnd, src2.wnd);
       if ( !WindowInImageQ(src1, dst->wnd) || 
            !WindowInImageQ(src2, dst->wnd) ||
            !WindowInImageQ(*dst, dst->wnd) )
        Error(10, "Window out of image borders");

       for(p=0;p<planes;p++)
        FOR_WINDOW(dst->wnd, r, c)
         dstP[p][c][r] = src1P[p][c][r] - src2P[p][c][r];

       break;

     case move:
       if (!(SameWindowSizeQ(src1.wnd, src2.wnd) && 
             SameWindowSizeQ(src1.wnd, dst->wnd)))
        Error(11, "Incompatible windows");

       if ( !WindowInImageQ(src1, dst->wnd) || 
            !WindowInImageQ(src2, dst->wnd) ||
            !WindowInImageQ(*dst, dst->wnd) )
        Error(10, "Window out of image borders");

       for(p=0;p<planes;p++)
        FOR_3_WINDOWS(dst->wnd, dr, dc, src1.wnd, s1r, s1c, src2.wnd, s2r, s2c)
         dstP[p][dc][dr] = src1P[p][s1c][s1r] - src2P[p][s2c][s2r]; 

       break;

    default:  
            Error(12, "Not Implemented");
  }

 return *dst;
}


ImageT MulImages(ImageT *dst, ImageT src1, ImageT src2, WTransfT transf)
/**********************************************************************/
/* does not check matrix size consistency                             */
{
 PixelT ***src1P = ImGetAA(src1);
 PixelT ***src2P = ImGetAA(src2);
 PixelT ***dstP = ImGetAA(*dst);
 int planes = ImGetK(src1);
 int p, r, c, dr, dc, s1r, s1c, s2r, s2c;

 switch (transf)
  {
    case intersect:
       dst->wnd = IntersectWindows(src1.wnd, src2.wnd);

       if ( !WindowInImageQ(src1, dst->wnd) || 
            !WindowInImageQ(src2, dst->wnd) ||
            !WindowInImageQ(*dst, dst->wnd) )
        Error(10, "Window out of image borders");

       for(p=0;p<planes;p++)
        FOR_WINDOW(dst->wnd, r, c)
         dstP[p][c][r] = src1P[p][c][r] * src2P[p][c][r];

       break;

     case move:
       if (!(SameWindowSizeQ(src1.wnd, src2.wnd) && 
             SameWindowSizeQ(src1.wnd, dst->wnd)))
        Error(11, "Incompatible windows");

       if ( !WindowInImageQ(src1, dst->wnd) || 
            !WindowInImageQ(src2, dst->wnd) ||
            !WindowInImageQ(*dst, dst->wnd) )
        Error(10, "Window out of image borders");

       for(p=0;p<planes;p++)
        FOR_3_WINDOWS(dst->wnd, dr, dc, src1.wnd, s1r, s1c, src2.wnd, s2r, s2c)
         dstP[p][dc][dr] = src1P[p][s1c][s1r] * src2P[p][s2c][s2r];

       break;

    default:  
            Error(12, "Not Implemented");
  }

 return *dst;
}

ImageT SDivImages(ImageT *dst, ImageT src1, ImageT src2, WTransfT transf)
/**********************************************************************/
/* does not check matrix size consistency                             */
{
 PixelT ***src1P = ImGetAA(src1);
 PixelT ***src2P = ImGetAA(src2);
 PixelT ***dstP = ImGetAA(*dst);
 int planes = ImGetK(src1);
 int p, r, c, dr, dc, s1r, s1c, s2r, s2c;

 switch (transf)
  {
    case intersect:
       dst->wnd = IntersectWindows(src1.wnd, src2.wnd);
       if ( !WindowInImageQ(src1, dst->wnd) || 
            !WindowInImageQ(src2, dst->wnd) ||
            !WindowInImageQ(*dst, dst->wnd) )
        Error(10, "Window out of image borders");

       for(p=0;p<planes;p++)
        FOR_WINDOW(dst->wnd, r, c)
         dstP[p][c][r] = (fabs(src2P[p][c][r]) < DBL_EPSILON) ? 0 : \
                          src1P[p][c][r]/src2P[p][c][r];

       break;

     case move:
       if (!(SameWindowSizeQ(src1.wnd, src2.wnd) && 
             SameWindowSizeQ(src1.wnd, dst->wnd)))
        Error(11, "Incompatible windows");

       if ( !WindowInImageQ(src1, dst->wnd) || 
            !WindowInImageQ(src2, dst->wnd) ||
            !WindowInImageQ(*dst, dst->wnd) )
        Error(10, "Window out of image borders");

       for(p=0;p<planes;p++)
        FOR_3_WINDOWS(dst->wnd, dr, dc, src1.wnd, s1r, s1c, src2.wnd, s2r, s2c)
         dstP[p][dc][dr] = (fabs(src2P[p][s2c][s2r]) < DBL_EPSILON) ? 0 :\
                             src1P[p][s1c][s1r]/src2P[p][s2c][s2r];

       break;

    default:  
            Error(12, "Not Implemented");
  }

 return *dst;
}


ImageT MulImageConst(ImageT *dst, ImageT src, PixelT cnst)
/********************************************************/
{

 /* PixelT *srcP = ImGetPr(src);
    PixelT *dstP  = ImGetPr(*dst); */

 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);

 /* int rows = ImGetM(src); */
 int planes = ImGetK(src);
 int p, r, c;

 dst->wnd = src.wnd;
 if ( !WindowInImageQ(src, dst->wnd) || !WindowInImageQ(*dst, dst->wnd) )
  Error(10, "Window out of image borders");

 for(p=0;p<planes;p++)
  FOR_WINDOW(src.wnd, r, c)
   dstP[p][c][r] = srcP[p][c][r]*cnst;

 /*   dstP[r+c*rows] = srcP[r+c*rows]*cnst; */
 
 return *dst;
}

ImageT ReciprocImage(ImageT *dst, PixelT cnst, ImageT src)
/********************************************************/
{
 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);

 int planes = ImGetK(src);
 int p, r, c;

 dst->wnd = src.wnd;
 if ( !WindowInImageQ(src, dst->wnd) || !WindowInImageQ(*dst, dst->wnd) )
  Error(10, "Window out of image borders");

 for(p=0;p<planes;p++)
  FOR_WINDOW(src.wnd, r, c)
   dstP[p][c][r] = (fabs(srcP[p][c][r]) < DBL_EPSILON) ? 0 :\
                          cnst/srcP[p][c][r];

 return *dst;
}



ImageT AddImageConst(ImageT *dst, ImageT src, PixelT cnst)
/********************************************************/
{
 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);
 int planes = ImGetK(src);
 int p, r, c;

 dst->wnd = src.wnd;
 if ( !WindowInImageQ(src, dst->wnd) || !WindowInImageQ(*dst, dst->wnd) )
  Error(10, "Window out of image borders");
 
 for(p=0;p<planes;p++)
  FOR_WINDOW(src.wnd, r, c)
   dstP[p][c][r] = srcP[p][c][r] + cnst;
 
 return *dst;
}

ImageT InvImage(ImageT *dst, ImageT src)
/***************************************/
{
 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);
 int planes = ImGetK(src);
 int p, r, c;

 dst->wnd = src.wnd;
 if ( !WindowInImageQ(src, dst->wnd) || !WindowInImageQ(*dst, dst->wnd) )
  Error(10, "Window out of image borders");

 for(p=0;p<planes;p++)
  FOR_WINDOW(src.wnd, r, c)
   dstP[p][c][r] = -srcP[p][c][r];
 
 return *dst;
}

ImageT SetImageConst(ImageT *dst, PixelT cnst)
/********************************************/
{
 /* PixelT *dstP  = ImGetPr(*dst); */
 PixelT ***dstP = ImGetAA(*dst);
 int planes = ImGetK(*dst);
 int p, r, c;
 /* int rows = ImGetM(*dst); */

 if ( !WindowInImageQ(*dst, dst->wnd) )
  Error(10, "Window out of image borders");

 for(p=0;p<planes;p++)
  FOR_WINDOW(dst->wnd, r, c)
   dstP[p][c][r] = cnst;
 
 return *dst;
}

ImageT SetBorderConst(ImageT *dst, PixelT cnst)
/********************************************/
{
 int p,r,c;
 /* PixelT *dstP = ImGetPr(*dst);*/

 PixelT ***dstP = ImGetAA(*dst);
 int planes = ImGetK(*dst);
 int rows = ImGetM(*dst);
 int cols = ImGetN(*dst)/planes;

  /* the left strip */
 for(p=0;p<planes;p++)
  for(c=0; c<dst->wnd.c1; c++)
   for(r=0; r<rows; r++)
    dstP[p][c][r] = cnst;
 /*   dstP[r+c*rows] = cnst; */

  /* the right strip */
 for(p=0;p<planes;p++)
  for(c=dst->wnd.c2+1; c<cols; c++)
   for(r=0; r<rows; r++)
    dstP[p][c][r] = cnst;

  /* the top strip */
 for(p=0;p<planes;p++)
  for(c=dst->wnd.c1; c<=dst->wnd.c2; c++)
   for(r=0; r<dst->wnd.r1; r++)
    dstP[p][c][r] = cnst;

  /* the bottom strip */
 for(p=0;p<planes;p++)
  for(c=dst->wnd.c1; c<=dst->wnd.c2; c++)
   for(r=dst->wnd.r2+1; r<rows; r++)
    dstP[p][c][r] = cnst;

 return *dst;
}
 

ImageT SquareImage(ImageT *dst, ImageT src)
/*****************************************/
{
 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);
 int planes = ImGetK(src);
 int p, r, c;

 /* PixelT *srcP = ImGetPr(src);
    PixelT *dstP  = ImGetPr(*dst);
    int rows = ImGetM(src);
    int r, c; */

 dst->wnd = src.wnd;
 if ( !WindowInImageQ(src, dst->wnd) || !WindowInImageQ(*dst, dst->wnd) )
  Error(10, "Window out of image borders");

 for(p=0;p<planes;p++)
  FOR_WINDOW(src.wnd, r, c)
   dstP[p][c][r] = SQR(srcP[p][c][r]);

 /* dstP[r+c*rows] = SQR(srcP[r+c*rows]); */

 return *dst;
}

ImageT SquareRootImage(ImageT *dst, ImageT src, double Precision)
/****************************************************************/
{
 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);
 int planes = ImGetK(src);
 int p, r, c;

 dst->wnd = src.wnd;
 if ( !WindowInImageQ(src, dst->wnd) || !WindowInImageQ(*dst, dst->wnd) )
  Error(10, "Window out of image borders");

 for(p=0;p<planes;p++)
  FOR_WINDOW(src.wnd, r, c)
  {
   /* double v = srcP[r+c*rows]; */
   /* dstP[r+c*rows] = (fabs(v) < Precision) ? 0 : sqrt(v); */

   double v = srcP[p][c][r];

   dstP[p][c][r] = (fabs(v) < Precision) ? 0 : sqrt(v);
  }

 return *dst;
}

ImageT CopyImage(ImageT *dst, ImageT src)
/***************************************/
{
 PixelT *dstP = ImGetPr(*dst);
 PixelT *srcP = ImGetPr(src);

 memcpy(dstP, srcP, ImGetM(*dst)*ImGetN(*dst)*sizeof(PixelT));
 dst->wnd = src.wnd;
 return *dst;
}

/*__________________ CONVOLUTIONS ________________*/


ImageT CollapseBySum(ImageT *dst, ImageT src)
{
 PixelT **dstP = ImGetA(*dst); 
 PixelT ***srcP = ImGetAA(src);
 int planes = ImGetK(src); /* the image size */
 int r,c;

 dst->wnd = src.wnd;
 if ( !WindowInImageQ(src, dst->wnd) || !WindowInImageQ(*dst, dst->wnd) )
  Error(10, "Window out of image borders");

 FOR_WINDOW(src.wnd, r, c)
  {
   int p;
   dstP[c][r] = srcP[0][c][r];
   for(p=1;p<planes;p++)
    dstP[c][r] += srcP[p][c][r];
  }

 return *dst;
}

ImageT Boxing(ImageT *dst, ImageT src, int mr, int mc)
/* does not check if the matrices have the same size!  Does not check
 if the matrix is at least mr x mc large. Needs 4*N^2
 additions+subtractions. The input window markes valid data, the
 resulting window is shrunken by mr/2 mc/2 from each side */
{ 
 PixelT *srcP = ImGetPr(src);
 PixelT *dstP = ImGetPr(*dst); 
 int planes = ImGetK(src); /* the image size */
 int rows = ImGetM(src); 
 int cols = ImGetN(src)/planes; 
 int mr2 = mr/2; /* the mask is 2*mr2+1 times 2*mc2+1 large */
 int mc2 = mc/2; 
 PixelT sum, *push;
 PixelT *beg, *end, *totalEnd;
 int i, r, c, p;
 PixelT *aux;  /* intermediate result */
 WindowT wnd;

 aux = (PixelT *)mxCalloc(rows*cols, sizeof(PixelT)); /* :auxiliary variable */
 if (aux==NULL)
  mexErrMsgTxt("Out of memory");

  /* not to exceed matrix dimensions: */
/* wnd = ConsWindow(mr2, mc2, rows-mr2-1, cols-mc2-1);
 wnd = SetWindow(*dst, IntersectWindows(wnd, src.wnd)); */

 if ( !WindowInImageQ(src, src.wnd) || !WindowInImageQ(*dst, src.wnd) )
   Error(10, "Window out of image borders");

 dst->wnd = wnd = SetWindow(*dst, CutOff(src, mr, mc));
 if (WindowEmptyQ(wnd))
  Error(10, "Window is empty");

 /* STEP 1: do the boxing per COLUMNS */
 for(p=0; p<planes; p++)
  {
   for(c = wnd.c1-mc2; c <= (wnd.c2+mc2); c++)
    {
     beg = srcP + rows*cols*p + c*rows+wnd.r1-mr2;   
     /* :set to the beginning of column */
     end = beg;
     totalEnd = beg+wnd.r2-wnd.r1+mr-1;
     sum = 0;
     for(i=0; i<mr; i++, end++)    /* :sum over the first mask position */
      sum += *end;
     
     push = aux+c*rows+wnd.r1; /* :start writing from proper position */
     *push = sum;
     for(push++; end<=totalEnd; end++,beg++,push++)
      {
       sum -= *beg;
       sum += *end;
       *push = sum;
      }
    }

   /* STEP 2: do the boxing per ROWS */
   for(r = wnd.r1-mr2;  r <= (wnd.r2+mr2); r++)
    {
     beg = aux+r+(wnd.c1-mc2)*rows;   /* :set to the beginning of row */
     end = beg;
     totalEnd = beg+(wnd.c2-wnd.c1+mc-1)*rows;
     sum = 0;
     for(i=0; i<mc; i++, end+=rows)
      sum += *end;
     
     push = dstP + rows*cols*p + (beg-aux) + mc2*rows;
     *push = sum;
     for(push+=rows; end<=totalEnd; end+=rows, beg+=rows, push+=rows)
      {
       sum -= *beg;
       sum += *end;
       *push = sum;
      }
    }
  } /* end of cycle for all planes */

 mxFree(aux); /* return the allocated matrix */
 return *dst;
}



ImageT Convolution(ImageT *dst, ImageT src, ImageT msk)
/*****************************************************/
/* does not check if the mask has odd size */
{
 int p,r,c,mr,mc; /* image and mask position */
 int mr2 = (msk.wnd.r2 - msk.wnd.r1 + 1)/2;
 int mc2 = (msk.wnd.c2 - msk.wnd.c1 + 1)/2;
 int planes = ImGetK(src); /* the image size */

 /* int mrows = 1+2*mr2;
    int rows = ImGetM(src); 
    int cols = ImGetN(src)/planes; */

 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);

 /*
   PixelT *srcP = ImGetPr(src);
   PixelT *dstP = ImGetPr(*dst);
   PixelT *msP = ImGetPr(msk);
 */

 WindowT wnd;
/* WindowT wnd = ConsWindow(mr2, mc2, rows-mr2-1, cols-mc2-1);
 wnd = SetWindow(*dst, IntersectWindows(wnd, src.wnd)); */

 if ( !WindowInImageQ(src, src.wnd) || !WindowInImageQ(*dst, src.wnd) )
   Error(10, "Window out of image borders");

 wnd = SetWindow(*dst, CutOff(src, 2*mr2+1, 2*mc2+1));

 if (ImGetNumberOfDimensions(msk) == 3)
  {
   PixelT ***msP = ImGetAA(msk);
   /* 3D mask */
   for(p=0; p<planes; p++)
    for(c = wnd.c1; c<= wnd.c2; c++)
     for(r = wnd.r1; r<= wnd.r2; r++)
      {
       double sum = 0;
       for(mc = -mc2; mc <= mc2; mc++)
        for(mr = -mr2; mr <= mr2; mr++)
         sum += srcP[p][c+mc][r+mr]*msP[p][mc+mc2][mr+mr2];
       
       /* sum += srcP[r+mr+(c+mc)*rows]*msP[mr+mr2+(mc+mc2)*mrows]; */
       
       dstP[p][c][r] = sum;
      }
  }
 else
  {
   /* just 2-D mask */
   PixelT **msP = ImGetA(msk);

   for(p=0; p<planes; p++)
    for(c = wnd.c1; c<= wnd.c2; c++)
     for(r = wnd.r1; r<= wnd.r2; r++)
      {
       double sum = 0;
       for(mc = -mc2; mc <= mc2; mc++)
        for(mr = -mr2; mr <= mr2; mr++)
         sum += srcP[p][c+mc][r+mr]*msP[mc+mc2][mr+mr2];
       
       /* sum += srcP[r+mr+(c+mc)*rows]*msP[mr+mr2+(mc+mc2)*mrows]; */
       
       dstP[p][c][r] = sum;
      }
    }

 return *dst;
}


ImageT CentralDiffR(ImageT *dst, ImageT src)
/*******************************************/
/* central difference in row direction, "src" and "dst"
   may be the same data matrix */ 
{
 int p,r,c;
 WindowT wnd;
 int planes = ImGetK(src);
 /* int cols = ImGetN(src)/planes;
    int rows = ImGetM(src); */
 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);
 /* PixelT *srcP = ImGetPr(src);
    PixelT *dstP = ImGetPr(*dst); */

/* WindowT wnd = ConsWindow(1, 0, rows-2, cols);
 wnd = SetWindow(*dst, IntersectWindows(wnd, src.wnd)); */

 wnd = SetWindow(*dst, CutOff(src, 3, 1));
 if ( !WindowInImageQ(src, wnd) || !WindowInImageQ(*dst, wnd) )
   Error(10, "Window out of image borders");

 for(p=0; p<planes; p++)
  for(c=wnd.c1; c<=wnd.c2; c++)
   {
    /* double save = srcP[wnd.r1 - 1 + c*rows + p*rows*cols]; */
    double save = srcP[p][c][wnd.r1 - 1];

    for(r=wnd.r1; r<=wnd.r2; r++)
     {
      /* double savenew = srcP[r+c*rows];
         dstP[r+c*rows] = (srcP[r+1+c*rows] - save)/2;  */
      double savenew = srcP[p][c][r];
      dstP[p][c][r] = (srcP[p][c][r+1] - save)/2;  
      save = savenew;
     }
   }

 return *dst;
}

ImageT CentralDiffC(ImageT *dst, ImageT src)
/*******************************************/
/* central difference in column direction, "src" and "dst"
   may be the same data matrix */ 
{
 int r,c,p;
 WindowT wnd;
 int planes = ImGetK(src);
 PixelT ***srcP = ImGetAA(src);
 PixelT ***dstP = ImGetAA(*dst);

/* WindowT wnd = ConsWindow(0, 1, rows, cols-2);
 wnd = SetWindow(*dst, IntersectWindows(wnd, src.wnd)); */

 wnd = SetWindow(*dst, CutOff(src, 1, 3));
 if ( !WindowInImageQ(src, wnd) || !WindowInImageQ(*dst, wnd) )
   Error(10, "Window out of image borders");

 for(p=0; p<planes; p++)
  for(r=wnd.r1; r<=wnd.r2; r++)
   {
    /* double save = srcP[r + (wnd.c1-1)*rows]; */
    double save = srcP[p][wnd.c1-1][r];
    for(c=wnd.c1; c<=wnd.c2; c++)
     {
      /* double savenew = srcP[r+c*rows];
         dstP[r+c*rows] = (srcP[r+(c+1)*rows] - save)/2;  */

      double savenew = srcP[p][c][r];
      dstP[p][c][r] = (srcP[p][c+1][r] - save)/2;  
      save = savenew;
     }
   }

 return *dst;
}


ImageT SobelC(ImageT *dst, ImageT src)
/************************************/
{
 ImageT sobelS;
 PixelT *sobP;

 sobelS = ImCreate(3,1,imgREAL);
 sobP = ImGetPr(sobelS);

 sobP[0] = 1/(2+sqrt(2.0));
 sobP[1] = sqrt(2.0)/(2+sqrt(2.0));
 sobP[2] = 1/(2+sqrt(2.0));

 Convolution(dst, src, sobelS); /* :this is smoothing along columns */
 CentralDiffC(dst, *dst);       /* :this is central difference along rows */

 SetBorderConst(dst, 0.0);
 ImFree(sobelS);
 return *dst;
}

ImageT SobelR(ImageT *dst, ImageT src)
/************************************/
{
 ImageT sobelS;
 PixelT *sobP;

 sobelS = ImCreate(1,3,imgREAL);
 sobP = ImGetPr(sobelS);
 sobP[0] = 1/(2+sqrt(2.0));
 sobP[1] = sqrt(2.0)/(2+sqrt(2.0));
 sobP[2] = 1/(2+sqrt(2.0));

 Convolution(dst, src, sobelS); /* :this is smoothing along rows */
 CentralDiffR(dst, *dst);       /* :this is central difference along cols */

 SetBorderConst(dst, 0.0);
 ImFree(sobelS);
 return *dst;
}

