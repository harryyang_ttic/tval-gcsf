/*_
                                                                     
   myerror.c   useful routines standardizing reporting errors,         
               printing messages on screen and into a log-file.        
               For description, see myerror.h                          
                                                                       
   (c) Radim Sara, GRASP UPenn, 1996                                   
       modified by R. Sara, 11/1/1999                                  

_*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <myerror.h>
#ifdef _WIN32
#include <io.h> /* for unlink() */
#else
#include <unistd.h> /* for unlink() */
#endif

 /* auxiliary local variables : */

static char logFile[64] = "log.log";

void (*errorFunction)(int) = exit;

static struct  {
 char function[128];
 char file[128];
 int line;
} ErrorPoint;

static FILE *MessageChannel = NULL; /* NULL implies stderr */
static FILE *ErrorChannel = NULL; /* NULL implies stderr */

static int (*locPrintf)(const char*,...) = printf;

static char printBufferA[256] = "";
static char printBufferB[256] = "";

MessageFnT SetP(const char *function, const char *file, int line, MessageFnT fun)
{
 strncpy(ErrorPoint.function, function, sizeof(ErrorPoint.function));
 ErrorPoint.line = line;
 strncpy(ErrorPoint.file, file, sizeof(ErrorPoint.file));
 return fun;
}

static void LocalPrintf(const char *fmt, ...)
{
 va_list args;
 va_start(args, fmt);
 vsprintf(printBufferA, fmt, args);
 va_end(args);
 if (MessageChannel == NULL) MessageChannel = stderr;
 if (MessageChannel == stdout)
  locPrintf("%s", printBufferA);
 else
  fprintf(MessageChannel, "%s", printBufferA);
}


void FullMessage(int localize, const char *fmt, ...)
{
 va_list args;
 va_start(args, fmt);

 if (localize != 0)
  LocalPrintf("%s() in %s(%i): ", 
   ErrorPoint.function, ErrorPoint.file, ErrorPoint.line);

 vsprintf(printBufferB, fmt, args);
 LocalPrintf("%s\n", printBufferB);
 va_end(args);
}

void RedirectMessage(FILE *f)
{
 MessageChannel = f;
}

void FullLogFile(int localize, const char *fmt, ...)
{
 int closeit = 1;
 FILE *logfile;
 va_list args;
 va_start(args, fmt);

 if ( (logFile == NULL) || (logFile[0] == 0) )
  {
   va_end(args);
   return;
  }

 if (strcmp(logFile, "stderr") == 0)
  {
   logfile = stderr;
   closeit = 0;
  }
 else
  logfile = fopen(logFile, "a");

 if (MessageChannel==NULL) MessageChannel = stderr;
 if (logfile == NULL)
  {
   fprintf(MessageChannel, "Panic: cannot open log file for append\n");
   va_end(args);
   return;
  }

 if (localize != 0)
  fprintf(logfile, "%s() in %s(%i): ", 
   ErrorPoint.function, ErrorPoint.file, ErrorPoint.line);

 vfprintf(logfile, fmt, args);
 fprintf(logfile, "\n");

 if (closeit)
  if (fclose(logfile) != 0)
   fprintf(MessageChannel, "Panic: cannot close log file\n");

 va_end(args);
}

void SetupLogFile(const char *fname)
{
 unlink(logFile);  /* remove the old file; do not check if it has succeeded */

 if (fname == NULL)
  {
   *((char*)logFile) = 0;
   return;
  }

 if (*fname != 0)
  {
   strncpy((char *)logFile, fname, sizeof(logFile));
   unlink(logFile);  /* remove the new file if it exists */
  }
}

void FullError(int code, const char *fmt, ...)
{
 va_list args;
 va_start(args, fmt);

 LocalPrintf("Error: %s() in %s(%i): ", 
  ErrorPoint.function, ErrorPoint.file, ErrorPoint.line);

 vsprintf(printBufferB, fmt, args);
 LocalPrintf("%s\n", printBufferB);
 va_end(args);

  /* call the error function and exit if not done there */
 if (errorFunction != NULL)
   errorFunction(code);

 exit(code);
}

void CallAtError(void (*errorfun)(int))
{
 errorFunction = errorfun;
}

void RedirectError(FILE *f)
{
 ErrorChannel = f;
}

void RedirectPrintf(int (*newprintf)(const char*,...))
{
 locPrintf = newprintf;
}
