#include <stdarg.h> 
#include <mex.h>
#include "ejournal.h"

/* MM/CHANGED */
/*#if (MATLABVER < 6)
#define getVariable(a,b) mexGetArray(b,a)
#else*/
#define getVariable(a,b) mexGetVariable(a,b)
/*#endif*/

static int journalON = 0;

void EjournalCatch(void)
{
 int lvl;
 mxArray *jrnl;
 mxArray *out[1];
 mexCallMATLAB(1,out,0,NULL,"ejournalvl");
 lvl = (int)mxGetScalar(out[0]);
 mxDestroyArray(out[0]);

 jrnl = getVariable("global", "ejournalvar");
 if (jrnl == NULL)
  journalON = 0;
 else
  {
   mxArray *lev = mxGetField(jrnl, 0, "level");
   if (lev != NULL)
    {
     double dlvl = mxGetScalar(lev);
     if (mxIsInf(dlvl))
      journalON = 1;
     else
      journalON = (int)dlvl > lvl; /* corrected 28.5.2004 by RS */
     /* mex file is not considered a run level, we need to add a unity */
    }
  }
}

static char printBuffer[256] = "";

void EjrnlPrintf(const char *fmt, ...)
{
 va_list args;

 if (!journalON) return;
 va_start(args, fmt);
 vsprintf(printBuffer, fmt, args);
 va_end(args);
 printf("%s\n", printBuffer);
}

int EjrnlReport(void)
{
 return journalON;
}
