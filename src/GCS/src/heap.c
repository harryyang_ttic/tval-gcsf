/*_

  HEAP.C   minimum-rooted and maximum-rooted heap data structures

  Errors:

   1  ... Out of memory
   2  ... Expected pointer to a HeapT, got NULL
   3  ... Do not know how to insert an element in a heap of undefined type
   4  ... Heap is empty
   5  ... ExtractRootH(NULL,&h) is permitted for a heap with non-NULL elements
   6  ... Array block size too small/large

_*/

/*#include <malloc.h>*/
#include <stdlib.h>
#include <stdarg.h>
#include "heap.h"
#include "index.h"

#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

/* memory management */
static void* (*hmalloc)(size_t) = malloc;
static void (*hfree)(void*) = free;
static void* (*hrealloc)(void*,size_t) = realloc;

void Hmm(void* (*emalloc)(size_t),
         void* (*erealloc)(void*,size_t),
         void (*efree)(void*))
{
 hmalloc = emalloc;
 hrealloc = erealloc;
 hfree = efree;
}

void InitH(HeapT *h, HeapType type, ...)
{

 if (h==NULL)
  Error(2, "Expected pointer to a HeapT, got NULL");

 h->n = 0;
 h->null = 1;
 h->type = type;
 h->list = NULL;
 h->comp = NULL;
 if (type == implicit)
  {
   va_list ap;
   va_start(ap, type);
   h->comp = va_arg(ap,isGreaterT);
   va_end(ap);
  }
}



void FreeH(HeapT *h, void (*Free)(void*))
{
 int i;

 if (h==NULL)
  Error(2, "Expected pointer to a HeapT, got NULL");

 if (Free != NULL)
  {
   if ( (!h->null) && (h->list != NULL) )
    for(i=0; i<h->n; i++)
     Free(h->list[i].block);
  }

 if ( (h->list != NULL) )
  {
   hfree(h->list);
  }

 h->n = 0;
 h->type = undefined;
 h->list = NULL;
 h->null = 1;
 h->comp = NULL;
}

static void upheap_max(HeapT *h, long int k)
{
 helementT v;
 long int z = (k-1)>>1;

 v = h->list[k];
 while ( k>0 && h->list[z].key <= v.key)
  {
   h->list[k] = h->list[z];
   k = z;
   z = (k-1)>>1;
  }
 h->list[k] = v;
}

static void upheap_min(HeapT *h, long int k)
{
 helementT v;
 long int z = (k-1)>>1;

 v = h->list[k];
 while ( k>0 && h->list[z].key > v.key)
  {
   h->list[k] = h->list[z];
   k = z;
   z = (k-1)>>1;
  }
 h->list[k] = v;
}

static void upheap_implicit(HeapT *h, long int k)
{
 helementT v;
 long int z = (k-1)>>1;

 v = h->list[k];
 while ( k>0 && !h->comp(h->list[z].block, v.block) )
  {
   h->list[k] = h->list[z];
   k = z;
   z = (k-1)>>1;
  }
 h->list[k] = v;
}

void InsertH(HeapT *h, HeapKeyT key, void *elm)
{
 long int w;

 if (h==NULL)
  Error(2, "Expected pointer to a HeapT, got NULL");

 if (h->type == undefined)
  Error(3, "Do not know how to insert an element in a heap of undefined type");

 if (elm != NULL)
  h->null = 0;

 h->n++;

 if (h->list == NULL)
  h->list = (helementT*)hmalloc(h->n*sizeof(h->list[0]));
 else
  h->list = (helementT*)hrealloc(h->list, h->n*sizeof(h->list[0]));

 if (h->list == NULL)
  Error(1, "Out of memory");

 w = h->n-1;
 h->list[w].block = elm;
 h->list[w].key = key;

 switch (h->type)
  {
  case max_rooted:
   upheap_max(h, h->n-1);
   break;
  case min_rooted:
   upheap_min(h, h->n-1);
   break;
  case implicit:
   upheap_implicit(h, h->n-1);
   break;
  case undefined:
   Error(4, "Cannot perform insert on undefined heap type");
  }

}

static void downheap_max(HeapT *h, long int k)
{
 helementT v;
 long int j = (k<<1)+1;

 v = h->list[k];
 while (j < h->n-1)
  {
   if ( h->list[j].key < h->list[j+1].key ) j++;
   if ( v.key >= h->list[j].key ) break;

   h->list[k] = h->list[j];
   k = j;
   j = (k<<1)+1;
  }

 if (j==h->n-1 && v.key < h->list[h->n-1].key )
  {
   h->list[k] = h->list[h->n-1];
   k = h->n-1;
  }

 h->list[k] = v;
}

static void downheap_min(HeapT *h, long int k)
{
 helementT v;
 long int j = (k<<1)+1;

 v = h->list[k];
 while (j < h->n-1)
  {
   if ( h->list[j].key > h->list[j+1].key ) j++;
   if ( v.key <= h->list[j].key ) break;

   h->list[k] = h->list[j];
   k = j;
   j = (k<<1)+1;
  }
 if (j==h->n-1 && v.key > h->list[h->n-1].key )
  {
   h->list[k] = h->list[h->n-1];
   k = h->n-1;
  }

 h->list[k] = v;
}

void downheap_implicit(HeapT *h, long int k)
{
 helementT v;
 long int j = (k<<1)+1;

 v = h->list[k];
 while (j < h->n-1)
  {
   if ( !h->comp(h->list[j].block, h->list[j+1].block ) ) j++;
   if ( h->comp(v.block, h->list[j].block) ) break;

   h->list[k] = h->list[j];
   k = j;
   j = (k<<1)+1;
  }

 if (j==h->n-1 && !h->comp(v.block, h->list[h->n-1].block) )
  {
   h->list[k] = h->list[h->n-1];
   k = h->n-1;
  }

 h->list[k] = v;
}

HeapKeyT ExtractRootH(void **root, HeapT *h)
{
 HeapKeyT key;

 if (h==NULL)
  Error(2, "Expected pointer to a HeapT, got NULL");

 if (h->n <= 0)
  Error(4, "Heap is empty");

 if (!h->null && root == NULL)
  Error(5, 
   "ExtractRootH(NULL, &h) is not permitted for a heap with non-NULL elements");
 
 if (h->type == undefined)
  Error(3, 
   "Do not know how to extract an element from a heap of undefined type");

 if (root != NULL) *root = h->list[0].block;
 key = h->list[0].key;

 if (h->n == 1)
  {
   h->n = 0;
   return key;
  }

 h->n--;
 h->list[0] = h->list[h->n]; /* :move last element to the root */
 switch (h->type)
  {
  case max_rooted:
   downheap_max(h, 0);
   break;
  case min_rooted:
   downheap_min(h, 0);
   break;
  case implicit:
   downheap_implicit(h, 0);
   break;
  case undefined:
   Error(4, "Cannot perform extract root on undefined heap type");
  }
 return key;
}

HeapKeyT ChangeRootKeyH(HeapT *h, HeapKeyT key)
{
 if (h->type == implicit)
  Error(4, "Cannot change root key in implicit heap");

 if (h==NULL)
  Error(2, "Expected pointer to a HeapT, got NULL");

 if (h->n <= 0)
  Error(4, "Heap is empty");

 h->list[0].key = key;
 if (h->type == max_rooted) 
  downheap_max(h, 0);
 else
  downheap_min(h, 0);

 return h->list[0].key; 
}


HeapKeyT HeapRootKey(HeapT h)
{
 if (h.type == implicit)
  Error(4, "Cannot return root key for implicit heap");

 if (h.n > 0)
  return h.list[0].key;
 else
  Error(4, "Heap is empty");

 return 0;
}

void *HeapRootBlock(HeapT h)
{

 if (h.n > 0)
  return h.list[0].block;
 else
  Error(4, "Heap is empty");

 return 0;
}

void ConsH(HeapT *h, helementT *data, long n)
{
 long int k;

 h->n = n;
 h->list = data;

 h->null = 1;
 for(k=0; k<n; k++)
  if (data[k].block != NULL)
   {
    h->null = 0;
    break;
   }

 switch (h->type)
  {
  case max_rooted:
   for(k=(n+1)>>1; k>0; k--)
    downheap_max(h, k-1); 
   break;

  case min_rooted:
   for(k=(n+1)>>1; k>0; k--)
    downheap_min(h, k-1); 
   break;

  case implicit:
   for(k=(n+1)>>1; k>0; k--)
    downheap_implicit(h, k-1); 
   break;

  case undefined:
   Error(4, "Cannot construct undefined heap type");

  }
}
