#include <set>
#include <queue>
#include <mex.h>
#include <arrays.h> //my library /home.dokt/cechj/matlab/cech/src/
                    //compile >> mex fname.c -I/home.dokt/cechj/matlab/cech/src/

using namespace std;  // std c++ libs implemented in std

const double NaN = mxGetNaN();
const double cNdef = -10; //value of undefined correlation

//tohle je jen pro grow_version = 3 (grow3)
const double c_h = 0.8; //upper bound for correlation
const double B = 0.5;
const double B0 = 0.1;
                    
struct seed {
  int xl;
  int xr;
  int row;
  double c;
};

bool operator <(const seed& a, const seed &b) {
  return a.c<b.c;
}

struct DW {
  int d;    //disparity
  double c; //correlation
};

bool operator <(const DW& a, const DW& b) {
  return a.d<b.d;
}

typedef set<DW> SET_DW;
typedef set<DW>::iterator SIT;

class MAIN {
public:
  ARRAY_2D<double> iL;
  ARRAY_2D<double> iR;
  ARRAY_2D<double> sL;
  ARRAY_2D<double> sR;
  ARRAY_2D<double> vL;
  ARRAY_2D<double> vR;
  ARRAY_2D<double> SEEDs;


  ARRAY_2D<SET_DW> dw; //current solution (disparity+correlation)
  ARRAY_2D<int> K; //current number of disparity candidates per pixel
  ARRAY_2D<double> C_best; //currently best correlation 
  ARRAY_2D<double> Cr_best;//currently best correlation(right camera),grow4 only

  int ds_points_number; //number of points in the disparity space

  int w;  //parametres
  int range;
  double c_thr;
  double dmin; //disparity searchrange
  double dmax;
  unsigned K_lim; //max number of disparity candidates
  double beta; //beta of lower correlation bound for growing
  bool i_seed_accept; //accept all initial seeds

  priority_queue<seed> SEEDS;

  double wcorr(int xl, int xr, int row);
  double wcorr2(int xl, int xr, int row);//checks if it has been already computed (grow5)
  void local_optim(int xl, int xr, int row, double& c, int& dp);
  void lo_left(int& xl, int& xr, int& row, double& c);  //for grow5
  void lo_right(int& xl, int& xr, int& row, double& c); //for grow5
  void lo_up(int& xl, int& xr, int& row, double& c);    //for grow5
  void lo_down(int& xl, int& xr, int& row, double& c);  //for grow5
  void grow();
  void grow2(); //should be equivalent but faster
  void grow3(); //as grow2, but revealed constraints for growing
  void grow4(); //as grow, but symmetric condition for growing
  void grow5(); //as grow4, but symmetric local optimization when growing
  void result(ARRAY_2D<double>& D, ARRAY_2D<double>& W, ARRAY_2D<double>& Kout);

  MAIN(ARRAY_2D<double>& iL_, ARRAY_2D<double> &iR_,
       ARRAY_2D<double>& sL_, ARRAY_2D<double> &sR_,
       ARRAY_2D<double>& vL_, ARRAY_2D<double> &vR_,
       ARRAY_2D<double>& SEEDs_, 
       int w_, int range_, double c_thr_, 
       double dmin_, double dmax_, int K_lim_, double beta_,
       bool i_seed_accept_);

  ~MAIN();
};

// MAIN methods implementation ---------------------------------------------

MAIN::MAIN(ARRAY_2D<double>& iL_, ARRAY_2D<double> &iR_,
       ARRAY_2D<double>& sL_, ARRAY_2D<double> &sR_,
       ARRAY_2D<double>& vL_, ARRAY_2D<double> &vR_,
       ARRAY_2D<double>& SEEDs_,   
       int w_, int range_, double c_thr_, 
       double dmin_, double dmax_, int K_lim_, double beta_,
       bool i_seed_accept_) {

  DW tmp_dw;
  seed s; 
  int i;

  iL.assign(iL_);
  iR.assign(iR_);
  sL.assign(sL_);
  sR.assign(sR_);
  vL.assign(vL_);
  vR.assign(vR_);
  SEEDs.assign(SEEDs_);

  w = w_; range = range_; c_thr = c_thr_; 
  dmin = dmin_;
  dmax = dmax_;
  K_lim = K_lim_;
  beta = beta_;
  i_seed_accept = i_seed_accept_;

  //filling the zero number of disparity candidates
  ARRAY_2D<int> K_(iL.M,iL.N);
  K.assign(K_);
  K_.need_to_release = 0;
  K.need_to_release = 1;

  for (i=1;i<=(K.M*K.N);i++) {
    K[i] = 0;
  }

  //current solution initialization: dw
  ARRAY_2D<SET_DW> dw_(iL.M,iL.N);
  dw.assign(dw_);
  dw_.need_to_release = 0;
  dw.need_to_release = 1;

  //currently best correlation
  ARRAY_2D<double> C_best_(iL.M,iL.N);
  ARRAY_2D<double> Cr_best_(iR.M,iR.N);
  C_best.assign(C_best_);
  Cr_best.assign(Cr_best_);
  C_best_.need_to_release = 0;  C_best.need_to_release = 1;
  Cr_best_.need_to_release = 0;  Cr_best.need_to_release = 1;

  for (i=1;i<=(K.M*K.N);i++) {
    C_best[i] = cNdef;
  }

  for (i=1;i<=(iR.M*iR.N);i++) {
    Cr_best[i] = cNdef;
  }

  ds_points_number = 0;
 
  //filling the list of seeds 
  for (i=1;i<=SEEDs.M;i++) {
    s.xl = int(SEEDs.s(i,1));
    s.xr = int(SEEDs.s(i,2));
    s.row = int(SEEDs.s(i,3));
    s.c = wcorr(s.xl,s.xr,s.row);
    
    tmp_dw.d = s.xl-s.xr;
    tmp_dw.c = s.c;
    if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
      if ( s.c >= C_best.s(s.row,s.xl)-beta ) {//do not accept worse candidates 
        if (i_seed_accept) {
          C_best.s(s.row,s.xl) = s.c;
          Cr_best.s(s.row,s.xr) = s.c;
          SEEDS.push(s);
          dw.s(s.row,s.xl).insert(tmp_dw);
          K.s(s.row,s.xl)++;
          ds_points_number++;
        }
        else {
          if ( s.c > c_thr) {
            //C_best.s(s.row,s.xl) = s.c;  //*removed 23.11.06
            //Cr_best.s(s.row,s.xr) = s.c; //*
            SEEDS.push(s);
            //dw.s(s.row,s.xl).insert(tmp_dw);
            //K.s(s.row,s.xl)++;
            //ds_points_number++;
          }
        }
      }
    }
  }

//   DW a,b,c;
//   a.d=1; a.c=0.5;
//   b.d=0; b.c=0.7;
//   c.d=1; c.c=0.1;
//   ARRAY_2D<SET_DW> sol(10,10); 
//   sol.need_to_release = 0;
//   solution.assign(sol);
//   solution.need_to_release = 1;
//   solution.s(1,1).insert(a);
//   solution.s(1,1).insert(b);
//   mexPrintf("#%i c=%f \n\n",solution.s(1,1).count(c),solution.s(1,1).find(c)->c);
}

// MAIN destructor -----------------------------------------------------------

MAIN::~MAIN() {
  int i,j;

  //clear all sets from array dw
  for (i=1; i<=dw.M; i++) {
    for (j=1; j<=dw.N; j++) {
      dw.s(i,j).clear();
    }
  }
}


// window correlation --------------------------------------------------------

double MAIN::wcorr(int xl, int xr, int row) {
  int i,j;
  double sLR;
  double c;
  
  sLR = 0;
  for (i=-w; i<=w; i++) {
      for (j=-w; j<=w; j++) {
        sLR = sLR + iL.s(row+i,xl+j) * iR.s(row+i,xr+j);
      }
  }

  c = 2*(sLR - sL.s(row,xl)*sR.s(row,xr)) / (vL.s(row,xl) + vR.s(row,xr));
  if (c>1) c=0; //hack
  return c;
}

// window correlation --------------------------------------------------------

double MAIN::wcorr2(int xl, int xr, int row) { 
//use stored value if it has been already computed
  int i,j;
  double sLR;
  double c;
  int ind;
  SIT sit; 
  DW tmp_dw;
  
//   tmp_dw.d = xl-xr;
//   ind = dw.sub2ind(row,xl);
//   sit = dw[ind].find(tmp_dw);
//   if (sit!=dw[ind].end()) {
//     return sit->c;
//     mexPrintf("cs = %f  ",sit->c);
//   }

  sLR = 0;
  for (i=-w; i<=w; i++) {
      for (j=-w; j<=w; j++) {
        sLR = sLR + iL.s(row+i,xl+j) * iR.s(row+i,xr+j);
      }
  }

  c = 2*(sLR - sL.s(row,xl)*sR.s(row,xr)) / (vL.s(row,xl) + vR.s(row,xr));
  if (c>1) c=0; //hack
  //  mexPrintf("c = %f\n",c);

  return c;
}


// local optimization ------------------------------------------------------

void MAIN::local_optim(int xl, int xr, int row, double& c, int& dp) {
  //c,dp is output
  
  int i,xrr;
  double cc; //current corelation

  c = -1;
  dp = 0;  
  for (i=-range; i<=range; i++) {
    xrr = xr + i;
    if (xl>w & xl<iL.N-w & xrr>w & xrr<iR.N-w & row>w & row<iL.M-w ) {
      cc = wcorr(xl,xrr,row);
      if (cc > c) {
        c = cc;
        dp = i;
      };
    };
  }
}

// local optimization (left) -------------------------------------------------

void MAIN::lo_left(int& xlo, int& xro, int& row, double& c) {
  //c, xlo, xro (output)
  
  int xli,xri,xl,xr;
  double cc;

  xli = xlo;
  xri = xro;
  
  c = -1;

  if (row>w & row<iL.M-w) { //perhaps redundant

    //1
    xl = xli-1; 
    xr = xri-1;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //2
    xl = xli-2; 
    xr = xri-1;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli-1; 
    xr = xri-2;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }

  }
}

// local optimization (right) -------------------------------------------------

void MAIN::lo_right(int& xlo, int& xro, int& row, double& c) {
  //c, xlo, xro (output)
  
  int xli,xri,xl,xr;
  double cc;

  xli = xlo;
  xri = xro;
  
  c = -1;

  if (row>w & row<iL.M-w) { //perhaps redundant

    //1
    xl = xli+1; 
    xr = xri+1;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //2
    xl = xli+2; 
    xr = xri+1;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli+1; 
    xr = xri+2;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }

  }
}

// local optimization (up) -------------------------------------------------

void MAIN::lo_up(int& xlo, int& xro, int& row, double& c) {
  //c, xlo, xro, row (output)
  
  int xli,xri,xl,xr;
  double cc;

  xli = xlo;
  xri = xro;
  

  c = -1;
  
  row = row - 1;
  if (row>w & row<iL.M-w) { 

    //1
    xl = xli; 
    xr = xri;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //2
    xl = xli-1; 
    xr = xri;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli+1; 
    xr = xri;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //4
    xl = xli; 
    xr = xri-1;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli; 
    xr = xri+1;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }

  }
}

// local optimization (down) -------------------------------------------------

void MAIN::lo_down(int& xlo, int& xro, int& row, double& c) {
  //c, xlo, xro, row (output)
  
  int xli,xri,xl,xr;
  double cc;

  xli = xlo;
  xri = xro;
  

  c = -1;
  
  row = row + 1;
  if (row>w & row<iL.M-w) { 

    //1
    xl = xli; 
    xr = xri;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //2
    xl = xli-1; 
    xr = xri;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli+1; 
    xr = xri;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //4
    xl = xli; 
    xr = xri-1;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }
    //3
    xl = xli; 
    xr = xri+1;
    if (xl>w & xl<iL.N-w & xr>w & xr<iR.N-w) {
      cc = wcorr2(xl,xr,row);
      if (cc > c) {
        c = cc;  
        xlo = xl; xro = xr;
      }
    }

  }
}


// growing -----------------------------------------------------------------

void MAIN::grow() {

  int xl,xr,row,dp,d;
  seed s,st;
  double c;
  DW tmp_dw;
  int ind;  

  //  unsigned vis=0;
  //  mxArray *M; //only for visualization

  //main loop
  
  while (!SEEDS.empty()) {
    s = SEEDS.top();
    SEEDS.pop(); //remove the processed seed


    //add the seed into the current disparity map
    //    if (!i_seed_accept ) {
    //  tmp_dw.d = s.xl-s.xr
    //  tmp_dw.c = c;
    //  dw.s(s.row,s.xl).insert(tmp_dw); //allways add the seed
    //} 

    //left
    xl = s.xl-1;
    xr = s.xr-1; 
    row = s.row;
    local_optim(xl,xr,row,c,dp);
    if (c>c_thr) {
      tmp_dw.d = xl-(xr+dp);    
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        if (! dw[ind].count(tmp_dw) ) { //has different disparity 
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //right
    xl = s.xl+1;
    xr = s.xr+1; 
    row = s.row;
    local_optim(xl,xr,row,c,dp);
    if (c>c_thr) {
      tmp_dw.d = xl-(xr+dp);
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        if (! dw[ind].count(tmp_dw) ) {
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //up
    xl = s.xl;
    xr = s.xr; 
    row = s.row-1;
    local_optim(xl,xr,row,c,dp);
    if (c>c_thr) {
      tmp_dw.d = xl-(xr+dp);
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        if (! dw[ind].count(tmp_dw) ) {
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //down
    xl = s.xl;
    xr = s.xr; 
    row = s.row+1;
    local_optim(xl,xr,row,c,dp);
    if (c>c_thr) {
      tmp_dw.d = xl-(xr+dp);
      ind = dw.sub2ind(row,xl);
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        if (! dw[ind].count(tmp_dw) ) {
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind] - beta ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //visualization only
    /*vis++;
    if (vis > vis_step) {
      M = D.convert2mx();
      mexCallMATLAB(0,NULL,1,&M,"imager");
      mxDestroyArray(M);
      vis = 0;
    }*/

  }
  
  //ds_points_number update (recount)
  int i;
  ds_points_number = 0;
  for (i=1;i<=(K.M*K.N);i++) {
    ds_points_number = ds_points_number+K[i];
  }

}


// growing -----------------------------------------------------------------


void MAIN::grow2() {

  int xl,xr,row,dp,d;
  seed s,st;
  double c;
  DW tmp_dw, tdw0,tdw_m,tdw_p;
  int ind;  

  //  unsigned vis=0;
  //  mxArray *M; //only for visualization

  //main loop
  
  while (!SEEDS.empty()) {
    s = SEEDS.top();
    SEEDS.pop(); //remove the processed seed

    //left
    xl = s.xl-1;
    xr = s.xr-1; 
    row = s.row;
    ind = dw.sub2ind(row,xl);

    tdw0.d = xl-xr;
    tdw_m.d = xl-(xr-1);
    tdw_p.d = xl-(xr+1);                             //has different disparity
    if (!dw[ind].count(tdw0) & !dw[ind].count(tdw_m) & !dw[ind].count(tdw_p)) {
      local_optim(xl,xr,row,c,dp);
      d = xl-(xr+dp);          
      if (c>c_thr) {
        if (d >= dmin & d <= dmax) {//in the disparity searchrange
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.d = d;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }


    //right
    xl = s.xl+1;
    xr = s.xr+1; 
    row = s.row;
    ind = dw.sub2ind(row,xl);

    tdw0.d = xl-xr;
    tdw_m.d = xl-(xr-1);
    tdw_p.d = xl-(xr+1);                             //has different disparity
    if (!dw[ind].count(tdw0) & !dw[ind].count(tdw_m) & !dw[ind].count(tdw_p)) {
      local_optim(xl,xr,row,c,dp);
      d = xl-(xr+dp);          
      if (c>c_thr) {
        if (d >= dmin & d <= dmax) {//in the disparity searchrange
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.d = d;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }


    //up
    xl = s.xl;
    xr = s.xr; 
    row = s.row-1;
    ind = dw.sub2ind(row,xl);

    tdw0.d = xl-xr;
    tdw_m.d = xl-(xr-1);
    tdw_p.d = xl-(xr+1);                             //has different disparity
    if (!dw[ind].count(tdw0) & !dw[ind].count(tdw_m) & !dw[ind].count(tdw_p)) {
      local_optim(xl,xr,row,c,dp);
      d = xl-(xr+dp);          
      if (c>c_thr) {
        if (d >= dmin & d <= dmax) {//in the disparity searchrange
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.d = d;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //down
    xl = s.xl;
    xr = s.xr; 
    row = s.row+1;
    ind = dw.sub2ind(row,xl);

    tdw0.d = xl-xr;
    tdw_m.d = xl-(xr-1);
    tdw_p.d = xl-(xr+1);                             //has different disparity
    if (!dw[ind].count(tdw0) & !dw[ind].count(tdw_m) & !dw[ind].count(tdw_p)) {
      local_optim(xl,xr,row,c,dp);
      d = xl-(xr+dp);          
      if (c>c_thr) {
        if (d >= dmin & d <= dmax) {//in the disparity searchrange
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.d = d;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //visualization only
    /*vis++;
    if (vis > vis_step) {
      M = D.convert2mx();
      mexCallMATLAB(0,NULL,1,&M,"imager");
      mxDestroyArray(M);
      vis = 0;
    }*/

  }
  
  //ds_points_number update (recount)
  int i;
  ds_points_number = 0;
  for (i=1;i<=(K.M*K.N);i++) {
    ds_points_number = ds_points_number+K[i];
  }

}

// growing -----------------------------------------------------------------


void MAIN::grow3() {

  int xl,xr,row,dp,d;
  seed s,st;
  double c;
  DW tmp_dw, tdw0,tdw_m,tdw_p;
  int ind;  

  //  unsigned vis=0;
  //  mxArray *M; //only for visualization

  //main loop
  
  while (!SEEDS.empty()) {
    s = SEEDS.top();
    SEEDS.pop(); //remove the processed seed

    //left
    xl = s.xl-1;
    xr = s.xr-1; 
    row = s.row;
    ind = dw.sub2ind(row,xl);

    tdw0.d = xl-xr;
    tdw_m.d = xl-(xr-1);
    tdw_p.d = xl-(xr+1);                             //has different disparity
    if (!dw[ind].count(tdw0) & !dw[ind].count(tdw_m) & !dw[ind].count(tdw_p)) {
      local_optim(xl,xr,row,c,dp);
      d = xl-(xr+dp);          
      if (c>c_thr) {
        if (d >= dmin & d <= dmax) {//in the disparity searchrange
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            //            if ( c>= C_best[ind]-beta ) {
            if (C_best[ind] < c_h) {beta = B;} else {beta = B0;}
            if ( c>= C_best[ind]-beta ) {
              //if ( c < c_h ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.d = d;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }


    //right
    xl = s.xl+1;
    xr = s.xr+1; 
    row = s.row;
    ind = dw.sub2ind(row,xl);

    tdw0.d = xl-xr;
    tdw_m.d = xl-(xr-1);
    tdw_p.d = xl-(xr+1);                             //has different disparity
    if (!dw[ind].count(tdw0) & !dw[ind].count(tdw_m) & !dw[ind].count(tdw_p)) {
      local_optim(xl,xr,row,c,dp);
      d = xl-(xr+dp);          
      if (c>c_thr) {
        if (d >= dmin & d <= dmax) {//in the disparity searchrange
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if (C_best[ind] < c_h) {beta = B;} else {beta = B0;}
            if ( c>= C_best[ind]-beta ) {
            //if ( c < c_h ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.d = d;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }


    //up
    xl = s.xl;
    xr = s.xr; 
    row = s.row-1;
    ind = dw.sub2ind(row,xl);

    tdw0.d = xl-xr;
    tdw_m.d = xl-(xr-1);
    tdw_p.d = xl-(xr+1);                             //has different disparity
    if (!dw[ind].count(tdw0) & !dw[ind].count(tdw_m) & !dw[ind].count(tdw_p)) {
      local_optim(xl,xr,row,c,dp);
      d = xl-(xr+dp);          
      if (c>c_thr) {
        if (d >= dmin & d <= dmax) {//in the disparity searchrange
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if (C_best[ind] < c_h) {beta = B;} else {beta = B0;}
            if ( c>= C_best[ind]-beta ) {
            //if ( c < c_h ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.d = d;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //down
    xl = s.xl;
    xr = s.xr; 
    row = s.row+1;
    ind = dw.sub2ind(row,xl);

    tdw0.d = xl-xr;
    tdw_m.d = xl-(xr-1);
    tdw_p.d = xl-(xr+1);                             //has different disparity
    if (!dw[ind].count(tdw0) & !dw[ind].count(tdw_m) & !dw[ind].count(tdw_p)) {
      local_optim(xl,xr,row,c,dp);
      d = xl-(xr+dp);          
      if (c>c_thr) {
        if (d >= dmin & d <= dmax) {//in the disparity searchrange
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if (C_best[ind] < c_h) {beta = B;} else {beta = B0;}
            if ( c>= C_best[ind]-beta ) {
              //if ( c < c_h ) {
              if (c>=C_best[ind]) C_best[ind] = c;
              K[ind]++;
              tmp_dw.d = d;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //visualization only
    /*vis++;
    if (vis > vis_step) {
      M = D.convert2mx();
      mexCallMATLAB(0,NULL,1,&M,"imager");
      mxDestroyArray(M);
      vis = 0;
    }*/

  }
  
  //ds_points_number update (recount)
  int i;
  ds_points_number = 0;
  for (i=1;i<=(K.M*K.N);i++) {
    ds_points_number = ds_points_number+K[i];
  }

}

// growing -----------------------------------------------------------------

void MAIN::grow4() {

  int xl,xr,row,dp,d;
  seed s,st;
  double c;
  DW tmp_dw;
  int ind,indr;  

  //  unsigned vis=0;
  //  mxArray *M; //only for visualization

  //main loop
  
  while (!SEEDS.empty()) {
    s = SEEDS.top();
    SEEDS.pop(); //remove the processed seed


    //add the seed into the current disparity map
    //    if (!i_seed_accept ) {
    //  tmp_dw.d = s.xl-s.xr
    //  tmp_dw.c = c;
    //  dw.s(s.row,s.xl).insert(tmp_dw); //allways add the seed
    //} 

    //left
    xl = s.xl-1;
    xr = s.xr-1; 
    row = s.row;
    local_optim(xl,xr,row,c,dp);
    if (c>c_thr) {
      tmp_dw.d = xl-(xr+dp);    
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        indr = iR.sub2ind(row,xr);
        if (! dw[ind].count(tmp_dw) ) { //has different disparity 
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta | c>= Cr_best[indr]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c; //update C_best
              if (c>=Cr_best[indr]) Cr_best[indr] = c; //update Cr_best
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //right
    xl = s.xl+1;
    xr = s.xr+1; 
    row = s.row;
    local_optim(xl,xr,row,c,dp);
    if (c>c_thr) {
      tmp_dw.d = xl-(xr+dp);
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        indr = iR.sub2ind(row,xr);
        if (! dw[ind].count(tmp_dw) ) {
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta | c>= Cr_best[indr]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c; //update C_best
              if (c>=Cr_best[indr]) Cr_best[indr] = c; //update Cr_best
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //up
    xl = s.xl;
    xr = s.xr; 
    row = s.row-1;
    local_optim(xl,xr,row,c,dp);
    if (c>c_thr) {
      tmp_dw.d = xl-(xr+dp);
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        indr = iR.sub2ind(row,xr);
        if (! dw[ind].count(tmp_dw) ) {
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta | c>= Cr_best[indr]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c; //update C_best
              if (c>=Cr_best[indr]) Cr_best[indr] = c; //update Cr_best
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //down
    xl = s.xl;
    xr = s.xr; 
    row = s.row+1;
    local_optim(xl,xr,row,c,dp);
    if (c>c_thr) {
      tmp_dw.d = xl-(xr+dp);
      ind = dw.sub2ind(row,xl);
      indr = iR.sub2ind(row,xr);
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        if (! dw[ind].count(tmp_dw) ) {
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta | c>= Cr_best[indr]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c; //update C_best
              if (c>=Cr_best[indr]) Cr_best[indr] = c; //update Cr_best
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr+dp;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //visualization only
    /*vis++;
    if (vis > vis_step) {
      M = D.convert2mx();
      mexCallMATLAB(0,NULL,1,&M,"imager");
      mxDestroyArray(M);
      vis = 0;
    }*/

  }
  
  //ds_points_number update (recount)
  int i;
  ds_points_number = 0;
  for (i=1;i<=(K.M*K.N);i++) {
    ds_points_number = ds_points_number+K[i];
  }

}


// growing -----------------------------------------------------------------

void MAIN::grow5() {

  int xl,xr,row,dp,d;
  seed s,st;
  double c;
  DW tmp_dw;
  int ind,indr;  

  //main loop
  while (!SEEDS.empty()) {

    s = SEEDS.top();
    SEEDS.pop(); //remove the processed seed

    //left
    xl = s.xl;
    xr = s.xr;
    row = s.row;
    lo_left(xl,xr,row,c);
    if (c>c_thr) {
      tmp_dw.d = xl-xr;
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        indr = iR.sub2ind(row,xr);
        if (! dw[ind].count(tmp_dw) ) { //has different disparity 
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta | c>= Cr_best[indr]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c; //update C_best
              if (c>=Cr_best[indr]) Cr_best[indr] = c; //update Cr_best
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //right
    xl = s.xl;
    xr = s.xr;
    row = s.row;
    lo_right(xl,xr,row,c);
    if (c>c_thr) {
      tmp_dw.d = xl-xr;
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        indr = iR.sub2ind(row,xr);
        if (! dw[ind].count(tmp_dw) ) { //has different disparity 
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta | c>= Cr_best[indr]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c; //update C_best
              if (c>=Cr_best[indr]) Cr_best[indr] = c; //update Cr_best
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //up
    xl = s.xl;
    xr = s.xr;
    row = s.row;
    lo_up(xl,xr,row,c);
    if (c>c_thr) {
      tmp_dw.d = xl-xr;
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        indr = iR.sub2ind(row,xr);
        if (! dw[ind].count(tmp_dw) ) { //has different disparity 
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta | c>= Cr_best[indr]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c; //update C_best
              if (c>=Cr_best[indr]) Cr_best[indr] = c; //update Cr_best
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }

    //down
    xl = s.xl;
    xr = s.xr;
    row = s.row;
    lo_down(xl,xr,row,c);
    if (c>c_thr) {
      tmp_dw.d = xl-xr;
      if (tmp_dw.d >= dmin & tmp_dw.d <= dmax) {//in the disparity searchrange
        ind = dw.sub2ind(row,xl);
        indr = iR.sub2ind(row,xr);
        if (! dw[ind].count(tmp_dw) ) { //has different disparity 
          if ( K[ind] < K_lim ) { //less than maximum number of disp.candidates
            if ( c>= C_best[ind]-beta | c>= Cr_best[indr]-beta ) {
              if (c>=C_best[ind]) C_best[ind] = c; //update C_best
              if (c>=Cr_best[indr]) Cr_best[indr] = c; //update Cr_best
              K[ind]++;
              tmp_dw.c = c;
              dw[ind].insert(tmp_dw);
   
              st.xl = xl;
              st.xr = xr;
              st.row = row;
              st.c = c;
              SEEDS.push(st);
            }
          }
        }
      }
    }


  }
  
  //ds_points_number update (recount)
  int i;
  ds_points_number = 0;
  for (i=1;i<=(K.M*K.N);i++) {
    ds_points_number = ds_points_number+K[i];
  }

}




// result assignement -----------------------------------------------------

void MAIN::result(ARRAY_2D<double>& D, ARRAY_2D<double>& W, 
                  ARRAY_2D<double>& Kout) {
  int i,j,k;
  int tmp_ind;
  SIT it; //set iterator;

  k = 1;
  for (i=1; i<=dw.M; i++) {
    for (j=1; j<=dw.N; j++) {
      tmp_ind = dw.sub2ind(i,j);
      it = dw[tmp_ind].begin();
      while ( it!=dw[tmp_ind].end() ) {
         D.s(k,1) = i;
         D.s(k,2) = j;
         D.s(k,3) = it->d;
         W.s(k,1) = it->c;
         it++;
         k++;
      }
      Kout[tmp_ind] = K[tmp_ind];
    }
  }

}


// ========================================================================

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  //input

  ARRAY_2D<double> iL(prhs[0]); 
  ARRAY_2D<double> iR(prhs[1]);
  
  ARRAY_2D<double> sL(prhs[2]);
  ARRAY_2D<double> sR(prhs[3]);

  ARRAY_2D<double> vL(prhs[4]);
  ARRAY_2D<double> vR(prhs[5]);

  ARRAY_2D<double> SEEDs(prhs[6]);

  double* tmp;
  int w,range;
  double c_thr;
  double dmin, dmax;
  int K_lim;
  double beta;
  double grow_version;
  bool i_seed_accept;

  tmp = mxGetPr(prhs[7]); w = int(tmp[0]);
  tmp = mxGetPr(prhs[8]); range = int(tmp[0]);
  tmp = mxGetPr(prhs[9]); c_thr = tmp[0];
  tmp = mxGetPr(prhs[10]); dmin = tmp[0]; dmax = tmp[1];
  tmp = mxGetPr(prhs[11]); 
  if (tmp[0]>INT_MAX) K_lim = INT_MAX; else K_lim = int(tmp[0]);
  tmp = mxGetPr(prhs[12]); beta = tmp[0];
  tmp = mxGetPr(prhs[13]); grow_version = int(tmp[0]);
  tmp = mxGetPr(prhs[14]); i_seed_accept = bool(tmp[0]);
  
  //end of input interface

  MAIN p(iL,iR,sL,sR,vL,vR,SEEDs, w,range,c_thr,dmin,dmax,K_lim,beta,
         i_seed_accept);

  if (grow_version == 1) {
    p.grow();
  }
  if (grow_version == 2) {
    p.grow2();
  }
  if (grow_version == 3) {
    p.grow3();
  }
  if (grow_version == 4) {
    p.grow4();
  }
  if (grow_version == 5) {
    p.grow5();
  }

  //output
  plhs[0] = mxCreateDoubleMatrix(p.ds_points_number, 3, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(p.ds_points_number, 1, mxREAL);
  ARRAY_2D<double> D(plhs[0]);
  ARRAY_2D<double> W(plhs[1]);
  plhs[2] = mxCreateDoubleMatrix(iL.M, iL.N, mxREAL);
  ARRAY_2D<double> K(plhs[2]);  //output (number of disp.candidates per pixel)

  p.result(D,W,K);

//   SIT it1,it2;
//   DW a,b,c,d;
//   multiset<DW> s1;
//   a.d=2; a.c=0.5;
//   b.d=1; b.c=0.7;
//   c.d=5; c.c=0.1;
//   d.d=4; d.c=0.8;
//   s1.insert(a);
//   s1.insert(b);
//   s1.insert(c);
//   mexPrintf("[%f]\n",s1.find(c)->c);
//   mexPrintf("%i, c=%f\n",s1.count(c),s1.find(c)->c);
//   it1 = s1.begin();
//   while ( it1!=s1.end() ) {
//     mexPrintf("(%i,%f)\n",it1->d,it1->c);
//     it1++;
//   }

//   hash_set<int, hash<int> > s2;
//   s2.insert(1);
//   mexPrintf("%i",s2.bucket_count());

  
}
  


/*

   [D,W,K] = dgrow3_mex(Il,Ir,SEEDs, Sl,Sr,Vl,VR, 
                       w,range,c_thr,disp_searchrange,max_candidates,
                       beta, growing, i_seed_accept)

   - Il,Ir...input images (rectified)
   - SEEDs...initial correspondences [xl,xr,row] (nx3 vector)

   ->D...list of disparity points [u,v,d] per row
   ->W...list of corresponding correlation [w] per row
   ->K...map of the number of disparity candidates
   ->beta...beta of lower correlation bound for growing
   ->growing...1-basic,
               2-fast, 
               3-two thresholds,
               4-symmetric (asymmetric neighbourhood - local optimization),
               5-fully symmetric [default]
*/

