/*_

  heap.h  minimum-rooted and maximum-rooted heap data structures

           Heap is a datastructure with two operations: extract-root and
           insert. In minimum-rooted heap, the root has the minimum value of
           the key, in maximum-rooted heap, it has the maximum value. In
           implicit heaps no complete ordering is given, only a function that
           compare two elements.

           Elements with equal key will be retrieved in an
           unpredictable order.

           Uses malloc(), realloc(), and free() to allocate/free
           auxiliary memory.

           Errors are reported with Error(). See <myerror.h>

 (c) Radim Sara, GRASP U of Penn, Jan 31, 1997
     ConsH() and ChangeRootKeyH() by R. Sara, CMP FEE CTU, Jul 22, 1999
     Implicit heaps by R. Sara, CMP FEE CTU, Oct 12, 2000

 Source: L. Kucera. Combinatorial Algorithms. Adam Hilger 1990.
         R. Sedgewick. Algorithms. 2nd ed. 1988

 To do: 

    1. replace root with a new item
_*/

#ifndef _HEAP_
#define _HEAP_

#include <myerror.h>

typedef double HeapKeyT;

typedef enum {undefined, min_rooted, max_rooted, implicit} HeapType;

typedef struct {HeapKeyT key; void *block;} helementT;

typedef int (*isGreaterT)(void *block1, void *block2);
 /* comparison method type, returns 1 if block1 > block2 and 0 otherwise */

typedef struct _heap {
 long int n;    /* heap size */
 int null;      /* 1 if the heap contains just the keys, initially null = 1 */
 HeapType type;
 helementT *list;
 isGreaterT comp;
} HeapT;
 /* WARNING: do not access the structure directly, use macros provided below */

extern void Hmm(void* (*emalloc)(size_t),
                void* (*erealloc)(void*,size_t),
                void (*efree)(void*));
/* Registers the set of memory management functions. Useful with
   Matlab: Hmm(mxMalloc,mxRealloc,mxFree). No need to register standard
   memory management, the default is set to alloc(), realloc(),
   free(). */

#define LengthH(hp) ( (hp).n )
 /* heap size */

#define IsEmptyH(hp) ( (hp).n == 0 )
 /* is the heap empty? */

#define GetHeapType(hp) ( (hp).type )
 /* heap type */

#define FOR_H(h, e, i)\
 for(e=(h).list[i=0].block; i < (h).n; e=(h).list[++i].block)
/* do something for all heap h elements (void*)e. The integer i is the
   element index in the heap. */

extern HeapKeyT HeapRootKey(HeapT h);
 /* returns the key of the heap root, causes error if the heap is empty or if
    it is an implicit heap. */

extern void *HeapRootBlock(HeapT h);
 /* returns the root block, causes error if the heap is empty. */

extern void InitH(HeapT *h, HeapType type, ...);
 /* InitH(h,type) initializes the datastructure. InitH(h,implicit,fun)
    initializes implicit heap given comparison function isGreaterT comp. */

extern void InsertH(HeapT *h, HeapKeyT key, void *elm);
 /* inserts new element into a heap. InsertH(&h, key, NULL) is possible,
    e.g. for heaps containing nothing but the keys. The key value is ignored
    in implicit heaps, use InsertH(&h, 0, elm) in this case. */

extern HeapKeyT ExtractRootH(void **root, HeapT *h);
 /* extracts (removes) root from the heap. Returns pointer to extracted root
    and the key value. k = ExtractRootH(NULL, h) is possible iff the heap
    elements are all NULL (heap contains just the keys). If not, Error()
    <myerror.h> is called. Attempt to extract element from an empty heap
    causes error. In implicit heaps the key is the value stored by InsertH()
    or ConsH(). */

extern HeapKeyT ChangeRootKeyH(HeapT *h, HeapKeyT key);
/* Changes the key in the element in the heap root. The heap is rearranged
   accordingly. Functionally equivalent to extract followed by insert of the
   extracted element but is much faster. The associated data block is not
   changed. Returns the new key. Cannot be used for implicit heaps. */

extern void ConsH(HeapT *h, helementT *data, long n);
/* Converts an array to a heap in O(n) steps. The rearranged array will become
   a part of the heap datastructure (never deallocate it or use it after it is
   converted to a heap, FreeH will take care of it). The heap structure must
   be initialized in InitH() prior to calling ConsH(). The parameter is an
   array of length n of blocks of type helementT and must be allocated by
   malloc() or calloc(), never by mxCalloc() in Matlab! */

extern void FreeH(HeapT *h, void (*Free)(void*));
 /* removes the datastructure from the memory including all remanining
    elements.  Free() removes a single element helementT from the list of
    elements and any references herewidth (if requested). Free(h, NULL) is
    possible (removes the heap auxiliary array using free()). */

extern void downheap_implicit(HeapT *h, long int k);

#endif
