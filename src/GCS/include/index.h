/*_

 $Source$
 $Id$

 index.h   index sets

 (c) Radim Sara (sara@cmp.felk.cvut.cz) FEE CTU Prague, 17 Feb 99

  This is free software; you can redistribute it and/or modify it. It is
  distributed WITHOUT ANY IMPLIED OR EXPRESSED WARRANTY and WITHOUT ANY
  FURTHER SUPPORT for the desired and/or other purpose. See README file in the
  main distribution.

 $Log$
_*/

#ifndef _INDEX_
#define _INDEX_

#include <sets.h>

typedef long int IndexT;   

typedef struct _indx {
 struct _indx *s, *p; /* successor, predecessor in the list */
 IndexT indx;         /* the index value                    */
} IndexLT;            /* element of index list              */

 /* index set */
typedef IndexLT *IndexSetT;



#define ValidIndex(indx) ((indx) >= 0)
 /* int ValidIndex(IndexT indx) */
 /* returns 1 if indx is a valid generic positive index */

extern IndexSetT ConsIS(int n);
 /* Index set constructor and initializer. The set is initialized to 1:n, if
    n==0, the set is empty. */

#define FreeIS(is) EmptyL((ListT*)(is), free)
/* extern void FreeIS(IndexSetT *is) */
/* Index set destructor. Frees all memory. The set becomes empty. */


/* All algorithms for index set manipulation are O(n1+n2). */

extern IndexSetT AddIS(IndexSetT *is, IndexT i);
 /* adds a new element to index set. */

extern IndexSetT RemoveIS(IndexSetT *is, IndexT i);
 /* removes an element from index set if it exists. */

extern void *CopyIndexL(void *indx);
 /* copies list element */

#define CopyIS(is) CopyL(is, CopyIndexL)
 /* IndexSetT CopyIS(IndexSetT is) */
 /* copies an index set */

#define FOR_IS(is, el, i)\
 for(el = FirstElmL(is), i = (!IsEmptyL(el)) ? ((IndexSetT)(el))->indx : 0;\
     !IsEmptyL(el); \
     el = NextElmL(el), i = ((IndexSetT)(el))->indx,\
     el = (IsFirstL(is, el)) ? NULLL : el)
 /* FOR_IS(IndexSet is, void *aux, IndexT i) */
 /* do something for all indices i in an index set */

extern int QElementIS(IndexSetT is, IndexT i);
 /* queries an element in an index set. Returns 1 if index is in the set, 0
    otherwise */

extern int QSubsetIS(IndexSetT is1, IndexSetT is2);
 /* queries a subset in a set. Returns  : 
    is1 sub is2  -1  (strictly subset)
    is2 sub is1   1  (strictly subset)
    is1 == is2    0  (equal)
    otherwise     2  

   an empty set is a subset of any non-empty set.
  */

#define QEmptyIS(is) IsEmptyL(is)
/* int QEmptyIS(IndexSetT is) queries set's emptiness. */

extern int QEqualIS(IndexSetT is1, IndexSetT is2);
 /* queries index set equality. Returns 1 if both sets are the same length and
    hold the same indices. */

extern IndexSetT IntersectionIS(IndexSetT *is1, IndexSetT is2);
 /* index set intersection. is2 will remain intact and is1 will be the
    result. Returns the result. */

extern IndexSetT UnionIS(IndexSetT *is1, IndexSetT is2);
 /* index sets union, is2 will remain intact and is1 will be the
    result. Returns the result. */

extern IndexSetT DifferenceIS(IndexSetT *is1, IndexSetT is2);
 /* index sets difference, is2 will remain intact and is1 will be the
    result. Returns the result. */

#define PrintIS(f, st, name)\
  PrintL(f, "%i","%s = [", ", ", "]\n", st, name)
 /* void PrintIS(FILE *f, IndexSet st, char *name) */
 /* prints an index set to FILE *f */


/*_ OBSOLETE: _*/

extern IndexLT *ConsIndexL(IndexT indx);
 /* list element constructor. Do not confuse with ConsIndex(). */

extern IndexT *ConsIndex(IndexT indx);
 /* constructor of (IndexT*); allocates space on heap and puts the
    value of indx there. Do not confuse with ConsIndexL() */


extern int CompareIndex(const void *i1, const void *i2,...);
 /* returns 1 if both are the same */

#define IndexSetIntersection(s1, s2)\
 SetIntersectionL(s1, s2, CompareIndex, CopyIndexL, free)
 /* intersection of index sets */

#define IndexSetDifference(s1, s2)\
 SetDifferenceL(s1, s2, CompareIndex, CopyIndexL, free)
 /* difference of index sets */

#define IndexSetUnion(s1, s2)\
 SetUnionL(s1, s2, CompareIndex, CopyIndexL, free)
 /* union of index sets */

extern IndexT RemoveIndex(ListT *s, IndexT i);
 /* removes index i from the set. If it is not a member of the set,
    does nothing. Returns the value of the removed index. */

#define PrintIndexList(f, st, name)\
  PrintL(f, "%i","%s = [", ", ", "]\n", st, name)
 /* prints a named list of indices s to file f */

#endif
