/*_
                                                                       
   sets.h   Basic operations on finite sets                            
            Based on list library of the same author.                  
                                                                       
   author: (c) Radim Sara, GRASP U of Penn, radim@grip.cis.upenn.edu       
   date:   April 26, 1996                                              
                                                                       
   modifications:                                                      
                                                                       
   DESCRIPTION: None of the functions alters the original lists. If    
   there are multiple occurences of an element in either set, they are 
   removed. It is assumed that both input sets (lists) have the same   
   datatype. For generality, all functions require pointers to three   
   auxiliary functions:                                                
                                                                       
   compare(), which returns 0 if two elements are not the same; this   
   function is given poiters TO DATA, not pointers to list elements!   
                                                                       
   copy(), which copies an element given pointer TO LIST ELEMENT,      
                                                                       
   freef(), which releases memory given pointer to list element, it is 
   usually standard free().                                            
                                                                       
   See also: PruneL() in list.h                                        
_*/

/*
   NOTES:

   better synopsis:

    ListT SetUnionL(ListT *result, ListT l1, ListT l2, 
             int (*compare)(), 
             void *(*copy)(), 
             void (*freef)());

    replaces *result with the result. If *result == l1 or *result == l2
    that means that l1 (l2) will be re-written

*/

#ifndef _SETS_
#define _SETS_

#include "mylist.h"

extern ListT SetUnionL(ListT l1, ListT l2, 
             int (*compare)(const void *data1, const void *data2,...), 
             void *(*copy)(void *element), 
             void (*freef)(void *element));
 /* Set union, removes multiple occurences of the same element from
    the result. See also parameters description above. */


extern ListT SetIntersectionL(ListT l1, ListT l2, 
             int (*compare)(const void *data1, const void *data2,...), 
             void *(*copy)(void *element), 
             void (*freef)(void *element));
 /* Set intersection, removes multiple occurences of the same element
    from the result. See also parameters description above. */

extern ListT SetDifferenceL(ListT l1, ListT l2, 
             int (*compare)(const void *data1, const void *data2,...), 
             void *(*copy)(void *element), 
             void (*freef)(void *element));
 /* Set difference, removes multiple occurences of the same element
    from the result. See also parameters description above. */

#endif
