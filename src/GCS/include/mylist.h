/*_

 list.h Library for double-linked circular lists

 author: R. Sara (sara@vision.felk.cvut.cz)

 **** alpha.1.2 version, June, 18th 1993 ****

 based on LISTFNS V 1.0 and 2.0 by R. Sara;
 a good inspiration was also taken from the library LL V 2.1 by G. Matas
 (g.matas@ee.surrey.ac.uk)

 PrintL is adapted from LL V 2.1  

 modifications:

  June 16th, 1993, Sara: 
     - corrected MAX_LIST_L_EXP, modified EmptyL() to be consistent 
     - added function equivalents of macros (for debugging)  
     - old macro AllocForD(void *, size_t) changed to 
       function void *AllocForD(size_t)

  December 28, 1994, Sara
     - corrected StdCons(), added definition of VA_BLOCK
  December 8, 1995, Sara
     - added parentheses around FindNextL
  January 20, 1996, Sara
     - corrected FOR_DOWN_L()
  March 15, 1996, Sara
     - added FOR_START_DOWN_L() and FOR_START_L()
  April 26, 1996, Sara
     - removed parameter definition as void* in the copy function in CopyAftL
  January 31, 1997, SortListL() renamed to SortL(). Backward compatibility
       macro exists.
  Feb 11, 1997, Sara
     - added variable arguments to PrintL
  Mar 2, 1997, Sara
     - added test for NULL in IsConsistent()
_*/

#ifndef _list_
#define _list_

#define ANSI
#include <stdio.h>
#include <stdlib.h> /* for size_t */

#ifdef __WATCOMC__
#define VA_BLOCK(ap) (ap)[0]
#else
#define VA_BLOCK(ap) (ap)
#endif
 /* this is a non-portable macro: returns pointer to the first element
    in the argument list, used only by StdCons(), check stdarg.h for
    the representation of the argument list to change this macro */
 
#define Lmalloc malloc
 /* Lmalloc alocates space for list elements. Lmalloc is used only
    in some of the routines belonging to the library extension */

#define Lcalloc calloc
 /* Lcalloc allocates space for a lookup table. Used in LookupTableL only */


/*_____________________ LINK STRUCTURE, TYPE DEFINITION ______________________*/

typedef struct linkst
 {
   void *s, *p;     /* successor, predecessor in a list */
 } LinksT;

typedef void *ListT;
 /* "list" is a pointer to an element of chain */



#define MAX_LIST_L_EXP (8*sizeof(long int))
#define MAX_LIST_LENGTH ( ((1L << (MAX_LIST_L_EXP-2)) - 1) + (1L << (MAX_LIST_L_EXP-2)) )


extern ListT NULLL;
 /* the null element/list : each of its successors/predecessors is NULLL */


/*________________________________ PREDICATES ________________________________*/
   
#ifndef DEBUG

#define IsFirstL(list, elm) ( (ListT)(list) == (ListT)(elm) )
 /* is the element the first one in the list? */

#define IsLastL(list, elm) ( (ListT)LastElmL(list) == (ListT)(elm) )
 /* is the element the last one in the list? */

#define IsEmptyL(list) ( (ListT)(list) == (ListT)NULLL )
 /* is the list empty? */ 

#else 
#ifdef ANSI
 extern int IsFirstL(ListT list, ListT elm);
 extern int IsLastL(ListT list, ListT elm);
 extern int IsEmptyL(ListT list); 
#else
 extern int IsFirstL();
 extern int IsLastL();
 extern int IsEmptyL(); 
#endif  
#endif


/*_______________________________ INITIALIZATION _____________________________*/

/* An element constructor (written by user) is a function that allocates
   memory for an element, initializes its links and contents, and returns
   pointer to the elment (returns pointer to a one-element list) */

#ifdef COOK_BOOK

 COOK-BOOK for constructors:

 MyStructT *MyConstructor(int blah1, int blah2)
 {
  MyStructT *block;

  AllocForE(block, MyStructT); 
   /* :allocates using Lmalloc, initializes links */

  block->blah1 = blah1;
  block->blah2 = blah2;

  return block;
 }

#endif

#define InitLinksE(elm) (PrevElmL(elm) = NextElmL(elm) = (void *)(elm))

#define AllocForE(block, type)\
 if ( ( block = (type*)Lmalloc(sizeof(type)) ) == NULL )\
  {\
   ListError(1);\
   return NULLL;\
  }\
 else\
  InitLinksE(block);                                 

  /* :see also AllocForD() later in the include file */


/*___________________ BASIC OPERATIONS WITH SINGLE ELEMENT __________________*/
   

#define FirstElmL(list) list
 /* dereference of the list first element; can be used as l-value */

#define LastElmL(list) ( ((LinksT*)(list))->p )
 /* dereference of the list last element; can be used as l-value */

#define NextElmL(elm) ( ((LinksT*)(elm))->s )
 /* dereference of the element successor; can be used as l-value */

#define PrevElmL(elm) ( ((LinksT*)(elm))->p )
 /* dereference of the element predecessor; can be used as l-value */
       
/* :functional equivalents does not exist. Use equivalent expressions. */


#ifdef ANSI

extern ListT OffsetElmL(ListT list, long int n);
 /* return element at offset n from the list beginning,
    n can be negative; n == 0 means no offset;
    OffsetElmL(list, n) returns (n+1)-th element in the list */

extern long int WhereIsElmL(ListT list, ListT elm);
 /* return the element offset from the first list element,
    return 0 if elm == list, -1 if element is not a member of the list.
    Does not check long int counter overflow. Complement to OffsetElmL() */

#else
 extern ListT OffsetElmL();
 extern long int WhereIsElmL();
#endif

/*_____ INSERT _____*/


#ifdef ANSI

extern ListT InsBefL(ListT *list, ListT element);
 /* insert new element (list) before the first element of a list,
    return pointer to inserted element. If element is an empty
    list, nothing happens, returns empty list. */

extern ListT InsAftL(ListT *list, ListT element);
 /* insert an element (list) after the first element of a list,
    return pointer to inserted element */

extern ListT InsFirstL(ListT *list, ListT element);
 /* insert an element (list) at the beginning of a list,
    return pointer to inserted element */

#else
 extern ListT InsBefL();
 extern ListT InsAftL();
 extern ListT InsFirstL();
#endif

 
#ifndef DEBUG      

#define InsLastL(listptr, new) InsBefL(listptr, new)
 /* insert an element at the end of a list, return pointer to inserted
    element */        

#else     
#ifdef ANSI
 extern ListT InsLastL(ListT *list, ListT new);
#else
 extern ListT InsLastL();
#endif
#endif


/*_____ UNLINK _____*/

/*

  free(UnlinkElmL(myList, myElem))            to delete a single element;
                                              don't free NULLL!

  InsFirstL(myList2, UnlinkFirstL(&myList1))  to move first element
                                              from myList1 to myList2
*/

#ifdef ANSI
extern ListT UnlinkElmL(ListT *list, ListT element);
 /* unlink an element from a list, return pointer to unlinked element;
    update the list reference if necessary (first element unlinked) */

#else
 extern ListT UnlinkElmL();
#endif
 
#ifndef DEBUG    

#define UnlinkFirstL(listptr) UnlinkElmL(listptr, *(listptr))
/* unlink the first element from a list, return pointer to it;
   update the list reference to the next element or set it to NULLL */

#define UnlinkLastL(listptr) UnlinkElmL(listptr, PrevElmL(*(listptr)))
/* unlink last element from a list, return pointer to it;
   update the list reference if necessary */

#else
#ifdef ANSI  
 extern ListT UnlinkFirstL(ListT *list);
 extern ListT UnlinkLastL(ListT *list);
#else    
 extern ListT UnlinkFirstL();
 extern ListT UnlinkLastL();
#endif
#endif

/*_______________________ BASIC OPERATIONS ON LIST __________________________*/

#ifdef ANSI

extern ListT EmptyL(ListT *list, void (*Free)(void *));
 /* delete all elements from a list, return an empty list reference (NULLL);
    use  EmptyL(&myList, free)  for nonhierarchical lists */

extern ListT ConcatL(ListT list1, ListT list2);
/* concatenate list1 and list2 */

extern ListT CopyAftL(
 ListT dest, ListT src, void *(*copyElement)(void *));
 /* copies a list src after dest in another list, returns pointer to
    dest or to the first element of the new list (when dest == NULLL) */

extern ListT ReverseL(ListT *list);
 /* reverses a list */

#else
 extern ListT EmptyL();
 extern ListT ConcatL();
 extern ListT CopyAftL();
#endif
        
#ifndef DEBUG
#define CopyL(list, copyElement) CopyAftL(NULLL, list, copyElement)
 /* copies a list. copyElement(void *) is user-defined function for
    single element copy */
#else
#ifdef ANSI
 extern ListT CopyL(ListT list, void *(*copyElement)(void *));
#else    
 extern ListT CopyL();
#endif
#endif

/*_______________ DO SOMETHING WITH ALL LIST ELEMENTS ________________*/


#define FOR_L(list, elm)\
 for(elm = FirstElmL(list); !IsEmptyL(elm); \
     elm = NextElmL(elm), elm = (IsFirstL(list, elm)) ? NULLL : elm)

  /* :do an operation for each element of a list;
     elm is NULL after the loop finishes. Analogy of 'for' command. */

#define FOR_START_L(list, start, elm)\
 for(elm = start; !IsEmptyL(elm); elm = NextElmL(elm),\
     elm = (IsFirstL(list, elm) || (elm == start)) ? NULLL : elm)

 /* :like FOR_L but starts with a given element. The test (elm == start)
     is just for safety. */

#define FOR_DOWN_L(list, elm)\
 for( elm = LastElmL(list); !IsEmptyL(elm);\
      elm = PrevElmL(elm),  elm = (IsLastL(list, elm)) ? NULLL : elm )

  /* :do an operation for each element of a list in reversed order;
     start with the last element; elm is NULL after the loop
     regularly finishes. Analogy of 'for' command. */

#define FOR_START_DOWN_L(list, start, elm)\
 for( elm = start; !IsEmptyL(elm); elm = PrevElmL(elm),\
      elm = (IsLastL(list, elm) || (elm == start)) ? NULLL : elm)


#define SAFE_FOR_L(list, start, new, next, oldlist)\
 for(oldlist = list, new = next = start;\
     ((!IsEmptyL(new)) && (!IsEmptyL(list)));\
     new = next,\
     (new == list) ? ((oldlist == list) ? new = NULLL : (oldlist = list)) : 0)


#ifdef comment
      SAFE_FOR_L does an operation for each element of a list;
      'list' variable may change within the loop; 'oldlist' must not.
      elm is NULLL after the loop finishes!  
      For an example see the EmptyL() code - it is written using SAFE_FOR_L.
      The only element that can be removed is "new"

      synopsis:

      ListT myList; /* list of data */
      ListT oldlist, current, successor; /* auxiliary variables */


      myList = ConsMyList();

       /* do sth with all list elements except of the first one: */
      SAFE_FOR_L(myList, NextElmL(myList), current, successor, oldlist)
       {
        successor = NextElmL(current); /* compulsory */

        /* ... here you can work with 'current', 'myList', 'successor';
               you are free to change 'myList', 'current' ... */
       }

       */      

     NOTE: Do not use UnlinkFirstL(&current) in such a loop.
           Use UnlinkElmL(&myList, current) instead, which is an equivalent.
#endif


#ifdef ANSI

extern long int LengthL(ListT list);
 /* length of a list; does not check the long int counter overflow. */

extern ListT SplitL(ListT *list, ListT elm);
 /* split a list into two parts:
   "list" ... "elm->p" become the first one  (referenced by the return value);
   "elm" ... "list->p" become the second one (referenced by elm);
    if list == elm, then the first part will be empty */
#else
 extern long int LengthL();
 extern ListT SplitL();
#endif

/*________________________ MORE COMPLEX LIST OPERATIONS ______________________*/

#ifndef DEBUG

#define FindFirstL(list, test) ApplyL(list, list, test)
 /* finds the first element of a list for which "test" returns non-NULLL;
    returns NULLL if no such element exist */

#define FindNextL(list, first, test)\
 ((IsFirstL(list, NextElmL(first))) ? NULLL :\
   ApplyL(list, NextElmL(first), test))
 /* finds next element in a list for which "test" returns NULLL;
    returns NULLL if no such element exist */
#else 
#ifdef ANSI
 extern ListT FindFirstL(ListT list, void *(*test)());
 extern ListT FindNextL(ListT list, ListT first, void *(*test)());
#else      
 extern ListT FindFirstL();
 extern ListT FindNextL();
#endif
#endif


#define FOR_SPEC_L(list, elm, spec) \
 for( elm = FindFirstL(list, spec); !IsEmptyL(elm);\
      elm = FindNextL(list, elm, spec))
/* :do an operation for each element that satisfies void *spec(elm) != NULLL */


#ifdef ANSI

extern ListT ApplyL(
 ListT list, ListT start, void *(*apply)(void*, ...), ...);
 /* applies "apply" to "start" and all its succesors
    until "apply" returns non-NULLL or the list end is reached;
    "apply" can access optional arguments of ApplyL;
    returns the non-NULLL value returned by "apply" */

extern ListT FindChangeL(
 ListT list, ListT start, long int (*criterion)(void *, ...),...);
 /* finds a successor of "start" in a list that changes "criterion",
    returns NULLL if not found; "criterion" can access optional
    arguments of FindChangeL */

extern ListT MakeFromL(void *(*construct)(void*,...),...);
 /* Links results of successive calls of "construct" into a list
    (until it returns NULLL); "construct" must allocate space for
    the elements */

extern ListT MapplyL(
 ListT list, ListT start, void *(*construct)(void *, ...),...);
 /* applies "construct" to a portion of a list starting with "start",
    links results into new list; "construct" can return NULLL */

extern ListT ExtractL(
 ListT *list, void *(*criterion)(void*, ...),...);
 /* extracts (unlinks) all elements satisfying "criterion" (non-NULLL == TRUE)
    returns list of extracted elements; */

extern void **LookupTableL(ListT list, unsigned long int *size);
 /* Creates a look-up table into a list for random access, the table
    contains pointers to data blocks (not to list elements), does not
    check whether the requested memory block has correct size (may
    cause problems for huge lists on computers with segmented memory);
    uses Lcalloc. Returns the list length in 'size'. */

extern ListT PruneL(ListT *list, 
 int (*compare)(const void*, const void*, ...), ... );
/* removes all multiple occurences of elements in a list, return
   the list of removed elms. compare() receives pointers to elements data and
   returns non-zero if the data *el1 == *el2. O(N*N) algorithm. */

#define SortListL SortL
 /* for backward compatibility */

extern ListT SortL(
 ListT list, int (*compare)(const void*, const void*, ...), ...);
 /* sorts a list according to a compare function, compare() is
    positive if "*e1 > *e2" => the result will be sorted in descending
    order; returns pointer to the sorted list; "list" will refer to
    the same element as before, compare() has the access to optional
    arguments of SortL.  O(N log N) algorithm. */

extern int PrintL(
 FILE *file, char *format, char *begin, char *separator, char *end, 
 ListT list,...);
 /* Formatted print of each element of a list. The variable parameters
    are to be printed with the "begin" format specifier. Original
    PrintL is by G. Matas */
 /* format conversion:

%[flags][width][.precision][modifier]type

TYPE

      type   output format
________________________________

 d,i   int   signed decimal integer
 u     int   unsigned decimal integer
 o           unsigned octal integer
 x           unsigned hexadecimal integer, using "abcdef"
 X           unsigned hexadecimal integer, using "ABCDEF"
 f   double  signed value in the form [-]dddd.dddd
 e   double  signed value in the form [-]dd.dde[sign]ddd
 E   double  like 'e', except that 'E' introduces the exponent instead of 'e'
 g   double  'f'  or 'e', whichever is more compact
 G   double  like 'g', except that 'G' introduces the exponent
 c     int   single character
 s   char*   characters printed up to the first null character


MODIFIER
________

h  short int (with d,i,o,x), short unsigned int (with u)
l  long int (d, i, o, x, X), long unsigned int (u), double (e,E,f,g,G)
L  long double (e, E, f, g, G)

  ____ NONSTANDARD(!!!) modifiers: ____

F  float (e, E, f, g, G)
A  array of characters instead of pointer to it (s)

*/

#else
 extern ListT ApplyL();
 extern ListT FindChangeL();
 extern ListT MakeFromL();
 extern ListT MapplyL();
 extern ListT ExtractL();
 extern void **LookupTableL();
 extern ListT PruneL();
 extern ListT SortL();
 extern int PrintL();
#endif

/*__________________________ EXTENSION FOR EASIER USE _______________________*/
  

#ifdef ANSI

   
extern ListT AllocForD(size_t DataSize);
 /* use AllocForD instead of AllocForE whenever you have not declared your list
    element type (including pointers to successor, predecessor). DataSize
    specifies size of data block without list pointers. */

extern ListT StdCons(size_t size, ...);
 /* constructs a list element from a data. Uses Lmalloc.
    Do not use for char and float! (promotion problems) */

#else
 extern ListT StdCons();  
 extern ListT AllocForD();
#endif
           
#ifndef DEBUG

#define StdConsL(data) StdCons(sizeof(data), data)
 /* single value constructor, see StdCons */

#define DataPointerL(element) ((LinksT*)element + 1)
 /* return an untyped pointer to user data given an element pointer */

#define ElmPointerL(element) ((LinksT*)element - 1)
 /* return an untyped pointer to the element given the pointer to user data */
    
#else
#ifdef ANSI  
 extern void *DataPointerL(ListT element);
 extern ListT ElmPointerL(void *element);
#else
 extern void *DataPointerL();
 extern ListT ElmPointerL();
#endif
#endif

#ifdef ANSI

extern ListT ConsListL(void *el1, ...);
 /* construct a list from given elements; can join multiple lists;
    usage: ListT myList = ConsListL(MyConsInt(1), MyConsDouble(1.0), NULLL); */

extern int IsConsistentL(ListT list);
 /* perform a list consistency check; when a first inconsistency detected,
    returns from execution.
  Return value (checks in this order):
     1 .... a non-reference instead of a successor encountered
     2 .... the list is incredibly long (moving forward)
    -1 .... a non-reference instead of a predecessor encountered
    -2 .... the list is incredibly long (moving backward)
     3 .... forward and backward chains diverge
     4 .... list is a pointer to NULL
    10 .... NULLL is not O.K.
     0 .... O.K.
 */
#else
 extern ListT ConsListL();
 extern int IsConsistentL();
#endif


/*___________________________ ERROR HANDLING _________________________________*/


#ifdef ANSI

extern int ListErrorCode(void);
 /* returns code of last error */

 /*  codes: 1 ... out of heap    */


extern void ListError(int errorCode);
 /* activates error handler; use for your extensions of the library */

extern int NullErrorHandler(int errorCode);
 /* default error handler (does nothing) */

extern int (*ListErrorHandler)(int code);
 /* error handler variable, default NullErrorHandler                */
 /* standard error report will be masked out if it returns nonzero  */
 /* usage: int (*OldErrorHandler)() = ListErrorHandler;
           ListErrorHandler = MyHandler;
           .....
           ListErrorHandler = OldErrorHandler;                      */


#else
 extern int ListErrorCode();
 extern void ListError();
 extern int (*ListErrorHandler)();
#endif
         
#endif
/* end of file list.h */
