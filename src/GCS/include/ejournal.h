/*_

  ejournal.h     Execution journal support for mex modules

  (c) Radim Sara (sara@cmp.felk.cvut.cz) FEE CTU Prague, 26 Dec 00

  _*/

#ifndef _EJOURNAL_
#define _EJOURNAL_

extern void EjournalCatch(void);
/* To be called at the beggining of mexFunction(). Initializes an auxiliary
   variable. */

extern void EjrnlPrintf(const char *fmt, ...);
/* prints a journal message similarly as the ejrnlpritf.m does */

extern int EjrnlReport(void);
/* returns 1 if execution journal is on */

#endif
