/*_

   imglib.h  simple image processing support for matlab
                                                               
   (c) 1997 Radim Sara, GRASP U of Penn                                
       Radim Sara (sara@cmp.felk.cvut.cz) FEE CTU Prague, 05 Sep 00            
                                                   
    modifications: 
     May 15, 1998
     September 5, 2000
     September 8, 2004: support for 3D matrices
_*/

#ifndef _IMGLIB_
#define _IMGLIB_

#include <mex.h>
#if !defined(V4_COMPAT) && !defined(V4_Redefined)
#define Real double
#define Matrix mxArray
#define imgREAL mxREAL 
#define V4_Redefined
#endif

#include <stdio.h>



typedef Real PixelT;

typedef struct _window
 {
  int r1, c1, r2, c2;
 } WindowT;

typedef struct _image
 {
  Matrix *im;     /* Matlab matrix structure                          */
  int allocated;  /* 1 if im is allocated by ConsImage(), 0 otherwise */
  WindowT wnd;    /* working window                                   */
  PixelT **col;   /* auxiliary array of pointers to matrix columns of
                     the 1st image plane                              */
  PixelT ***pcol; /* auxiliary array of pointers to image planes,
                     array of pointers to columns each                */
 } ImageT;


#define ImGetPr(img) (mxGetPr((img).im))
#define ImGetPi(img) (mxGetPi((img).im))

#define ImGetM(img)  (mxGetM((img).im))
/* get the size of dimension 1 */

#define ImGetN(img)  (mxGetN((img).im))
/* get the product of sizes of all other dimensions. Use
   ImGetN(img)/ImGetK(img) to get the number of columns */

extern int ImGetK(ImageT im);
 /* return 3rd dimension size, returns 1 in 2-D matrices */

#define ImGetDimensions(img) mxGetDimensions((img).im)
#define ImGetNumberOfDimensions(img) mxGetNumberOfDimensions((img).im)

#define ImGetA(img) ((img).col)
 /* :pointer to array of column pointers */

#define ImGetAA(img) ((img).pcol)
 /* :pointer to array of arrays of column pointers */


/* GENERAL RULES:  - no function allocates space for matrices 
                   - coordinates are zero-based (upper left corner is (0,0))

 indexing:

 1-plane (monochromatic images)  im.col[column][row]
 n-plane (color, etc)            im.pcol[plane][column][row]

*/

/*___________________________ WINDOWS _______________________*/

extern const WindowT EmptyWindow;

extern WindowT ConsWindow(int r1, int c1, int r2, int c2);
 /* creates window structure */

#define WindowEmptyQ(wnd) (((wnd).r1>(wnd).r2) || ((wnd).c1>(wnd).c2))
 /* returns 1 if windows do not overlap */

extern int SameWindowSizeQ(WindowT wnd1, WindowT wnd2);
 /* returns 1 if the windows have the same size */

extern WindowT FullWindow(ImageT img);
 /* returns full image window; upper left corner has (0,0) */

#define SetWindow(img, wind) (((img).wnd) = (wind))
 /* idiom: wnd = SetWindow(img, FullWindow(img)) 
    intersects with FullWindow(img) */

#define GetWindow(img) ((img).wnd)
 /* returns current image window */

extern WindowT WindowUp(WindowT wnd, int rows);
 /* moves window "rows" pixels up
    idiom: SetWindow(img, WindowUp(img.wnd)) */

#define WindowDown(wnd, rows) WindowUp(wnd,-rows)
 /* moves window "rows" pixels down */

extern WindowT WindowLeft(WindowT wnd, int cols);
 /* moves window "cols" pixels left */

#define WindowRight(wnd, cols) WindowLeft(wnd, -cols)
 /* moves window "rows" pixels right */

extern WindowT ShrinkBottomSide(WindowT wnd, int rows);
 /* shrinks window by "rows" rows from the bottom */

extern WindowT ShrinkLeftSide(WindowT wnd, int cols);
 /* shrinks window by "cols" columns from the left */

extern WindowT ShrinkRightSide(WindowT wnd, int cols);
 /* shrinks window by "cols" columns from the right */

extern WindowT ShrinkTopSide(WindowT wnd, int rows);
 /* shrinks window by "rows" rows from the top */


extern WindowT CutOff(ImageT im, int mr, int mc);
 /* removes strip around the window for convolution mask of size mr x
    mc the strip widths are half of the corresponding mask
    sizes. Empty window may result. */

extern WindowT Extend(ImageT im, int mr, int mc);
 /* adds strip around the image window for convolution mask of size mr
    x mc the strip widths are half of the corresponding mask
    sizes. Does not extend the image borders. */

extern WindowT IntersectWindows(WindowT wnd1, WindowT wnd2);
 /* returns empty window if there is no intersection, see WindowEmptyQ() */

#define inwindow(wnd,u,v) ((wnd).r1 <= (u) && (u) <= (wnd).r2 && (wnd).c1 <= (v) && (v) <= (wnd).c2)
/* a true/false test if image point (u,v) is within window wnd */

#define FOR_WINDOW(wnd, r, c)\
 for(c=(wnd).c1; c<=(wnd).c2; (c)++)\
  for(r=(wnd).r1; r<=(wnd).r2; (r)++)
 /* :executes the subsequent block for each pixel of the window (per
    columns starting upper left)
 
     idiom: 
             int r, c;                current position in image
             PixelT **ptr = ImGetA(image);

             FOR_WINDOW(image.wnd, r, c)
              ptr[c][r] = 1; sets ones in the window, per columns */

#define FOR_WINDOW_REVERSE(wnd, r, c)\
 for(c=(wnd).c2; c>=(wnd).c1; (c)--)\
  for(r=(wnd).r2; r>=(wnd).r1; (r)--)
 /* :executes the subsequent block for each pixel of the window (per
    columns starting lower right) */

#define FOR_2_WINDOWS(wnda, ra, ca, wndb, rb, cb)\
 for(ca=(wnda).c1,cb=(wndb).c1; ca<=(wnda).c2; ca++,cb++)\
  for(ra=(wnda).r1,rb=(wndb).r1; ra<=(wnda).r2; ra++,rb++)
 /* :executes the subsequent block for each pixel of the window
    "wnda"; the corresponding values are taken from "wndb". Only the
    offset of "wndb" matters. Proceeds per columns from upper left. */

#define FOR_3_WINDOWS(wnda, ra, ca, wndb, rb, cb, wndc, rc, cc)\
 for(ca=(wnda).c1,cb=(wndb).c1,cc=(wndc).c1; ca<=(wnda).c2; ca++,cb++,cc++)\
  for(ra=(wnda).r1,rb=(wndb).r1,rc=(wndc).r1; ra<=(wnda).r2; ra++,rb++,rc++)
 /* :extension of FOR_2_WINDOWS */

extern void GetWindowCenter(int *r, int*c, WindowT wnd);
 /* returns the center pixel coordinates */

extern void PrintWindow(FILE *out, WindowT w, const char *name);
/* prints a named window in the form of <name> [r1, r2, c1, c2] */

/*____________________ CONSTRUCTORS ___________________*/

extern ImageT ConsImage(Matrix *m);
 /* constructs image structure from a Matlab matrix and sets full
    window. See ImFree() for more info. */

extern ImageT ImCreate(int rows, int cols, int complexFlag);
 /* allocates space for new real image and sets the full window */

extern ImageT ImCreate3D(int rows, int cols, int planes, int complexFlag);
 /* allocates space for new 3d real image and sets the full window */

extern void ImFree(ImageT im);
 /* frees image constructed by ConsImage or ImCreate. If im was
    created by ConsImage() the matlab Matrix is NOT deallocated */

/*_____________________IMAGE ARITHMETICS_____________________*/
 
/* IMPORTANT: all subsequent functions work as follows:
 
   if transf is set to:

    "intersect" than the input windows are intersected, the output
                window is set to that intersection and the operation
                is performed in the intersection only

    "move"      than if all the windows have the same size,
                the windows are `moved' to overlap and then the operation
                is performed. If the windows do not have the same size,
                execution is suspended and an error is reported to MATLAB
                The destination window remains unchanged.

  If there is no "transf" argument, the destination window is set
  equivalent to the source window.

  All functions work on real parts only.

  */

typedef enum {intersect=1, move=2} WTransfT;

extern int WindowInImageQ(ImageT img, WindowT wnd);
 /* 1 if wnd is inside the image frame */

extern ImageT InvImage(ImageT *dst, ImageT src);
 /* dst := -src ; dst and src can be the same */

extern ImageT SumImages(ImageT *dst, ImageT src1, ImageT src2, WTransfT tranf);
 /* dst := src1 + src2 */

extern ImageT SubImages(ImageT *dst, ImageT src1, ImageT src2, WTransfT tranf);
 /* dst := src1 - src2 */

extern ImageT MulImages(ImageT *dst, ImageT src1, ImageT src2, WTransfT tranf);
 /* dst := src1 * src2  */

extern ImageT SDivImages(ImageT *dst, ImageT src1, ImageT src2, WTransfT tranf);
 /* dst := src1/src2 where src2!=0 or dst := 0 otherwise */

extern ImageT MulImageConst(ImageT *dst, ImageT src, PixelT cnst);
 /* dst := cnst*src */

extern ImageT AddImageConst(ImageT *dst, ImageT src, PixelT cnst);
 /* dst := cnst+src  */

extern ImageT SetImageConst(ImageT *dst, PixelT cnst);
 /* dst:= const */

extern ImageT SetBorderConst(ImageT *dst, PixelT cnst);
 /* dst := const out of the window */

extern ImageT SquareImage(ImageT *dst, ImageT src);
 /* dst := im.^2 */

extern ImageT SquareRootImage(ImageT *dst, ImageT src, double precision);
 /* dst := sqrt(im) negative values are passed to math library sqrt()
    function, values that within "precision" around zero are assumed
    zero */
  
extern ImageT ReciprocImage(ImageT *dst, PixelT cnst, ImageT src);
/* dst := cnst/src */

extern ImageT CopyImage(ImageT *dst, ImageT src);
 /* dst := src Copies window as well. Space must be allocated! */

/*________________________ CONVOLUTIONS ___________________________*/

/* NOTE: the input window defines the area of valid data. The output
   window is a shrunken version of the input window because of the
   non-zero mask size. The difference is half of the corresponding
   matrix size. The data outside of the output window are undefined
   (Any value, including NaN can appear there. To ensure the strip
   around image have well-defined values, use SetBorderConst()). If
   you have valid data outside of the input window, extend it by
   Extend() prior calling Boxing(), etc.: 

   Extend(image, mskrows, mskcols);
   Boxing(image, mskrows, mskcols);
   Cutoff(image, mskrows, mskcols);

    MAKE SURE you have enough space to extend the window. The image borders
    makes this procedure more complicated.

   Convolutions are done per image planes. If the mask is 3D, each
   plane is comvolved by its corresponding mask. If it is just 2-D,
   all planes are convolved with the same mask. If you want to collaps
   the resulting planes using some function, use CollapseXXX().

 */

extern ImageT CollapseBySum(ImageT *dst, ImageT src);
/* collapses all planes to a single one by summing them together */

extern ImageT Boxing(ImageT *dst, ImageT src, int mr, int mc);
/* performs boxing sum over the image using mask mr X mc;
   * does not check if the matrices have the same size! 
   * does not check if the matrix is at least mr x mc large!
   * leaves an empty strip around image window borders (shrinks
     the input window): the input window markes valid data. If you 
     want to avoid this, extend the window by Extend() prior to
     calling Boxing() */
     
extern ImageT BoxMin(ImageT *dst, ImageT src, int mr, int mc);
/* performs boxing min over the image using mask mr X mc;
   * does not check if the matrices have the same size! 
   * does not check if the matrix is at least mr x mc large!
   * leaves an empty strip around image window borders (shrinks
     the input window): the input window markes valid data. If you 
     want to avoid this, extend the window by Extend() prior to
     calling Boxing() */

extern ImageT BoxMax(ImageT *dst, ImageT src, int mr, int mc);
/* performs boxing min over the image using mask mr X mc;
   * does not check if the matrices have the same size! 
   * does not check if the matrix is at least mr x mc large!
   * leaves an empty strip around image window borders (shrinks
     the input window): the input window markes valid data. If you 
     want to avoid this, extend the window by Extend() prior to
     calling Boxing() */

extern ImageT Convolution(ImageT *dst, ImageT src, ImageT msk);
/* convolution of an image and a non-decomposable mask; does not check
   if the mask has odd size. Sets the output window to the valid
   result area. */


extern ImageT CentralDiffR(ImageT *dst, ImageT src);
/* central difference in row direction, "src" and "dst" may be the
   same data matrix. Sets the output window to the valid result
   area. */

extern ImageT CentralDiffC(ImageT *dst, ImageT src);
/* central difference in column direction, "src" and "dst" may be the
   same data matrix. Sets the output window to the valid result
   area. */

extern ImageT SobelR(ImageT *dst, ImageT src);
/* difference in row direction. Sets the output window to the valid
   result area. */

extern ImageT SobelC(ImageT *dst, ImageT src);
/* difference in column direction. Sets the output window to the valid
   result area. */

#endif
