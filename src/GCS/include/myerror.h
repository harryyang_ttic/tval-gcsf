/*_
                                                                     
 myerror.h   useful routines standardizing reporting errors,         
             printing messages on screen and in a log-file.          
             See descriptions below declarations.                    

             Error group codes:

              1 ... memory allocation
              2 ... i/o error
             10 ... low-level library errors
            100 ... application errors

                                                  
 (c) Radim Sara, GRASP U of Penn, 1996                                   
                                                                     
_*/

#ifndef _MYERROR_
#define _MYERROR_

#include <stdio.h>

/* some compilers do not have __FUNCTION__ preprocessor variable */
#ifndef FUNCTION_EXISTS
#define FNCREF "unknown"
#else
#define FNCREF __FUNCTION__
#endif

#define Message SetP(FNCREF,__FILE__,__LINE__,FullMessage)
 /* Message(loc, format,...) prints a message on message channel, it
    works like printf(). If loc is non-zero it prints the exact
    position in the source code where the error has occured. Best to
    report unexpected soft errors. */

extern void RedirectMessage(FILE *f);
 /* Redirects message channel to file f. Default f = NULL (implies
    stderr). Does not check if f is a valid stream.  */

#define LogFile SetP(FNCREF,__FILE__,__LINE__,FullLogFile)
 /* LogFile(loc, format,...) prints a line in log file, works like
    printf() If loc is non-zero it prints the exact position in the
    source code where the error has occured. Best to report debugging
    messages, etc. */

extern void SetupLogFile(const char *fname);
 /* Removes the most recent log file and resets its name if
    requested. SetupLogFile(NULL) suppresses any log output
    SetupLogFile("") does not reset the previously defined
    name. SetupLogFile("stderr") redirects output to stderr. The
    default log file name is log.log */

#define Error  SetP(FNCREF,__FILE__,__LINE__,(MessageFnT)FullError)
 /* Error(err_code, format, ...) prints an error message on error
    channel and then calls the error-handler previously set by
    CallAtError(). Default handler is exit(err_code). See also
    CallAtError(). Best to invoke the error-handling mechanism.  */

extern void RedirectError(FILE *f);
 /* Redirects error messages to file f. Default f = NULL (implies
    stderr). Does not check if f is a valid stream. */

extern void CallAtError(void (*errorfun)(int));
 /* Registers a new error-handling function. If the error handling
    function does not call exit(code), it is called after the control
    returns from the function. */

extern void RedirectPrintf(int (*newprintf)(const char*,...));
 /* Redefines printf() for errors redirected to stdout. Does not affect
    fprintf() which writes to the log file. Useful under Matlab. Synopsis:
    RedirectError(stdout);
    RedirectMessage(stdout);
    RedirectPrintf(mexPrintf);
    CallAtError(LocalError);
  */

/*_ auxiliary types and functions _*/

typedef void (*MessageFnT)(int, const char*,...);

extern MessageFnT SetP(const char *function, const char *file, int line, MessageFnT fun);
extern void FullMessage(int localize, const char *fmt, ...);
extern void FullLogFile(int localize, const char *fmt, ...);
extern void FullError(int code, const char *fmt, ...);

#endif
