/*_

 $Source$
 $Id$

 Matlab 5.3 mex module

 x = boxing(im, msksiz)

     Sum by boxing algorithm over centered window msksiz(1) x msksiz(2)

     im - input image (double)
     msksiz - [rows, cols], both must be odd

 (c) Radim Sara (sara@cmp.felk.cvut.cz) FEE CTU Prague, 22 Oct 99

  This is free software for a non-commercial purpose. Commercial use requires
  a licence. The software is distributed WITHOUT ANY IMPLIED OR EXPRESSED
  WARRANTY and WITHOUT ANY FURTHER SUPPORT for the desired and/or other
  purpose. See README file in the main distribution.

 $Log$
_*/

#include <mex.h>
#include <matrix.h>
#include "myerror.h"
#include "imglib.h"

/* _________________________________ MAIN _________________________________ */


static void LocalError(int c)
{
 static char sbuf[64];

 sprintf(sbuf, "Error code: %i", c);
 mexErrMsgTxt(sbuf);
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
 int m, n, M;
 int r,c,r1,c1;
 double *workp, *p;
 ImageT work, out;
 WindowT wnd;

 SetupLogFile("stderr");
 CallAtError(LocalError);

 if (nrhs != 2)
  Error(2, "Not enough input arguments");

 if ( !mxIsClass(prhs[0], "double") )
  Error(2, "Image is not of double-float class");

 if ( mxGetM(prhs[1])*mxGetN(prhs[1]) != 2 )
  Error(2, "Mask size is not in the form [rows, columns]");

 if ( !mxIsClass(prhs[1], "double") )
  Error(2, "Mask size is not of double-float class");

 p = mxGetPr(prhs[1]);
 r = (int)p[0];
 c = (int)p[1];
 
 if (r % 2 == 0 || c % 2 == 0)
  Error(2, "Mask size must be odd");

 m = mxGetM(prhs[0]);
 n = mxGetN(prhs[0]);

 work = ImCreate(m+2*(r/2), n+2*(c/2), mxREAL);

  /* copy image to working variable: */
 p = mxGetPr(prhs[0]);
 workp = ImGetPr(work);
 M = ImGetM(work);
 wnd = CutOff(work, r, c);
 FOR_WINDOW(wnd, r1, c1)
  workp[r1+c1*M] = *p++;

 out = ImCreate(m+2*(r/2), n+2*(c/2), mxREAL);
 Boxing(&out, work, r, c); 

  /* copy result to Matlab */
 plhs[0] = mxCreateDoubleMatrix(m, n, mxREAL);
 p = mxGetPr(plhs[0]);
 workp = ImGetPr(out);
 M = ImGetM(out);
 FOR_WINDOW(out.wnd, r1, c1)
  *p++ = workp[r1+c1*M];

 ImFree(work);
 ImFree(out);
}
