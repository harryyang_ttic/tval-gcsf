#include <queue>
#include <mex.h>
#include <arrays.h> //my library /home.dokt/cechj/matlab/cech/src/
                    //compile >> mex fname.c -I/home.dokt/cechj/matlab/cech/src/
#include <math.h>
using namespace std;  // std c++ libs implemented in std

#define max(a,b) (a>b)?a:b;

const double NaN = mxGetNaN();
const double eps = mxGetEps();
                    
struct seed {
  int xl1;
  int xr1;
  int y1;
  int xl2;
  int xr2;
  int y2;
  double c;
};

bool operator <(const seed& a, const seed&b) {
  return a.c<b.c;
}


class MAIN {
public:
  ARRAY_2D<double> iL1;
  ARRAY_2D<double> iR1;
  ARRAY_2D<double> sL1;
  ARRAY_2D<double> sR1;
  ARRAY_2D<double> vL1;
  ARRAY_2D<double> vR1;
  
  ARRAY_2D<double> iL2;
  ARRAY_2D<double> iR2;
  ARRAY_2D<double> sL2;
  ARRAY_2D<double> sR2;
  ARRAY_2D<double> vL2;
  ARRAY_2D<double> vR2;
  ARRAY_2D<double> SEEDs;
  ARRAY_2D<double> D1; //input disparity map
  
  
  ARRAY_2D<double> D2; //output disparity map
  ARRAY_2D<double> Fh; //output horizontal flow (left)
  ARRAY_2D<double> Fv; //output horizontal flow (left)
  ARRAY_2D<double> W;  //output correlation map

  int w;
  ARRAY_2D<int> local_search; //local search-matrix [x,y] (nx2)
  double c_thr;
  double epsilon;
  double dmin, dmax; //(horizontal) disparity searchrange
  double fHmin, fHmax; //(horizontal) flow searchrange
  double fVmin, fVmax; //(veritcal) flow searchrange
  double kappa, alpha;
  int vis_step;
  
  priority_queue<seed> SEEDS;

  double wcorr_L1_R1(int xl, int xr, int yl, int yr);
  double wcorr_L1_L2(int xl, int xr, int yl, int yr);
  double wcorr_R1_R2(int xl, int xr, int yl, int yr);
  double wcorr_L2_R2(int xl, int xr, int yl, int yr);

  double wcorr(int xl1, int xr1, int y1, int xl2, int xr2, int y2);
  void local_optim(int xl1, int xr1, int y1, int xl2, int xr2, int y2, 
                         double&c, int& Xl2, int& Xr2, int& Y2);
  void grow();

  MAIN(ARRAY_2D<double>& iL1_, ARRAY_2D<double>& iR1_,
       ARRAY_2D<double>& sL1_, ARRAY_2D<double>& sR1_,
       ARRAY_2D<double>& vL1_, ARRAY_2D<double>& vR1_,
       ARRAY_2D<double>& iL2_, ARRAY_2D<double>& iR2_,
       ARRAY_2D<double>& sL2_, ARRAY_2D<double>& sR2_,
       ARRAY_2D<double>& vL2_, ARRAY_2D<double>& vR2_,          
       ARRAY_2D<double>& D1_, ARRAY_2D<double>& SEEDs_, 
       int w_, double c_thr_, ARRAY_2D<double>& local_search_, 
       double dmin_, double dmax_, double fHmin_, double fHmax_,
       double fVmin_, double fVmax_,
       int vis_step_, double epsilon_, double kappa_, double alpha_,
       ARRAY_2D<double>& D2_, ARRAY_2D<double>& Fh_, ARRAY_2D<double>& Fv_,
       ARRAY_2D<double>& W_);

  ~MAIN(){};
};

// MAIN methods implementation ---------------------------------------------

MAIN::MAIN(ARRAY_2D<double>& iL1_, ARRAY_2D<double>& iR1_,
       ARRAY_2D<double>& sL1_, ARRAY_2D<double>& sR1_,
       ARRAY_2D<double>& vL1_, ARRAY_2D<double>& vR1_,
       ARRAY_2D<double>& iL2_, ARRAY_2D<double>& iR2_,
       ARRAY_2D<double>& sL2_, ARRAY_2D<double>& sR2_,
       ARRAY_2D<double>& vL2_, ARRAY_2D<double>& vR2_,          
       ARRAY_2D<double>& D1_, ARRAY_2D<double>& SEEDs_, 
       int w_, double c_thr_, ARRAY_2D<double>& local_search_, 
       double dmin_, double dmax_, double fHmin_, double fHmax_,
       double fVmin_, double fVmax_,
       int vis_step_, double epsilon_, double kappa_, double alpha_,
       ARRAY_2D<double>& D2_, ARRAY_2D<double>& Fh_, ARRAY_2D<double>& Fv_,
       ARRAY_2D<double>& W_){

  seed s; 
  int i;

  iL1.assign(iL1_);
  iR1.assign(iR1_);
  sL1.assign(sL1_);
  sR1.assign(sR1_);
  vL1.assign(vL1_);
  vR1.assign(vR1_);
  
  iL2.assign(iL2_);
  iR2.assign(iR2_);
  sL2.assign(sL2_);
  sR2.assign(sR2_);
  vL2.assign(vL2_);
  vR2.assign(vR2_);
  
  D1.assign(D1_);
  SEEDs.assign(SEEDs_);
  
  D2.assign(D2_);
  Fh.assign(Fh_);
  Fv.assign(Fv_);
  W.assign(W_);
  
  ARRAY_2D<int> loc_search(local_search_.M,local_search_.N);
  for (i=1; i<=local_search_.M*local_search_.N; i++) {
    loc_search[i] = int(local_search_[i]);
  }
  local_search.assign(loc_search);
  loc_search.need_to_release = 0;
  local_search.need_to_release = 1;

  w = w_; c_thr = c_thr_; vis_step = vis_step_;
  dmin = dmin_; dmax = dmax_;
  fHmin = fHmin_; fHmax = fHmax_;
  fVmin = fVmin_; fVmax = fVmax_;
  epsilon = epsilon_;
  //i_seed_accept = i_seed_accept_;
  kappa = kappa_;
  alpha = alpha_;
  
  //output intialilzation
  for(i=1;i<=(iL1.M*iL1.N);i++) { 
    D2[i] = NaN;
    Fh[i] = NaN;
    Fv[i] = NaN;
    W[i] = NaN;
  }
  
  //filling the list (priority queue)  of seeds and initial disparity 
  //into disparity map
  for (i=1;i<=SEEDs.M;i++) {
    s.xl1 = int(SEEDs.s(i,1));
    s.xr1 = int(SEEDs.s(i,2));
    s.y1 = int(SEEDs.s(i,3));
    s.xl2 = int(SEEDs.s(i,4));
    s.xr2 = int(SEEDs.s(i,5));
    s.y2 = int(SEEDs.s(i,6));
    s.c = wcorr(s.xl1,s.xr1,s.y1, s.xl2,s.xr2,s.y2)+alpha;

    //mexPrintf("%f \n",s.c);
    SEEDS.push(s);
  }

  
}

// window correlation (pairwise) L1_R1--------------------------------------

double MAIN::wcorr_L1_R1(int xl, int xr, int yl, int yr) {
  int i,j;
  double sLR;
  double c;
  
  sLR = 0;
  for (i=-w; i<=w; i++) {
      for (j=-w; j<=w; j++) {
        sLR = sLR + iL1.s(yl+i,xl+j) * iR1.s(yr+i,xr+j);
      }
  }

  c = 2*(sLR - sL1.s(yl,xl)*sR1.s(yr,xr)) / (vL1.s(yl,xl) + vR1.s(yr,xr));
  if (c>1) c=0; //hack
  //c = c*exp(-kappa*( abs(xl-xr)+abs(yl-yr) ) );
  return c;
}

// window correlation (pairwise) L2_R2--------------------------------------

double MAIN::wcorr_L2_R2(int xl, int xr, int yl, int yr) {
  int i,j;
  double sLR;
  double c;
  
  sLR = 0;
  for (i=-w; i<=w; i++) {
      for (j=-w; j<=w; j++) {
        sLR = sLR + iL2.s(yl+i,xl+j) * iR2.s(yr+i,xr+j);
      }
  }

  c = 2*(sLR - sL2.s(yl,xl)*sR2.s(yr,xr)) / (vL2.s(yl,xl) + vR2.s(yr,xr));
  if (c>1) c=0; //hack
  //c = c*exp(-kappa*( abs(xl-xr)+abs(yl-yr) ) );
  return c;
}

// window correlation (pairwise) L1_L2--------------------------------------

double MAIN::wcorr_L1_L2(int xl, int xr, int yl, int yr) {
  int i,j;
  double sLR;
  double c;
  
  sLR = 0;
  for (i=-w; i<=w; i++) {
      for (j=-w; j<=w; j++) {
        sLR = sLR + iL1.s(yl+i,xl+j) * iL2.s(yr+i,xr+j);
      }
  }

  c = 2*(sLR - sL1.s(yl,xl)*sL2.s(yr,xr)) / (vL1.s(yl,xl) + vL2.s(yr,xr));
  if (c>1) c=0; //hack
  //c = c*exp(-kappa*( abs(xl-xr)+abs(yl-yr) ) );
  return c;
}

// window correlation (pairwise) R1_R2--------------------------------------


double MAIN::wcorr_R1_R2(int xl, int xr, int yl, int yr) {
  int i,j;
  double sLR;
  double c;
  
  sLR = 0;
  for (i=-w; i<=w; i++) {
      for (j=-w; j<=w; j++) {
        sLR = sLR + iR1.s(yl+i,xl+j) * iR2.s(yr+i,xr+j);
      }
  }

  c = 2*(sLR - sR1.s(yl,xl)*sR2.s(yr,xr)) / (vR1.s(yl,xl) + vR2.s(yr,xr));
  if (c>1) c=0; //hack
  //c = c*exp(-kappa*( abs(xl-xr)+abs(yl-yr) ) );
  return c;
}


// window correlation (4 points correlation) -------------------------------

double MAIN::wcorr(int xl1, int xr1, int y1, int xl2, int xr2, int y2) {
    double c;
    /*c = 1/3 * ( wcorr_L2_R2(xl2,xr2,y2,y2) +
                wcorr_L1_L2(xl1,xl2,y1,y2) +
                wcorr_R1_R2(xr1,xr2,y1,y2) );*/
    c = ( wcorr_L2_R2(xl2,xr2,y2,y2)+
          wcorr_L1_L2(xl1,xl2,y1,y2)+
          wcorr_R1_R2(xr1,xr2,y1,y2) )/3;
    return c;
}


// local optimization ------------------------------------------------------
void MAIN::local_optim(int xl1, int xr1, int y1, int xl2, int xr2, int y2, 
                         double&c, int& Xl2, int& Xr2, int& Y2) {
    int l;
    int xl,xr,y;
    int rxl, rxr, ry; 
    double cc; //current corelation
    double c2; //2nd best correlation
    double cr,ccr; //correlation with regularization
    
    c = -1; cr = -1; c2 = -1;
    
    for (l=1; l<=local_search.M; l++) {
        xl = xl2+local_search.s(l,1);
        xr = xr2+local_search.s(l,2);
        y = y2+local_search.s(l,3);
        if ( xl>w && xl<iL2.N-w && xr>w && xr<iR2.N-w &&
              y>w &&  y<iL2.M-w ) { //range check
            if ( mxIsNaN(Fh.s(y1,xl1)) && mxIsNaN(D2.s(y,xl)) ) { //not alrady matched
                cc = wcorr(xl1, xr1, y1, xl, xr, y);
                ccr = cc*exp(-kappa*( abs(local_search.s(l,1))+abs(local_search.s(l,2))+abs(local_search.s(l,3)) ) );
                //ccr = cc*exp( -kappa*( abs(xl-xl1)+abs(xr-xr1)+abs(y-y1) ) );
                if (cc > c) {
                    c2 = c;
                    c = cc;
                    Xl2 = xl; Xr2 = xr; Y2 = y;
                };
                if (ccr>cr) {
                    cr = ccr;
                    rxl = xl; rxr = xr; ry = y;
                }
            }
        };
    }
    if ( c-c2 < epsilon ) { //do not grow when ambiguity
        //c = -1;
        c = cr;
        Xl2 = rxl; Xr2 = rxr; Y2 = ry;
    }
    
}

// // local optimization ------------------------------------------------------
// void MAIN::local_optim(int xl, int xr, int yl, int yr, 
//                        double& c, int& dp, int& dpv) {
//   //c,dp is output
//   
//   int i,j,k,xrr,yrr;
//   double cc; //current corelation
//   double c2; //2nd best correlation
//   c = -1;
//   dp = 0; dpv = 0;
//   for (k=1; k<=local_search.M; k++) {
//     //  for (i=-range; i<=range; i++) {
//     i = local_search.s(k,1);
//     j = local_search.s(k,2);
//     xrr = xr + i;
//     yrr = yr + j;
//     if (xl>w & xl<iL.N-w & xrr>w & xrr<iR.N-w & 
//         yl>w & yl<iL.M-w & yrr>w & yrr<iR.M-w) {
//       cc = wcorr(xl,xrr,yl,yrr);
//       //      mexPrintf("(%i,%i, %i,%i |%f) ",xl,xrr,yl,yrr,cc);
//       if (cc > c) {
//         c2 = c;     
//         c = cc;
//         dp = i;
//         dpv = j;
//       };
//     };
//   }
//   if ( c-c2 < epsilon ) { //do not grow when ambiguity
//     c = -1;
//   }
//   //  mexPrintf("\n");
// }

// growing -----------------------------------------------------------------

void MAIN::grow() {

  unsigned vis=0;
  int xl1,xr1,y1,xr2,xl2,y2;
  int Xl2,Xr2,Y2;
  int d,fh_l,fh_r,fv;
  seed s,st;
  double c;
  int ind;
  mxArray *M; //only for visualization

  
  
  //main loop
  while (!SEEDS.empty()) {
    s = SEEDS.top();
    SEEDS.pop();   //remove the processed seed

    //mexPrintf("grow: %f, (%f,%f), (%f,%f) \n",s.c, fHmin,fHmax,fVmin,fVmax);
    
    //assing a seed to the results 
    if (s.c>c_thr) {
        if ( mxIsNaN( W.s(s.y1,s.xl1) ) || s.c > W.s(s.y1,s.xl1)  ) {  //not matched or lower correlation
        //if ( mxIsNaN( Fh.s(s.y1,s.xl1) ) ) {  //not matched yet
            D2.s(s.y2, s.xl2) = s.xl2-s.xr2;
            Fh.s(s.y1,s.xl1) = s.xl1-s.xl2;
            Fv.s(s.y1,s.xl1) = s.y1-s.y2;
            W.s(s.y1,s.xl1) = s.c;
        }
    }
    
    
    //left 
    xl1 = s.xl1-1;
    y1 = s.y1;
    xl2 = s.xl2-1;
    y2 = s.y2;
    ind = D2.sub2ind(y1, xl1);
    if ( xl1>w ) { //range check
        if ( mxIsNaN(Fh[ind]) ) { //not matched yet
            if ( !mxIsNaN( D1[ind] ) && abs((s.xl1-s.xr1)-int(D1[ind]))<=1 ) { //disparity in previous frame defined and not very different from the seed
                //xr1 = s.xl1 - int(D1[ind]); (bug fixed, 07/10/2010)
				xr1 = xl1 - int(D1[ind]);
                if (xr1>w && xr1<iR1.N-w) {
                    xr2 = s.xr2 + (xr1-s.xr1);
                    local_optim(xl1, xr1, y1, xl2, xr2, y2, c, Xl2, Xr2, Y2); 
                    if (c>c_thr) {
                        d = Xl2-Xr2;
                        if (d>=dmin && d<=dmax) { //disparity searchrange
                            fh_l = xl1-Xl2; fh_r = xr1-Xr2;
                            if (fh_l>=fHmin && fh_l<=fHmax && fh_r>=fHmin && fh_r<=fHmax) { //horizontal flow range
                                fv = y1-Y2;
                                if (fv>=fVmin && fv<=fVmax) { //vertical flow range
                                    D2.s(Y2,Xl2) = d;
                                    Fh[ind] = fh_l;
                                    Fv[ind] = fv;
                                    W[ind] = c;
                                    
                                    st.xl1 = xl1;
                                    st.xr1 = xr1;
                                    st.y1 = y1;
                                    st.xl2 = Xl2;
                                    st.xr2 = Xr2;
                                    st.y2 = Y2;
                                    st.c = c;
                                    
                                    SEEDS.push(st);
                                }
                            }
                        }
                    }
                }
            }
        }
    }    
    
    //right 
    xl1 = s.xl1+1;
    y1 = s.y1;
    xl2 = s.xl2+1;
    y2 = s.y2;
    ind = D2.sub2ind(y1, xl1);
    if ( xl1<iL1.N-w ) { //range check
        if ( mxIsNaN(Fh[ind]) ) { //not matched yet
            if ( !mxIsNaN( D1[ind] ) && abs((s.xl1-s.xr1)-int(D1[ind]))<=1 ) { //disparity in previous frame defined and not very different from the seed
                //xr1 = s.xl1 - int(D1[ind]);  (bug fixed, 07/10/2010) 
				xr1 = xl1 - int(D1[ind]); 
                if (xr1>w && xr1<iR1.N-w) {
                    xr2 = s.xr2 + (xr1-s.xr1);
                    local_optim(xl1, xr1, y1, xl2, xr2, y2, c, Xl2, Xr2, Y2);
                    if (c>c_thr) {
                        d = Xl2-Xr2;
                        if (d>=dmin && d<=dmax) { //disparity searchrange
                            fh_l = xl1-Xl2; fh_r = xr1-Xr2;
                            if (fh_l>=fHmin && fh_l<=fHmax && fh_r>=fHmin && fh_r<=fHmax) { //horizontal flow range
                                fv = y1-Y2;
                                if (fv>=fVmin && fv<=fVmax) { //vertical flow range
                                    D2.s(Y2,Xl2) = d;
                                    Fh[ind] = fh_l;
                                    Fv[ind] = fv;
                                    W[ind] = c;
                                    
                                    st.xl1 = xl1;
                                    st.xr1 = xr1;
                                    st.y1 = y1;
                                    st.xl2 = Xl2;
                                    st.xr2 = Xr2;
                                    st.y2 = Y2;
                                    st.c = c;
                                    
                                    SEEDS.push(st);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    
    //up
    xl1 = s.xl1;
    y1 = s.y1-1;
    xl2 = s.xl2;
    y2 = s.y2-1;
    ind = D2.sub2ind(y1, xl1);
    if ( y1>w ) { //range check
        if ( mxIsNaN(Fh[ind]) ) { //not matched yet
            if ( !mxIsNaN( D1[ind] ) && abs((s.xl1-s.xr1)-int(D1[ind]))<=1 ) { //disparity in previous frame defined and not very different from the seed
                xr1 = s.xl1 - int(D1[ind]);
                if (xr1>w && xr1<iR1.N-w) {
                    xr2 = s.xr2 + (xr1-s.xr1);
                    local_optim(xl1, xr1, y1, xl2, xr2, y2, c, Xl2, Xr2, Y2);
                    if (c>c_thr) {
                        d = Xl2-Xr2;
                        if (d>=dmin && d<=dmax) { //disparity searchrange
                            fh_l = xl1-Xl2; fh_r = xr1-Xr2;
                            if (fh_l>=fHmin && fh_l<=fHmax && fh_r>=fHmin && fh_r<=fHmax) { //horizontal flow range
                                fv = y1-Y2;
                                if (fv>=fVmin && fv<=fVmax) { //vertical flow range
                                    D2.s(Y2,Xl2) = d;
                                    Fh[ind] = fh_l;
                                    Fv[ind] = fv;
                                    W[ind] = c;
                                    
                                    st.xl1 = xl1;
                                    st.xr1 = xr1;
                                    st.y1 = y1;
                                    st.xl2 = Xl2;
                                    st.xr2 = Xr2;
                                    st.y2 = Y2;
                                    st.c = c;
                                    
                                    SEEDS.push(st);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    

    //down
    xl1 = s.xl1;
    y1 = s.y1+1;
    xl2 = s.xl2;
    y2 = s.y2+1;
    ind = D2.sub2ind(y1, xl1);
    if ( y1<iL1.M-w ) { //range check
        if ( mxIsNaN(Fh[ind]) ) { //not matched yet
            if ( !mxIsNaN( D1[ind] ) && abs((s.xl1-s.xr1)-int(D1[ind]))<=1 ) { //disparity in previous frame defined and not very different from the seed
                xr1 = s.xl1 - int(D1[ind]);
                if (xr1>w && xr1<iR1.N-w) {
                    xr2 = s.xr2 + (xr1-s.xr1);
                    local_optim(xl1, xr1, y1, xl2, xr2, y2, c, Xl2, Xr2, Y2);
                    if (c>c_thr) {
                        d = Xl2-Xr2;
                        if (d>=dmin && d<=dmax) { //disparity searchrange
                            fh_l = xl1-Xl2; fh_r = xr1-Xr2;
                            if (fh_l>=fHmin && fh_l<=fHmax && fh_r>=fHmin && fh_r<=fHmax) { //horizontal flow range
                                fv = y1-Y2;
                                if (fv>=fVmin && fv<=fVmax) { //vertical flow range
                                    D2.s(Y2,Xl2) = d;
                                    Fh[ind] = fh_l;
                                    Fv[ind] = fv;
                                    W[ind] = c;
                                    
                                    st.xl1 = xl1;
                                    st.xr1 = xr1;
                                    st.y1 = y1;
                                    st.xl2 = Xl2;
                                    st.xr2 = Xr2;
                                    st.y2 = Y2;
                                    st.c = c;
                                    
                                    SEEDS.push(st);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    
    //visualization only
    vis++;
    if (vis > vis_step) {
      M = D2.convert2mx();
      mexCallMATLAB(0,NULL,1,&M,"imager");
      mxDestroyArray(M);
      vis = 0;
    }

  }

}


// ========================================================================

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  //input

  ARRAY_2D<double> iL1(prhs[0]); 
  ARRAY_2D<double> iR1(prhs[1]);  
  ARRAY_2D<double> sL1(prhs[2]);
  ARRAY_2D<double> sR1(prhs[3]);
  ARRAY_2D<double> vL1(prhs[4]);
  ARRAY_2D<double> vR1(prhs[5]);
  
  ARRAY_2D<double> iL2(prhs[6]); 
  ARRAY_2D<double> iR2(prhs[7]);  
  ARRAY_2D<double> sL2(prhs[8]);
  ARRAY_2D<double> sR2(prhs[9]);
  ARRAY_2D<double> vL2(prhs[10]);
  ARRAY_2D<double> vR2(prhs[11]);
  

  ARRAY_2D<double> D1(prhs[12]);
  ARRAY_2D<double> SEEDs(prhs[13]);

  double* tmp;
  int w;
  int vis_step;
  double c_thr;
  double epsilon;
  double dmin, dmax;
  double fHmin,fHmax,fVmin,fVmax;
  bool i_seed_accept;
  double kappa;
  double alpha;

  tmp = mxGetPr(prhs[14]); w = int(tmp[0]);
  tmp = mxGetPr(prhs[15]); c_thr = tmp[0];

  ARRAY_2D<double> local_search(prhs[16]);

  tmp = mxGetPr(prhs[17]); dmin = tmp[0]; dmax = tmp[1];
  tmp = mxGetPr(prhs[18]); fHmin = tmp[0]; fHmax = tmp[1];
  tmp = mxGetPr(prhs[19]); fVmin = tmp[0]; fVmax = tmp[1];
  
  
  tmp = mxGetPr(prhs[20]); vis_step = int(tmp[0]);
  tmp = mxGetPr(prhs[21]); epsilon = tmp[0];
  tmp = mxGetPr(prhs[22]); kappa = tmp[0];
  tmp = mxGetPr(prhs[23]); alpha = tmp[0];
  
  //output
  plhs[0] = mxCreateDoubleMatrix(iL1.M, iL1.N, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(iL1.M, iL1.N, mxREAL);
  plhs[2] = mxCreateDoubleMatrix(iL1.M, iL1.N, mxREAL);
  plhs[3] = mxCreateDoubleMatrix(iL1.M, iL1.N, mxREAL);
  ARRAY_2D<double> D2(plhs[0]);
  ARRAY_2D<double> Fh(plhs[1]);
  ARRAY_2D<double> Fv(plhs[2]);
  ARRAY_2D<double> W(plhs[3]);

  //end of interface

  MAIN p(iL1,iR1,sL1,sR1,vL1,vR1,
         iL2,iR2,sL2,sR2,vL2,vR2,          
         D1,SEEDs,w,c_thr,
         local_search,dmin,dmax,fHmin,fHmax,fVmin,fVmax,
         vis_step,epsilon,kappa,alpha,
         D2,Fh,Fv,W);

  p.grow();

}
  

/*

[D2,Fh,Fv,W] = dgrowF_bf_mex(Il1,Ir1,sL1,sR1,vL1,vR1, ...
                             Il2,Ir2,sL2,sR2,vL2,vR2, ...
                             D1,SEEDs, w, c_thr, ...
                             local_search, searchrange, searchrange_Fh, searchrange_Fv, ...
                             vis_step, epsilon, kappa);
 */

